//*****************************************************************************
// Copyright (C) 2007 DESY(Deutsches-Elektronen Synchrotron) 
//
// File Name	: fru.h
// 
// Title		: FRU device header file
// Revision		: 1.1
// Notes		:	
// Target MCU	: Atmel AVR series
//
// Author       : Vahan Petrosyan (vahan_petrosyan@desy.de)
// Modified by  : Markus Joos (markus.joos@cern.ch)
//
// Description : FRU information and payload control definitions
//					
//
// This code is distributed under the GNU Public License
//		which can be found at http://www.gnu.org/licenses/gpl.txt
//*****************************************************************************
#ifndef FRU_H
#define FRU_H

	#include "avrlibtypes.h"


	// Constants
	#define FRU_SIZE              296

	#define FRU_COLD_RESET        0
	#define FRU_WARM_RESET        1
	#define FRU_REBOOT            2
	#define FRU_QUIESCE           4
	#define POWER_UP              5
	#define POWER_DOWN            6

	enum M_Status{ M0, M1, M2, M3, M4, M5, M6, M7 };

	#define AMC_MODULE		0x80
	#define PCIE			0x02
	#define LANE_NOT_USED	0xFF
	#define LANE0_FLAG		0x01
	#define LANE1_FLAG		0x02
	#define LANE2_FLAG		0x04
	#define LANE3_FLAG		0x08
	#define NO_EXTENSION	0x00
	#define OPERATES_ALONE	0x00

	#define EXACT_MATCHES	0x00
	#define MATCHES_01		0x01
	#define MATCHES_10		0x02

	#define PORT(n) 		n

	//Function prototypes
	u08 ipmi_get_fru_inventory_area_info(u08* buf);
	u08 ipmi_fru_data_read(u08* area, u08 len, u08* data);
	u08 ipmi_fru_data_write(u08* area,u08* data, u08 len);
	u08 ipmi_picmg_port_state_get(u08 port, u08* buf);
	u08 ipmi_picmg_port_state_set(u08 port, u08 state);
	u08 ipmi_picmg_properties(u08* buf);
	u08 ipmi_set_fru_policy(u08 mask_bits, u08 set_bits);
	u08 ipmi_get_fru_policy();
	u08 ipmi_set_fru_activation(u08 fru_activation_deactivation);

#endif //FRU_H