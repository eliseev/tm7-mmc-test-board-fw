/*
 * frueditor.h
 *
 * Created: 24/03/2015 16:56:32
 *  Author: jumendez
 */ 
#ifdef USE_FRU_INFO_ENABLE
	#ifndef FRUEDITOR_H
	#define FRUEDITOR_H

	void light_amc_point_to_point_record_func(void);
	void module_current_record_function(void);
	void board_info_area_function(void);
	void product_info_area_function(void);
	void point_to_point_clock(void);
	void fru_header_function(void);
	void free_fru_binary(void);
	void write_fru_binary();

	#endif

	//Module Current record (SAVE)
	#ifdef MODULE_CURRENT_RECORD_SAVE
		#define current_in_ma(curr)		(u08)(curr/100);
	#endif

	//AMC Point to point connectivity record (LIGHT RECORD SAVE)
	#ifdef LIGHT_AMC_MODULE_POINT_TO_POINT_RECORD_SAVE
		#undef LIGHT_AMC_MODULE_POINT_TO_POINT_RECORD
		#define LIGHT_AMC_MODULE_POINT_TO_POINT_RECORD(record_id, port, protocol, protocol_extension, matches)							\
			amc_point_to_point_records[record_id] = (u08 *)malloc(sizeof(u08) * 21);													\
			amc_point_to_point_records[record_id][0] = 0xC0;																			\
			amc_point_to_point_records[record_id][1] = 0x02;																			\
			amc_point_to_point_records[record_id][2] = 16;																				\
			amc_point_to_point_records[record_id][3] = 0x00;																			\
			amc_point_to_point_records[record_id][4] = 0x00;																			\
			amc_point_to_point_records[record_id][5] = 0x5A;																			\
			amc_point_to_point_records[record_id][6] = 0x31;																			\
			amc_point_to_point_records[record_id][7] = 0x00;																			\
			amc_point_to_point_records[record_id][8] = 0x19;																			\
			amc_point_to_point_records[record_id][9] = 0x00;																			\
			amc_point_to_point_records[record_id][10] = 0x00;																			\
			amc_point_to_point_records[record_id][11] = 0x80;																			\
			amc_point_to_point_records[record_id][12] = 0x01;																			\
			amc_point_to_point_records[record_id][13] = 0xE0 | port;																	\
			amc_point_to_point_records[record_id][14] = 0xFF;																			\
			amc_point_to_point_records[record_id][15] = 0xFF;																			\
			amc_point_to_point_records[record_id][16] = 0x00;																			\
			amc_point_to_point_records[record_id][17] = 0x01 | (protocol << 4);															\
			amc_point_to_point_records[record_id][18] = protocol_extension;																\
			amc_point_to_point_records[record_id][19] = 0x00;																			\
			amc_point_to_point_records[record_id][20] = 0xFC | matches;
	#endif

	//AMC Clock Configuration record (INIT)
	#ifdef AMC_POINT_TO_POINT_CLOCK_INIT
		#undef INDIRECT_CLOCK_CONNECTION
		#undef DIRECT_CLOCK_CONNECTION

		#define INDIRECT_CLOCK_CONNECTION(connection_id, clock_id, clock_activation_control, PLL, clock_source_receiver, dependant_clock_id)									\
			u08 indirect_ ## connection_id ## _ ## clock_id ## _ ## clock_activation_control ## _array[] = {(PLL << 1)| clock_source_receiver, dependant_clock_id};

		#define DIRECT_CLOCK_CONNECTION(connection_id, clock_id, clock_activation_control, PLL, clock_source_receiver, clock_family, clock_accuracy, freq_Hz, min_Hz, max_Hz)	\
			u08 direct_ ## connection_id ## _ ## clock_id ## _ ## clock_activation_control ## _array[] = {																		\
				(PLL << 1)| clock_source_receiver, 																																\
				clock_family, clock_accuracy, 																																	\
				(u08)(((u32)freq_Hz) & 0xFF), 																															\
				(u08)((((u32)freq_Hz) & 0xFF00) >> 8), 																												\
				(u08)((((u32)freq_Hz) & 0xFF0000) >> 16), 																												\
				(u08)((((u32)freq_Hz) & 0xFF000000) >> 24),																											\
				(u08)(((u32)min_Hz) & 0xFF), 																															\
				(u08)((((u32)min_Hz) & 0xFF00) >> 8), 																													\
				(u08)((((u32)min_Hz) & 0xFF0000) >> 16), 																												\
				(u08)((((u32)min_Hz) & 0xFF000000) >> 24),																												\
				(u08)(((u32)max_Hz) & 0xFF), 																															\
				(u08)((((u32)max_Hz) & 0xFF00) >> 8), 																													\
				(u08)((((u32)max_Hz) & 0xFF0000) >> 16), 																												\
				(u08)((((u32)max_Hz) & 0xFF000000) >> 24),																												\
			};
	#endif

	//AMC Clock Configuration record (SAVE)
	#ifdef AMC_POINT_TO_POINT_CLOCK_SAVE
		#undef INDIRECT_CLOCK_CONNECTION
		#undef DIRECT_CLOCK_CONNECTION

		#define INDIRECT_CLOCK_CONNECTION(connection_id, clock_id, clock_activation_control, PLL, clock_source_receiver, dependant_clock_id)													\
			clock_descriptor_exist[(clock_id-1) + (clock_activation_control*5)] =  1;																											\
			clock_descriptor[(clock_id-1) + (clock_activation_control*5)][0] =  clock_id;																										\
			clock_descriptor[(clock_id-1) + (clock_activation_control*5)][1] =  clock_activation_control;																						\
			indirect_connection_array[(clock_id-1) + (clock_activation_control*5)][connection_id] = indirect_ ## connection_id ## _ ## clock_id ## _ ## clock_activation_control ## _array;

		#define DIRECT_CLOCK_CONNECTION(connection_id, clock_id, clock_activation_control, PLL, clock_source_receiver, clock_family, clock_accuracy, freq_Hz, min_Hz, max_Hz)					\
			clock_descriptor_exist[(clock_id-1) + (clock_activation_control*5)] =  1;																											\
			clock_descriptor[(clock_id-1) + (clock_activation_control*5)][0] =  clock_id;																										\
			clock_descriptor[(clock_id-1) + (clock_activation_control*5)][1] =  clock_activation_control;																						\
			direct_connection_array[(clock_id-1) + (clock_activation_control*5)][connection_id] = direct_ ## connection_id ## _ ## clock_id ## _ ## clock_activation_control ## _array;
	#endif
#endif