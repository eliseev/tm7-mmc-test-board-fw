/*
 * hotswap.c
 *
 * Created: 17/03/2015 15:38:15
 *  Author: jumendez
 */ 
#include <avr/interrupt.h>

#include "avrlibdefs.h"
#include "avrlibtypes.h"
#include "fru.h"
#include "iodeclaration.h"
#include "user_code/user_code.h"
#include "led.h"
#include "sdr.h"
#include "user_code/sensors.h"
#include "hotswap.h"
#include "ipmi_if.h"

u08 handle_switch_value, handle_state_changed = 0;
extern sensor_t sens[SDR_MAX_ID];

enum M_Status hotswapStatus;

// The code of the external interrupt
ISR(INT2_vect){
	sens[HOT_SWAP_SENSOR].value_changed_flag = 1;
}

void hotswap_init(){
	// Set the interrupt mode to logical change for interrupt 7 -- Comment DE: in code the interrupt 2 is being configured!
	// in the external interrupt configuration register
	EICRA |= (1 << ISC20);   // Strange! According to 
	EICRA &= ~(1 << ISC21);  // documentation it is a "reserved" configuration state 

	
	// Allow external interrupt 7
	EIMSK |= (1 << INT2);
	
	sens[HOT_SWAP_SENSOR].value = (inb(LOCAL_HANDLE_SWITCH_PPIN) & BV(LOCAL_HANDLE_SWITCH_PIN)) ? HANDLE_SWITCH_OPENED:HANDLE_SWITCH_CLOSED;	
	sendHotswapEvent(sens[HOT_SWAP_SENSOR].value);
	handle_switch_value = sens[HOT_SWAP_SENSOR].value;
}

u08 event_sent_error = 0;

void hotswap_scanner(){			
	if(sens[HOT_SWAP_SENSOR].value_changed_flag || event_sent_error){
		sens[HOT_SWAP_SENSOR].value = (inb(LOCAL_HANDLE_SWITCH_PPIN) & BV(LOCAL_HANDLE_SWITCH_PIN)) ? HANDLE_SWITCH_OPENED:HANDLE_SWITCH_CLOSED;
		sens[HOT_SWAP_SENSOR].value_changed_flag = 0;
		sendHotswapEvent(sens[HOT_SWAP_SENSOR].value);
	}
}

void sendHotswapEvent(u08 value){
	
	u08 hotswap_module_value;
	
	switch(value){
		case HANDLE_SWITCH_CLOSED: hotswap_module_value = 0x00; break;
		case HANDLE_SWITCH_OPENED: hotswap_module_value = 0x01; break;
		case HOTSWAP_QUIESCED: hotswap_module_value = 0x02; break;
		default: return;
	}
	
	event_sent_error = ipmi_event_send(HOT_SWAP_SENSOR, ASSERTION_EVENT, &hotswap_module_value, 1);	
}