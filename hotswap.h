/*
 * hotswap.h
 *
 * Created: 17/03/2015 15:46:05
 *  Author: jumendez
 */ 


#ifndef HOTSWAP_H_
#define HOTSWAP_H_

#include "fru.h"

#define HANDLE_SWITCH_CLOSED		0x01
#define HANDLE_SWITCH_OPENED		0x00
#define HOTSWAP_QUIESCED			0x02

void hotswap_init();
void hotswap_scanner();
void hotswap_led_management(enum M_Status oldStatus, enum M_Status newStatus);
void sendHotswapEvent(u08 value);

#endif /* HOTSWAP_H_ */