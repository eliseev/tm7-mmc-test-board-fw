/*
 *  Filename    : hpm.c
 *
 *  Title       : HPM.1 interface
 *  Revision    : 1.0
 *  Target MCU	: Atmel AVR series
 *  Description : HPM.1 interface functions
 *
 *  Created		: 18/10/2014
 *
 *  Author		: Julian Mendez <julian.mendez@cern.ch>
 */ 
#include <string.h>
#include <avr/wdt.h>

#include "hpm.h"

#include "user_code_select.h"
#include "i2c.h"
#include "timer.h"
#include "ipmi_if.h"

u08 cmd_in_progress, last_cmd_comp_code, comp_estimate, cmd_in_progress_data[25], cmd_in_progress_data_length; //Get status upgrade cmd informations

extern ipmi_msg_t rqs;
extern u08 rsp_data[MAX_BYTES_READ+5], rsp_data_len;
extern u08 jump_to_bootloader;

u08 hpm_response_send(){	//Return completion code
	
	u08 cc = IPMI_CC_OK;
	u08 i;
	
	if (rqs.netfn != IPMI_GROUP_EXTENSION_NETFN){
		return 0xcc;
	}
	
	switch (rqs.cmd){
		
		case GET_UPGRADE_STATUS:
			
			rsp_data[0] = cmd_in_progress;
			rsp_data[1] = last_cmd_comp_code;
			rsp_data[2] = comp_estimate;
			
			rsp_data_len = 3;
			
			break;
			
		case GET_TARGET_UPGRADE_CAPABILITIES:
			rsp_data[0] = 0x00;		//HPM.1 version
			rsp_data[1] = 0x28;		//Capabilities: Table 3-3 HPM.1 specification
			rsp_data[2] = 24;		//Upgrade timeout: 2 min
			rsp_data[3] = 0x00;		//Self-test is not supported
			rsp_data[4] = 0x00;		//Rollback is not supported
			rsp_data[5] = 12;		//Inaccessibility timeout: 1 min
			rsp_data[6] = 0x01;		//Only one component
			
			rsp_data_len = 7;
			
			break;
			
		case GET_COMPONENT_PROPERTIES:
			if (rqs.data[1] != 0)
				return 0x82;	//Invalid component ID
			
			switch(rqs.data[2]){
				case GENERAL_COMP_PROPERTIES:
					rsp_data[0] = 0x24;		//General component properties
					rsp_data_len = 1;
					break;
					
				case COMP_CURRENT_VERSION:
					rsp_data[0] = MMC_FW_REL_MAJ;	//Firmware revision (Major)
					rsp_data[1] = MMC_FW_REL_MIN;	//Firmware revision (Minor)
					for(i=2; i<6; i++)				//Auxilary Firmware Revision Information
						rsp_data[i] = 0x00;
						
					rsp_data_len = 6;
					break;
					
				case COMP_DESCRIPTION:
					for (i=0; i<12 ; i++){
						if(i< strlen(COMP_DESC_STR))
							rsp_data[i] = COMP_DESC_STR[i];			
						else
							rsp_data[i] = 0;
					}
					
					rsp_data_len = 12;
					
					break;
					
				case COMP_ROLLBACK_FW_VERSION:	//Not implemented yet
					return 0x83;
					
				case COMP_DEFERRED_UPG_FW_VERSION: //Not implemented yet
					return 0x83;	//Invalid component properties selector
					
				//case COMP_OEM_OFFSET:					//No OEM properties implemented yet
				
				default:
					return 0xcc;	//Invalid component properties selector
				
			}
		
			break;
			
		case INITIATE_UPGRADE_ACTION:
			if (rqs.data[1] != 0x00){	//Invalid component
				return 0x81;
			}else if (last_cmd_comp_code == 0x80){	//Command in progress
				return 0x80;
			}
			
			switch (rqs.data[2]){
				case UPGRADE_ACT_BACKUP_COMP: //Not implemented yet
					return 0xCC;
					break;
					
				case UPGRADE_ACT_PREPARE_COMP:	//Not implemented yet
					return 0x00;
					break;
					
				case UPGRADE_ACT_UPLOAD_FOR_UPGRADE:	//Not implemented yet
					jump_to_bootloader = 1;
					last_cmd_comp_code = 0x80;
					cmd_in_progress_data_length = 1;
					cmd_in_progress = INITIATE_UPGRADE_ACTION;
					cmd_in_progress_data[0] = UPGRADE_ACT_UPLOAD_FOR_UPGRADE;
					cc = 0x80;
					break;
					
				case UPGRADE_ACT_UPLOAD_FOR_COMPARE:	//Not implemented yet
					return 0xCC;
					break;
					
				default:
					return 0xCC;
			}
			
			break;
		
		case UPLOAD_FIRMWARE_BLOCK:
			cc = 0x83;		
			break;
			
		case FINISH_FIRMWARE_UPLOAD:
			cc = 0x81;
			break;
			
		default: 
			cc = 0xCC;
	}
	
	return cc;
}

void hpm_long_cmd(){	//called from mmc_main.c - Execute long commands
}