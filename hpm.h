/*
 *  Filename    : hpm.h
 *
 *  Title       : HPM.1 interface header file
 *  Revision    : 1.0
 *  Target MCU	: Atmel AVR series
 *  Description : HPM.1 interface general definitions and structures
 *
 *  Created		: 18/10/2014
 *
 *  Author		: Julian Mendez <julian.mendez@cern.ch>
 */ 


#ifndef HPM_H_
#define HPM_H_

#include "avrlibtypes.h"

//HPM Commands (PIGMG)
#define GET_TARGET_UPGRADE_CAPABILITIES		0x2E	//Mandatory
#define GET_COMPONENT_PROPERTIES			0x2F	//Mandatory
#define ABORT_FIRMWARE_UPGRADE				0x30	//Optional
#define INITIATE_UPGRADE_ACTION				0x31	//Mandatory
#define UPLOAD_FIRMWARE_BLOCK				0x32	//Mandatory
#define FINISH_FIRMWARE_UPLOAD				0x33	//Mandatory
#define GET_UPGRADE_STATUS					0x34	//Mandatory
#define ACTIVE_FIRMWARE						0x35	//Mandatory
#define QUERY_SELF_TEST_RESULTS				0x36	//Optional (v1.0): Self-test not implemented / Mandatory (v2.0): Self-test implemented
#define QUERY_ROLLBACK_STATUS				0x37	//Optional (v1.0): Rollback not implemented / Mandatory (v2.0): Rollback implemented
#define INITIATE_MANUAL_ROLLBACK			0x38	//Optional (v1.0): Rollback not implemented / Mandatory (v2.0): Rollback implemented

//COMPONENT PROPERTIES
#define GENERAL_COMP_PROPERTIES				0x00
#define COMP_CURRENT_VERSION				0x01
#define COMP_DESCRIPTION					0x02
#define COMP_ROLLBACK_FW_VERSION			0x03
#define COMP_DEFERRED_UPG_FW_VERSION		0x04
#define COMP_OEM_OFFSET						0xC0

//UPGRADE ACTION
#define UPGRADE_ACT_BACKUP_COMP				0x00
#define UPGRADE_ACT_PREPARE_COMP			0x01
#define UPGRADE_ACT_UPLOAD_FOR_UPGRADE		0x02
#define UPGRADE_ACT_UPLOAD_FOR_COMPARE		0x03

//Firmware description (Should be user specific): MAX 12 bytes
#define COMP_DESC_STR	 "CERN MMC"

//Functions prototype
u08 hpm_response_send();
void hpm_long_cmd();

#endif /* HPM_H_ */