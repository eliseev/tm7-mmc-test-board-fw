/*
 * iodeclaration.h
 *
 * Created: 19/03/2015 18:27:38
 *  Author: jumendez
 */ 


#ifndef IODECLARATION_H_
#define IODECLARATION_H_

#include "avrlibdefs.h"
#include "avrlibtypes.h"

//Port selection define
#define MMCPORT_A						0
#define MMCPORT_B						1
#define MMCPORT_C						2
#define MMCPORT_D						3
#define MMCPORT_E						4
#define MMCPORT_F						5
#define MMCPORT_G						6

//IO state
#define HIGH							1
#define LOW								0
#define ACTIVE							0xFF
#define INACTIVE						0x00

//IO Direction
#define INPUT							0x00
#define OUTPUT							0x01

//IO Type
#define MMC_PORT						0
#define IO_EXTENDER_PCF8574AT			1

typedef struct {
	unsigned char port;
	unsigned char pin;
	unsigned char active;
	unsigned char inactive;
}signal_t;

/** General pins declaration */
#define LOCAL_LOW_VOLTAGE_POK_PORT	MMCPORT_A
#define LOCAL_LOW_VOLTAGE_POK_PPORT	PORTA
#define LOCAL_LOW_VOLTAGE_POK_PPIN	PINA
#define LOCAL_LOW_VOLTAGE_POK_PIN	PA0

#define RTM_PS_PORT					MMCPORT_A
#define RTM_PS_PPORT				PORTA
#define RTM_PS_PPIN					PINA
#define RTM_PS_PIN					PA2

#define RTM_12V_ENABLE_PORT			MMCPORT_A
#define RTM_12V_ENABLE_PPORT		PORTA
#define RTM_12V_ENABLE_PPIN			PINA
#define RTM_12V_ENABLE_PIN			PA3

#define RTM_3V3_ENABLE_PORT			MMCPORT_A
#define RTM_3V3_ENABLE_PPORT		PORTA
#define RTM_3V3_ENABLE_PPIN			PINA
#define RTM_3V3_ENABLE_PIN			PA4

#define RTM_I2C_ENABLE_PORT			MMCPORT_A
#define RTM_I2C_ENABLE_PPORT		PORTA
#define RTM_I2C_ENABLE_PPIN			PINA
#define RTM_I2C_ENABLE_PIN			PA5

#define GA_PULLUP_PORT				MMCPORT_B
#define GA_PULLUP_PPORT				PORTB
#define GA_PULLUP_PPIN				PINB
#define GA_PULLUP_PIN				PB0

#define GA0_PORT					MMCPORT_B
#define GA0_PPORT					PORTB
#define GA0_PPIN					PINB
#define GA0_PIN						PB3

#define GA1_PORT					MMCPORT_B
#define GA1_PPORT					PORTB
#define GA1_PPIN					PINB
#define GA1_PIN						PB2

#define GA2_PORT					MMCPORT_B
#define GA2_PPORT					PORTB
#define GA2_PPIN					PINB
#define GA2_PIN						PB1

#define GA_DDR						DDRB

#define LOCAL_RED_LED_PORT			MMCPORT_B
#define LOCAL_RED_LED_PPORT			PORTB
#define LOCAL_RED_LED_PPIN			PINB
#define LOCAL_RED_LED_PIN			PB5

#define LOCAL_GREEN_LED_PORT		MMCPORT_B
#define LOCAL_GREEN_LED_PPORT		PORTB
#define LOCAL_GREEN_LED_PPIN		PINB
#define LOCAL_GREEN_LED_PIN			PB6

#define LOCAL_BLUE_LED_PORT			MMCPORT_B
#define LOCAL_BLUE_LED_PPORT		PORTB
#define LOCAL_BLUE_LED_PPIN			PINB
#define LOCAL_BLUE_LED_PIN			PB7

#define LOCAL_RESET_FPGA_PORT		MMCPORT_C
#define LOCAL_RESET_FPGA_PPORT		PORTC
#define LOCAL_RESET_FPGA_PPIN		PINC
#define LOCAL_RESET_FPGA_PIN		PC2

#define LOCAL_RELOAD_FPGA_PORT		MMCPORT_C
#define LOCAL_RELOAD_FPGA_PPORT		PORTC
#define LOCAL_RELOAD_FPGA_PPIN		PINC
#define LOCAL_RELOAD_FPGA_PIN		PC3

#define LOCAL_FPGA2_INIT_DONE_PORT	MMCPORT_C
#define LOCAL_FPGA2_INIT_DONE_PPORT	PORTC
#define LOCAL_FPGA2_INIT_DONE_PPIN	PINC
#define LOCAL_FPGA2_INIT_DONE_PIN	PC4

#define LOCAL_FPGA1_INIT_DONE_PORT	MMCPORT_C
#define LOCAL_FPGA1_INIT_DONE_PPORT	PORTC
#define LOCAL_FPGA1_INIT_DONE_PPIN	PINC
#define LOCAL_FPGA1_INIT_DONE_PIN	PC5

#define LOCAL_REG_ENABLE_PORT		MMCPORT_C
#define LOCAL_REG_ENABLE_PPORT		PORTC
#define LOCAL_REG_ENABLE_PPIN		PINC
#define LOCAL_REG_ENABLE_PIN		PC6

#define LOCAL_DCDC_ENABLE_PORT		MMCPORT_C
#define LOCAL_DCDC_ENABLE_PPORT		PORTC
#define LOCAL_DCDC_ENABLE_PPIN		PINC
#define LOCAL_DCDC_ENABLE_PIN		PC7

#define IPMB_SCL_PORT				MMCPORT_D
#define IPMB_SCL_PPORT				PORTD
#define IPMB_SCL_PPIN				PIND
#define IPMB_SCL_PIN				PD0

#define IPMB_SDA_PORT				MMCPORT_D
#define IPMB_SDA_PPORT				PORTD
#define IPMB_SDA_PPIN				PIND
#define IPMB_SDA_PIN				PD1

#define LOCAL_HANDLE_SWITCH_PORT	MMCPORT_D
#define LOCAL_HANDLE_SWITCH_PPORT	PORTD
#define LOCAL_HANDLE_SWITCH_PPIN	PIND
#define LOCAL_HANDLE_SWITCH_PIN		PD2

#define LOCAL_I2C_SCL_PORT			MMCPORT_D
#define LOCAL_I2C_SCL_PPORT			PORTD
#define LOCAL_I2C_SCL_PPIN			PIND
#define LOCAL_I2C_SCL_PIN			PD4

#define LOCAL_I2C_SDA_PORT			MMCPORT_D
#define LOCAL_I2C_SDA_PPORT			PORTD
#define LOCAL_I2C_SDA_PPIN			PIND
#define LOCAL_I2C_SDA_PIN			PD5

#define LOCAL_I2C_DDR				DDRD

#define PS1_PORT					MMCPORT_E
#define PS1_PPORT					PORTE
#define PS1_PPIN					PINE
#define PS1_PIN						PE2

#define PS0_PORT					MMCPORT_E
#define PS0_PPORT					PORTE
#define PS0_PPIN					PINE
#define PS0_PIN						PE3

#define PRESENCE_12V_PORT			MMCPORT_F
#define PRESENCE_12V_PPORT			PORTF
#define PRESENCE_12V_PPIN			PINF
#define PRESENCE_12V_PIN			PF0

#define MASTER_TCK_PORT				MMCPORT_G
#define MASTER_TCK_PPORT			PORTG
#define MASTER_TCK_PPIN				PING
#define MASTER_TCK_PIN				PG0

#define MASTER_TMS_PORT				MMCPORT_G
#define MASTER_TMS_PPORT			PORTG
#define MASTER_TMS_PPIN				PING
#define MASTER_TMS_PIN				PG1

#define MASTER_TDO_PORT				MMCPORT_G
#define MASTER_TDO_PPORT			PORTG
#define MASTER_TDO_PPIN				PING
#define MASTER_TDO_PIN				PG2

#define MASTER_TDI_PORT				MMCPORT_G
#define MASTER_TDI_PPORT			PORTG
#define MASTER_TDI_PPIN				PING
#define MASTER_TDI_PIN				PG3

/** User pins declaration */
#define GPIO_0_PORT					MMCPORT_C
#define GPIO_0_PPORT				PORTC
#define GPIO_0_PPIN					PINC
#define GPIO_0_PIN					PC2

#define GPIO_1_PORT					MMCPORT_C
#define GPIO_1_PPORT				PORTC
#define GPIO_1_PPIN					PINC
#define GPIO_1_PIN					PC4

#define GPIO_2_PORT					MMCPORT_C
#define GPIO_2_PPORT				PORTC
#define GPIO_2_PPIN					PINC
#define GPIO_2_PIN					PC0

#define GPIO_3_PORT					MMCPORT_C
#define GPIO_3_PPORT				PORTC
#define GPIO_3_PPIN					PINC
#define GPIO_3_PIN					PC5

#define GPIO_4_PORT					MMCPORT_C
#define GPIO_4_PPORT				PORTC
#define GPIO_4_PPIN					PINC
#define GPIO_4_PIN					PC6

#define GPIO_5_PORT					MMCPORT_C
#define GPIO_5_PPORT				PORTC
#define GPIO_5_PPIN					PINC
#define GPIO_5_PIN					PC7

#define GPIO_6_PORT					MMCPORT_F
#define GPIO_6_PPORT				PORTF
#define GPIO_6_PPIN					PINF
#define GPIO_6_PIN					PF1

#define GPIO_7_PORT					MMCPORT_F
#define GPIO_7_PPORT				PORTF
#define GPIO_7_PPIN					PINF
#define GPIO_7_PIN					PF2

#define GPIO_8_PORT					MMCPORT_F
#define GPIO_8_PPORT				PORTF
#define GPIO_8_PPIN					PINF
#define GPIO_8_PIN					PF3

#define GPIO_9_PORT					MMCPORT_E
#define GPIO_9_PPORT				PORTE
#define GPIO_9_PPIN					PINE
#define GPIO_9_PIN					PE6

#define GPIO_10_PORT				MMCPORT_E
#define GPIO_10_PPORT				PORTE
#define GPIO_10_PPIN				PINE
#define GPIO_10_PIN					PE4

#define GPIO_11_PORT				MMCPORT_E
#define GPIO_11_PPORT				PORTE
#define GPIO_11_PPIN				PINE
#define GPIO_11_PIN					PE5

#define GPIO_12_PORT				MMCPORT_E
#define GPIO_12_PPORT				PORTE
#define GPIO_12_PPIN				PINE
#define GPIO_12_PIN					PE7


#endif /* IODECLARATION_H_ */