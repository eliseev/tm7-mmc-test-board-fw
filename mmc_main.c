//*****************************************************************************
// Copyright (C) 2007 DESY(Deutsches-Elektronen Synchrotron) 
//
//File Name	: mmc_main.c
// 
// Title		: mmc main file
// Revision		: 1.0
// Notes		:	
// Target MCU	: Atmel AVR series
//
// Author       : Vahan Petrosyan (vahan_petrosyan@desy.de)
//
// Modified by  : Frederic Bompard, CPPM (Centre Physique des Particules de Marseille)
// Modified by  : Markus Joos (markus.joos@cern.ch)
//
// Description : The main routine for MMC(Module Management Controller).
//
// This code is distributed under the GNU Public License
// which can be found at http://www.gnu.org/licenses/gpl.txt
//*****************************************************************************




//MJ: Files that are still to be reviewed:
// jtag.c & .h


// Include Files
#include <avr/io.h>		    // include I/O definitions (port names, pin names, etc)
#include <avr/interrupt.h>	// include interrupt support
#include <avr/sleep.h>
#include <avr/wdt.h>

#include "global.h"	    	// include our global settings
#include "timer.h"		    // include timer function library
#include "ipmi_if.h"	    // ipmi interface
#include "led.h"
#include "rtm_mng.h"
#include "fru.h"
#include "avrlibdefs.h"
#include "project.h"
#include "user_code/sensors.h"
#include "user_code/config.h"
#include "user_code_select.h"
#include "toolfunction.h"
#include "hpm.h"
#include "hotswap.h"
#include "payload.h"
#include "a2d.h"
#include "eeprom.h"
#include "i2csw.h"
#include "sdr.h"

// Constants   //MJ: there is no mmc_main.h file yet

#define HOTSWAP_EVENT 0x01
#define TIMER_EVENT   0x02

#define BLINK_LONG	  10
#define BLINK_SHORT	  2

#define BOOTLOADER_START					0x1E000

#define DEACTIVATED		0
#define ACTIVATED		1

// External variables
extern u08 ipmb_address;
#ifdef USE_RTM
	extern sensor_t sens[NUM_SENSOR_AMC + NUM_SENSOR_RTM];
#else
	extern sensor_t sens[SDR_MAX_ID];
#endif
extern volatile ipmi_status status_bits;
//extern leds led[NUM_OF_LED];


// Global variables
volatile u08 event = 0;
u08 rtm_sensor_enable = 0;
u08 rtm_hs_enable = 0;
u08 HotSwap, start_crate = 0, blink, flag_blink;

// Function prototypes
void timer_event(void);
void init_port(void);

#ifdef LED_DEFINITION_ENABLE
	#undef NUM_OF_AMC_LED
	#define NUM_OF_AMC_LED	3+AMC_USER_LED_CNT
#endif
extern leds amc_led[NUM_OF_AMC_LED];
extern u08 activation_locked, deactivation_locked;

u08 jump_to_bootloader = 0;
u08 deactivation_state = 0;

u08 fpga_done = 0;
u08 reset_in = 0;
u08 pll_reset_in_pipe = 0;

//Debug vars for TM7-MMC dummy
#define RELAY_PERIOD 33 // x100ms
u08 relay_delay = 0;


//***********/
int main(void)
//***********/
{
	
	volatile u08 state = DEACTIVATED;
	
	
	#ifdef USE_USER_GEOG_ADDR
		u08 ipmi_spec_addr[] = {0x72, 0x74, 0x76, 0x78, 0x7A, 0x7C, 0x7E, 0x80, 0x82, 0x84, 0x86, 0x88, 0x70};
	#endif		
	
	wdt_disable();	//Disable recovery mode (HPM)
	force_app_start();	//Used in the boot loader mode: Provide the state
	
    // INIT

    // port pins inputs with pull-ups and outputs at startup
    init_port();

/////////Debug snippet for the TM7-MMC Dummy board (init relay control output) //////////////////////////////////////
	DDRC |= (0x01 << PC1);
	PORTC &= ~(0x01 << PC1);
	
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	
//    if (EEPROM_read(0) == 0xff)   // MJ: Should we really load the EEPROM in the MMC code or should this be done once at production time?
        Write_FRU_Info_default(); // Write default value < FRU_information > in EEPROM


    if ((inb(ACSR) & BV(ACO)))    // check if the board is in the crate
    {
        ipmi_init();
        sdr_init(ipmb_address);
    }
    sbi(ACSR, ACD);

    sensor_init();                // init sensor
    timerInit();                  // init timer
	
    i2cswInit();
    leds_init();

    timerAttach(timer_event);	

	hotswap_init();

    sei();                        // enable all interrupts
    // end INIT

	#ifdef USE_USER_GEOG_ADDR
	if(!isinarray(ipmb_address, ipmi_spec_addr, 13)){
		return user_main();
	}
	#endif
	
	wdt_enable(WDTO_2S);          // enable watchdog at 1 second
		
    while(1)
    {		
		wdt_reset();              // reset the watchdog
		
		//Get IPMI request
        ipmi_check_request();     // check if ipmi request <<data in from ipmi>>		
		
		//Bootloader jump - HPM feature
		if(jump_to_bootloader){
			force_bootloader_start();
			while(1);	//Watchdog reset
		}
		
		//if(a2dConvert8bit(0) > 0x82)			local_led_control(GREEN_LED, LED_ON);
		if(sens[VOLT_12].value > 0x82)			
			local_led_control(GREEN_LED, LED_ON);
		else									
			local_led_control(GREEN_LED, LED_OFF);
		

		if(state == DEACTIVATED && sens[VOLT_12].value > 0x82){
			state = ACTIVATED;
			wdt_disable();
			active_payload();
			wdt_enable(WDTO_2S); 

		}else if(state == ACTIVATED && sens[VOLT_12].value <= 0x40){
			state = DEACTIVATED;
		}
		
		if(deactivation_state){
			deactivation_state = 0;
			desactive_payload();
			sens[HOT_SWAP_SENSOR].value = HOTSWAP_QUIESCED;
			sendHotswapEvent(HOTSWAP_QUIESCED);
		}
			
		//Polled tasks				
        if (event & TIMER_EVENT)           // all 100 ms
        {
            ipmi_check_event_respond();    // respond to ipmi - JM: should be modified
            sensor_monitoring_user();      // check sensors
            			
		
			fpga_done = (inb(LOCAL_FPGA1_INIT_DONE_PPIN) & BV(LOCAL_FPGA1_INIT_DONE_PIN)) ? 1 : 0;
			reset_in = (inb(GPIO_11_PPIN) & BV(GPIO_11_PIN)) ? 1 : 0;
			pll_reset_in_pipe = (pll_reset_in_pipe << 1) + reset_in;
			if (pll_reset_in_pipe == 0x01)
			{
				local_led_control(RED_LED, LED_ON);
				//PLL_restart_120();
				PLL_config_LHC_40();
				//PLL_config_320();
				local_led_control(RED_LED, LED_OFF);
				//pll_reset_in_pipe = 0X00;
			}
			//else	local_led_control(RED_LED, LED_OFF);
			//} 
			event = 0;
/////////Debug snippet for the TM7-MMC Dummy board switching the relay //////////////////////////////////////////////
			if (relay_delay==RELAY_PERIOD)
			{
				if (state == ACTIVATED) 
					PORTC= (PORTC&(0x01 << PC1))? PORTC&~(0x01 << PC1) : PORTC|(0x01 << PC1) ; //switch relay 
				else 
					PORTC &= ~(0x01 << PC1);   //switch off the relay if the board is inactive
					
				relay_delay = 0;
			}
			else
				relay_delay++;
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		}
        
        //set_sleep_mode(SLEEP_MODE_IDLE);   // put the MCU to sleep
        //sleep_mode();
    }
    return 0;
}


//***************/
void timer_event()   //Only called once from mmc_main.c
//***************/
{
    static u08 timer = 0;

    timer++;
	
    if (timer == 100){
        event |= TIMER_EVENT;
        timer = 0;
	}
}


//*****************/
void init_port(void)    //Only called once from mmc_main.c; could be in-lined  //MJ-VB: How much of this is user code?
//*****************/
{
	//Definition of DDR (Data Direction Register)
	//bit = 0: This line is an input line
	//bit = 1: This line is an output line
	//
	//Definition of PORT
	//If line is output and PORT bit = 0: Send logical 0
	//If line is output and PORT bit = 1: Send logical 1
	//If line is input and PORT bit = 0:  Disable the pull-up of this line
	//If line is input and PORT bit = 1:  Enable the pull-up of this line
	//
	//Ownership of the ports and their lines
	//G = This bit is part of the generic infrastructure
	//U = This bit is user defined
	//N = This bit is not used or does not exist
	//Example: GGGUUUNN
	//Bits 7-5 are "G"
	//Bits 4-2 are "U"
	//Bits 1-0 are "N"

	//The code below initialises the ports that contain "G" bits. User bits in these ports will be defined later in the user code section

	//Port A: GGGGGGGG
	DDRA  = 0x00;
	PORTA = 0x00;

	//Port B: GGGGGGGG
	DDRB  = 0xE1;
	PORTB = 0x00;

	//Port C: UUUUUUUU
	DDRC = 0x00;
	PORTC = 0x00;
	
	//Port D: GGGGGGGG
	DDRD  = 0x00;
	PORTD = 0x00;

	//Port E: UUUUGGNN
	DDRE  = 0x00;
	PORTE = 0x00;

	//Port F: GGGGUUUG
	DDRF  = 0x00;
	PORTF = 0x00;
	
	//Port G: NNNNUUUU
	DDRG  = 0x00;
	PORTG = 0x00;
	
	#ifdef USERGPIO_DEF_ENABLE
		#ifdef GPIO0_DIR
			#if GPIO0_DIR == INPUT
				DDRC &= ~(0x01 << PC2);
			#else
				DDRC |= 0x01 << PC2;
			#endif
		#endif
		
		#ifdef GPIO1_DIR
			#if GPIO1_DIR == INPUT
				DDRC &= ~(0x01 << PC4);
			#else
				DDRC |= 0x01 << PC4;
			#endif
		#endif
		
		#ifdef GPIO2_DIR
			#if GPIO2_DIR == INPUT
				DDRC &= ~(0x01 << PC0);
			#else
				DDRC |= 0x01 << PC0;
			#endif
		#endif
		
		#ifdef GPIO3_DIR
			#if GPIO3_DIR == INPUT
				DDRC &= ~(0x01 << PC5);
			#else
				DDRC |= 0x01 << PC5;
			#endif
		#endif
		
		#ifdef GPIO4_DIR
			#if GPIO4_DIR == INPUT
				DDRC &= ~(0x01 << PC6);
			#else
				DDRC |= 0x01 << PC6;
			#endif
		#endif
		
		#ifdef GPIO5_DIR
			#if GPIO5_DIR == INPUT
				DDRC &= ~(0x01 << PC7);
			#else
				DDRC |= 0x01 << PC7;
			#endif
		#endif
		
		#ifdef GPIO6_DIR
			#if GPIO6_DIR == INPUT
				DDRF &= ~(0x01 << PF1);
			#else
				DDRF |= 0x01 << PF1;
			#endif
		#endif
		
		#ifdef GPIO7_DIR
			#if GPIO7_DIR == INPUT
				DDRF &= ~(0x01 << PF2);
			#else
				DDRF |= 0x01 << PF2;
			#endif
		#endif
		
		#ifdef GPIO8_DIR
			#if GPIO8_DIR == INPUT
				DDRF &= ~(0x01 << PF3);
			#else
				DDRF |= 0x01 << PF3;
			#endif
		#endif
		
		#ifdef GPIO9_DIR
			#if GPIO9_DIR == INPUT
				DDRE &= ~(0x01 << PE6);
			#else
				DDRE |= 0x01 << PE6;
			#endif
		#endif
		
		#ifdef GPIO10_DIR
			#if GPIO10_DIR == INPUT
				DDRE &= ~(0x01 << PE4);
			#else
				DDRE |= 0x01 << PE4;
			#endif
		#endif
		
		#ifdef GPIO11_DIR
			#if GPIO11_DIR == INPUT
				DDRE &= ~(0x01 << PE5);
			#else
				DDRE |= 0x01 << PE5;
			#endif
		#endif
		
		#ifdef GPIO12_DIR
			#if GPIO12_DIR == INPUT
				DDRE &= ~(0x01 << PE7);
			#else
				DDRE |= 0x01 << PE7;
			#endif
		#endif
		
		#ifdef GPIO13_DIR
			#if GPIO13_DIR == INPUT
				DDRA &= ~(0x01 << PA3);
			#else
				DDRA |= 0x01 << PA3;
			#endif
		#endif
		
		#ifdef GPIO14_DIR
			#if GPIO14_DIR == INPUT
				DDRA &= ~(0x01 << PA4);
			#else
				DDRA |= 0x01 << PA4;
			#endif
		#endif
		
		#ifdef GPIO15_DIR
			#if GPIO15_DIR == INPUT
				DDRA &= ~(0x01 << PA5);
			#else
				DDRA |= 0x01 << PA5;
			#endif
		#endif
		
		
	#else
		if (USER_CODE == 2)		init_port_user();  //Now the user can overlay his requirements but should respect the "G" bits
	#endif
}