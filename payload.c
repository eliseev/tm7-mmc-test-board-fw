/*
 * payload.c
 *
 * Created: 19/03/2015 18:28:56
 *  Author: jumendez
 */ 
#include <avr/io.h>		    // include I/O definitions (port names, pin names, etc)
#include <avr/wdt.h>

#include "iodeclaration.h"
#include "user_code/config.h"
#include "user_code/user_code.h"
#include "user_code/sensors.h"
#include "sdr.h"
#include "hotswap.h"

#include "payload.h"

extern u08 deactivation_state;
extern sensor_t sens[SDR_MAX_ID];

u08 active_payload(){
	
	int i = 0;	
	POWER_ON_SEQ	

	return 0;
}

u08 desactive_payload(){
	
	int i = 0;
	POWER_OFF_SEQ
	
	return 0;
}


u08 ipmi_picmg_fru_control(u08 control_type){
	
	int i = 0;
	
	
	switch (control_type){
		case FRU_COLD_RESET:
			#ifdef COLD_RESET_SEQ
				COLD_RESET_SEQ
			#else
				return 0xFF;
			#endif
			break;
			
		case FRU_WARM_RESET:
			#ifdef WARM_RESET_SEQ
				WARM_RESET_SEQ
			#else
				return 0xFF;
			#endif
			break;
			
		case FRU_REBOOT:
			#ifdef REBOOT_SEQ
				REBOOT_SEQ
			#else
				return 0xFF;
			#endif
			break;
			
		default:
			return(0xff);
			break;
    }

    return(0);
}

u08 ipmi_picmg_get_fru_control_capabilities(){
	/*
	FRU Control Capabilities Mask.
		[4-7] - Reserved
		[3] - 1b - Capable of issuing a diagnostic interrupt
		[2] - 1b - Capable of issuing a graceful reboot
		[1] - 1b - Capable of issuing a warm reset
		[0] - Reserved
	*/
	u08 capa = 0;
	
	#ifdef REBOOT_SEQ
		capa |= 0x04;
	#endif
	
	#ifdef WARM_RESET_SEQ
		capa |= 0x02;
	#endif
	
	return capa;
}