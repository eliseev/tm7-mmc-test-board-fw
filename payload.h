/*
 * payload.h
 *
 * Created: 19/03/2015 18:24:46
 *  Author: jumendez
 */ 


#ifndef PAYLOAD_H_
#define PAYLOAD_H_

#include "iodeclaration.h"
#include "timer.h"
/*
#define SIG(id, port_d, pin_d, active_d, inactive_d)		signal_t payload_ ## id ## _signal = {		\
		port : port_d,																					\
		pin : pin_d,																					\
		active : active_d,																				\
		inactive : inactive_d,																			\
	};
	

#define ACTIVE_SIGNAL(id)																													\
	if(payload_ ## id ## _signal.active == HIGH){																							\
		switch(payload_ ## id ## _signal.port){																								\
			case MMCPORT_A:	sbi(PORTA, payload_ ## id ## _signal.pin);	break;																\
			case MMCPORT_B:	sbi(PORTB, payload_ ## id ## _signal.pin);	break;																\
			case MMCPORT_C:	sbi(PORTC, payload_ ## id ## _signal.pin);	break;																\
			case MMCPORT_D:	sbi(PORTD, payload_ ## id ## _signal.pin);	break;																\
			case MMCPORT_E:	sbi(PORTE, payload_ ## id ## _signal.pin);	break;																\
			case MMCPORT_F:	sbi(PORTF, payload_ ## id ## _signal.pin);	break;																\
			case MMCPORT_G:	sbi(PORTG, payload_ ## id ## _signal.pin);	break;																\
		}																																	\
	}else{																																	\
		switch(payload_ ## id ## _signal.port){																								\
			case MMCPORT_A:	cbi(PORTA, payload_ ## id ## _signal.pin);	break;																\
			case MMCPORT_B:	cbi(PORTB, payload_ ## id ## _signal.pin);	break;																\
			case MMCPORT_C:	cbi(PORTC, payload_ ## id ## _signal.pin);	break;																\
			case MMCPORT_D:	cbi(PORTD, payload_ ## id ## _signal.pin);	break;																\
			case MMCPORT_E:	cbi(PORTE, payload_ ## id ## _signal.pin);	break;																\
			case MMCPORT_F:	cbi(PORTF, payload_ ## id ## _signal.pin);	break;																\
			case MMCPORT_G:	cbi(PORTG, payload_ ## id ## _signal.pin);	break;																\
		}																																	\
	}

#define DESACTIVE_SIGNAL(id)																												\
	if(payload_ ## id ## _signal.inactive == HIGH){																							\
		switch(payload_ ## id ## _signal.port){																								\
			case MMCPORT_A:	sbi(PORTA, payload_ ## id ## _signal.pin);	break;																\
			case MMCPORT_B:	sbi(PORTB, payload_ ## id ## _signal.pin);	break;																\
			case MMCPORT_C:	sbi(PORTC, payload_ ## id ## _signal.pin);	break;																\
			case MMCPORT_D:	sbi(PORTD, payload_ ## id ## _signal.pin);	break;																\
			case MMCPORT_E:	sbi(PORTE, payload_ ## id ## _signal.pin);	break;																\
			case MMCPORT_F:	sbi(PORTF, payload_ ## id ## _signal.pin);	break;																\
			case MMCPORT_G:	sbi(PORTG, payload_ ## id ## _signal.pin);	break;																\
		}																																	\
	}else{																																	\
		switch(payload_ ## id ## _signal.port){																								\
			case MMCPORT_A:	cbi(PORTA, payload_ ## id ## _signal.pin);	break;																\
			case MMCPORT_B:	cbi(PORTB, payload_ ## id ## _signal.pin);	break;																\
			case MMCPORT_C:	cbi(PORTC, payload_ ## id ## _signal.pin);	break;																\
			case MMCPORT_D:	cbi(PORTD, payload_ ## id ## _signal.pin);	break;																\
			case MMCPORT_E:	cbi(PORTE, payload_ ## id ## _signal.pin);	break;																\
			case MMCPORT_F:	cbi(PORTF, payload_ ## id ## _signal.pin);	break;																\
			case MMCPORT_G:	cbi(PORTG, payload_ ## id ## _signal.pin);	break;																\
		}																																	\
	}
	
#define DELAY(ms)		delay_ms(ms);

#define WAIT_FOR_ACTIVE(id, timeout, error)																									\
	switch(payload_ ## id ## _signal.port){																									\
		case MMCPORT_A:																														\
			for(i=0; i < timeout && ((inb(PINA) & BV(payload_ ## id ## _signal.pin)) ? HIGH:LOW) != payload_ ## id ## _signal.active; i++)	\
				delay_ms(1);																												\
			if(((inb(PINA) & BV(payload_ ## id ## _signal.pin)) ? HIGH:LOW) != payload_ ## id ## _signal.active){							\
				error																														\
			}																																\
			break;																															\
		case MMCPORT_B:																														\
			for(i=0; i < timeout && ((inb(PINB) & BV(payload_ ## id ## _signal.pin)) ? HIGH:LOW) != payload_ ## id ## _signal.active; i++)	\
				delay_ms(1);																												\
			if(((inb(PINB) & BV(payload_ ## id ## _signal.pin)) ? HIGH:LOW) != payload_ ## id ## _signal.active){							\
				error																														\
			}																																\
			break;																															\
		case MMCPORT_C:																														\
			for(i=0; i < timeout && ((inb(PINC) & BV(payload_ ## id ## _signal.pin)) ? HIGH:LOW) != payload_ ## id ## _signal.active; i++)	\
				delay_ms(1);																												\
			if(((inb(PINC) & BV(payload_ ## id ## _signal.pin)) ? HIGH:LOW) != payload_ ## id ## _signal.active){							\
				error																														\
			}																																\
			break;																															\
		case MMCPORT_D:																														\
			for(i=0; i < timeout && ((inb(PIND) & BV(payload_ ## id ## _signal.pin)) ? HIGH:LOW) != payload_ ## id ## _signal.active; i++)	\
				delay_ms(1);																												\
			if(((inb(PIND) & BV(payload_ ## id ## _signal.pin)) ? HIGH:LOW) != payload_ ## id ## _signal.active){							\
				error																														\
			}																																\
			break;																															\
		case MMCPORT_E:																														\
			for(i=0; i < timeout && ((inb(PINE) & BV(payload_ ## id ## _signal.pin)) ? HIGH:LOW) != payload_ ## id ## _signal.active; i++)	\
				delay_ms(1);																												\
			if(((inb(PINE) & BV(payload_ ## id ## _signal.pin)) ? HIGH:LOW) != payload_ ## id ## _signal.active){							\
				error																														\
			}																																\
			break;																															\
		case MMCPORT_F:																														\
			for(i=0; i < timeout && ((inb(PINF) & BV(payload_ ## id ## _signal.pin)) ? HIGH:LOW) != payload_ ## id ## _signal.active; i++)	\
				delay_ms(1);																												\
			if(((inb(PINF) & BV(payload_ ## id ## _signal.pin)) ? HIGH:LOW) != payload_ ## id ## _signal.active){							\
				error																														\
			}																																\
			break;																															\
		case MMCPORT_G:																														\
			for(i=0; i < timeout && ((inb(PING) & BV(payload_ ## id ## _signal.pin)) ? HIGH:LOW) != payload_ ## id ## _signal.active; i++)	\
				delay_ms(1);																												\
			if(((inb(PING) & BV(payload_ ## id ## _signal.pin)) ? HIGH:LOW) != payload_ ## id ## _signal.active){							\
				error																														\
			}																																\
			break;																															\
	}
	
#define WAIT_FOR_INACTIVE(id, timeout, error)																								\
	switch(payload_ ## id ## _signal.port){																									\
		case MMCPORT_A:																														\
			for(i=0; i < timeout && ((inb(PINA) & BV(payload_ ## id ## _signal.pin)) ? HIGH:LOW) != payload_ ## id ## _signal.inactive; i++)\
				delay_ms(1);																												\
			if(((inb(PINA) & BV(payload_ ## id ## _signal.pin)) ? HIGH:LOW) != payload_ ## id ## _signal.inactive){							\
				error																														\
			}																																\
			break;																															\
		case MMCPORT_B:																														\
			for(i=0; i < timeout && ((inb(PINB) & BV(payload_ ## id ## _signal.pin)) ? HIGH:LOW) != payload_ ## id ## _signal.inactive; i++)\
				delay_ms(1);																												\
			if(((inb(PINB) & BV(payload_ ## id ## _signal.pin)) ? HIGH:LOW) != payload_ ## id ## _signal.inactive){							\
				error																														\
			}																																\
			break;																															\
		case MMCPORT_C:																														\
			for(i=0; i < timeout && ((inb(PINC) & BV(payload_ ## id ## _signal.pin)) ? HIGH:LOW) != payload_ ## id ## _signal.inactive; i++)\
				delay_ms(1);																												\
			if(((inb(PINC) & BV(payload_ ## id ## _signal.pin)) ? HIGH:LOW) != payload_ ## id ## _signal.inactive){							\
				error																														\
			}																																\
			break;																															\
		case MMCPORT_D:																														\
			for(i=0; i < timeout && ((inb(PIND) & BV(payload_ ## id ## _signal.pin)) ? HIGH:LOW) != payload_ ## id ## _signal.inactive; i++)\
				delay_ms(1);																												\
			if(((inb(PIND) & BV(payload_ ## id ## _signal.pin)) ? HIGH:LOW) != payload_ ## id ## _signal.inactive){							\
				error																														\
			}																																\
			break;																															\
		case MMCPORT_E:																														\
			for(i=0; i < timeout && ((inb(PINE) & BV(payload_ ## id ## _signal.pin)) ? HIGH:LOW) != payload_ ## id ## _signal.inactive; i++)\
				delay_ms(1);																												\
			if(((inb(PINE) & BV(payload_ ## id ## _signal.pin)) ? HIGH:LOW) != payload_ ## id ## _signal.inactive){							\
				error																														\
			}																																\
			break;																															\
		case MMCPORT_F:																														\
			for(i=0; i < timeout && ((inb(PINF) & BV(payload_ ## id ## _signal.pin)) ? HIGH:LOW) != payload_ ## id ## _signal.inactive; i++)\
				delay_ms(1);																												\
			if(((inb(PINF) & BV(payload_ ## id ## _signal.pin)) ? HIGH:LOW) != payload_ ## id ## _signal.inactive){							\
				error																														\
			}																																\
			break;																															\
		case MMCPORT_G:																														\
			for(i=0; i < timeout && ((inb(PING) & BV(payload_ ## id ## _signal.pin)) ? HIGH:LOW) != payload_ ## id ## _signal.inactive; i++)\
				delay_ms(1);																												\
			if(((inb(PING) & BV(payload_ ## id ## _signal.pin)) ? HIGH:LOW) != payload_ ## id ## _signal.inactive){							\
				error																														\
			}																																\
			break;																															\
	}
*/

#define SET_SIGNAL(id)								sbi(id ## _PPORT, id ## _PIN);

#define CLEAR_SIGNAL(id)							cbi(id ## _PPORT, id ## _PIN);

#define DELAY(ms)									wdt_reset(); delay_ms(ms);

#define WAIT_FOR_HIGH(id, timeout, error)																\
	wdt_reset();																						\
	for(i=0; i < timeout && !(inb(id ## _PPIN) & BV(id ## _PIN)); i++)				delay_ms(1);		\
	if(!(inb(id ## _PPIN) & BV(id ## _PIN)))										error
	
#define WAIT_FOR_LOW(id, timeout, error)																\
	wdt_reset();																						\
	for(i=0; i < timeout && (inb(id ## _PPIN) & BV(id ## _PIN)); i++)				delay_ms(1);		\
	if((inb(id ## _PPIN) & BV(id ## _PIN)))											error

#define PON_ERROR		desactive_payload(); return 1;
#define POFF_ERROR		return 1;

#define PON_CONTINUE	asm volatile("nop");

u08 active_payload();
u08 desactive_payload();
u08 ipmi_picmg_fru_control(u08 control_type);
u08 ipmi_picmg_get_fru_control_capabilities();
	
#endif /* PAYLOAD_H_ */