//*****************************************************************************
// Copyright (C) 2007 DESY(Deutsches-Elektronen Synchrotron) 
//
//File Name	: project.h
// 
// Title		: project definitions
// Revision		: 1.0
// Notes		:	
// Target MCU	: Atmel AVR series
//
// Author       : Vahan Petrosyan (vahan_petrosyan@desy.de)
// Modified by  : Markus Joos (markus.joos@cern.ch)
//
// Description : Pins configurations and main project definitions
//					
//
// This code is distributed under the GNU Public License
//		which can be found at http://www.gnu.org/licenses/gpl.txt
//*****************************************************************************
#ifndef PROJECT_H
#define PROJECT_H

//I2C buffer size for IPMB messages
#define MAX_BYTES_READ 60

//Supported IPMI release, nibble swapped
#define MMC_IPMI_REL		0x51	// V1.5

//Additional device support
#define IPMI_MSG_ADD_DEV_SUPP	0x29	// event receiver, accept sensor cmds

#endif
