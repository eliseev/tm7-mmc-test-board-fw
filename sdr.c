//*****************************************************************************
// Copyright (C) 2007 DESY(Deutsches-Elektronen Synchrotron) 
//
// File Name	: sdr_main.c
// 
// Title		: SDR records
// Revision		: 1.0
// Notes		:	
// Target MCU	: Atmel AVR series
//
// Author       : Vahan Petrosyan (vahan_petrosyan@desy.de)
// Modified by  : Frederic Bompard (CPPM)
// Modified by  : Paschalis Vichoudis (CERN)
// Modified by  : Markus Joos (markus.joos@cern.ch)
//
// Description : Sensor Data Records, temperature and voltage management of the AMC module
//					
//
// This code is distributed under the GNU Public License
//		which can be found at http://www.gnu.org/licenses/gpl.txt
//*****************************************************************************

//MJ: check for obsolete includes
#include <avr/io.h>		// include I/O definitions (port names, pin names, etc)
#include "avrlibdefs.h"
#include "sdr.h"
#include "led.h"
#include "i2csw.h"
#include "ipmi_if.h"
#include "fru.h"
#include "a2d.h"
#include "rtm_mng.h"
#include "user_code_select.h"
#include "sensor_macro.h"
#include "user_code/sensors.h"
#include "project.h"

// External variables
extern u08 rtm_sensor_enable;
extern u08 rtm_hs_enable;

#ifdef USE_RTM
	sensor_t sens[SDR_MAX_ID + NUM_SENSOR_RTM];
#else
	sensor_t sens[SDR_MAX_ID];	
#endif

u08 sdrLen[SDR_MAX_ID+1];
u08 *sdrPtr[SDR_MAX_ID+1];

SDR_DECLARATION(SDR_MAX_ID)

#ifdef USE_RTM
	#define SENSOR_CNT	(SDR_MAX_ID + NUM_SDR_RTM)
	#define SDR_CNT		(SDR_MAX_ID + 1 + NUM_SDR_RTM)
#else
	#define SENSOR_CNT	(SDR_MAX_ID)
	#define SDR_CNT		(SDR_MAX_ID + 1)
#endif
//**********************/
void sdr_init(u08 ipmiID)   //Called from mmc_main.c
//**********************/
{
    u08 i;

	SDR_INIT(SDR_MAX_ID)
	
	for (i = 0; i < SDR_CNT; i++)
    {		
        sdrPtr[i][4] = sdrLen[i] - 5;                          //set length field in sensor
		
        if (sdrPtr[i][3] == 0x02 || sdrPtr[i][3] == 0x01)      //full / compact sensor: fill Entity instance. See Table 37-1 of IPMI1.5
            sdrPtr[i][9] = 0x60 | ((ipmiID - 0x70) >> 1);      //VB: This should be checked more carefully according to chapter 33.1 of IPMIv1.5
			
        else if (sdrPtr[i][3] == 0x12)                         //Device Locator record: fill Entity instance. See Table 37-5 of IPMI1.5
            sdrPtr[i][13] = 0x60 | ((ipmiID - 0x70) >> 1);     //MJ: according to MTCA.4 REQ 3-62 the "entity instance" should be "AMC's site number + 60h". Does this code do that?

        sdrPtr[i][0] = i; 									   //fill record ID
        sdrPtr[i][1] = 0x00;								   //MSB is always 0 
        sdrPtr[i][5] = ipmiID;								   //Sensor Owner ID
    }
}


//***************/
void sensor_init()   //Called from mmc_main.c
//***************/
{
    u08 i, j;

	for (i = 0; i < SENSOR_CNT; i++){

		for (j = 0; j < SDR_CNT; j++){
			if ((sdrPtr[j][3] == 0x02 || sdrPtr[j][3] == 0x01) && sdrPtr[j][7] == i)	//JM: Only for full and compact sensors ?
				break;
		}
		
		sens[i].type      = sdrPtr[j][12];
		sens[i].event_dir = sdrPtr[j][13];
		sens[i].value     = 0;
		sens[i].state     = 0;
		sens[i].value_changed_flag = 0;
		sens[i].sdr_ptr   = j;
	}
    
    a2dInit();
    a2dSetChannel(0);
    a2dStartConvert();
	
	sensor_init_user();
}

//***********************************/
void check_temp_event(u08 temp_sensor)    //Called from user_code.c   //MJ: Was declared "static". Why?
//***********************************/
{
    // Check sensor Threshold limits
	u08 lnr_gh = IPMI_THRESHOLD_LNR_GH;
	u08 lnr_gl = IPMI_THRESHOLD_LNR_GL;
	u08 lc_gh = IPMI_THRESHOLD_LC_GH;
	u08 lc_gl = IPMI_THRESHOLD_LC_GL;
	u08 lnc_gh = IPMI_THRESHOLD_LNC_GH;
	u08 lnc_gl = IPMI_THRESHOLD_LNC_GL;
	u08 unc_gh = IPMI_THRESHOLD_UNC_GH;
	u08 uc_gh = IPMI_THRESHOLD_UC_GH;
	u08 unc_gl = IPMI_THRESHOLD_UNC_GL;
	u08 unr_gh = IPMI_THRESHOLD_UNR_GH;
	u08 uc_gl = IPMI_THRESHOLD_UC_GL;
	u08 unr_gl = IPMI_THRESHOLD_UNR_GL;
	
    switch (sens[temp_sensor].state)
    {
		case TEMP_STATE_LOW_NON_REC:
			if (sens[temp_sensor].value >= (sdrPtr[sens[temp_sensor].sdr_ptr][39] + sdrPtr[sens[temp_sensor].sdr_ptr][43]))
			{   //higher than lower non-recoverable
				sens[temp_sensor].state = TEMP_STATE_LOW_CRIT;
				ipmi_event_send(temp_sensor, ASSERTION_EVENT, &lnr_gh, 1); //lower non recoverable going high
			}
			break;

		case TEMP_STATE_LOW_CRIT:
			if (sens[temp_sensor].value <= sdrPtr[sens[temp_sensor].sdr_ptr][39])
			{   //temp is below lower non-recoverable
				sens[temp_sensor].state = TEMP_STATE_LOW_NON_REC;
				ipmi_event_send(temp_sensor, DEASSERTION_EVENT, &lnr_gl, 1); //lower non recoverable going low
			}
			if (sens[temp_sensor].value > (sdrPtr[sens[temp_sensor].sdr_ptr][40] + sdrPtr[sens[temp_sensor].sdr_ptr][43]))
			{   //higher than lower critical
				sens[temp_sensor].state = TEMP_STATE_LOW;
				ipmi_event_send(temp_sensor, ASSERTION_EVENT, &lc_gh, 1); //lower critical going high
			}
			break;

		case TEMP_STATE_LOW:
			if (sens[temp_sensor].value <= sdrPtr[sens[temp_sensor].sdr_ptr][40])
			{   //temp is below lower critical
				sens[temp_sensor].state = TEMP_STATE_LOW_CRIT;
				ipmi_event_send(temp_sensor, DEASSERTION_EVENT, &lc_gl, 1); //lower critical going low
			}
			if (sens[temp_sensor].value > (sdrPtr[sens[temp_sensor].sdr_ptr][41] + sdrPtr[sens[temp_sensor].sdr_ptr][43]))
			{   //lower non critical
				sens[temp_sensor].state = TEMP_STATE_NORMAL;
				ipmi_event_send(temp_sensor, ASSERTION_EVENT, &lnc_gh, 1); //lower non critical going high
			}
			break;

		case TEMP_STATE_NORMAL:
			if (sens[temp_sensor].value <= sdrPtr[sens[temp_sensor].sdr_ptr][41])
			{   //temp is below lower non-critical
				sens[temp_sensor].state = TEMP_STATE_LOW;
				ipmi_event_send(temp_sensor, DEASSERTION_EVENT, &lnc_gl, 1); //lower non critical going low
			}
			if (sens[temp_sensor].value >= sdrPtr[sens[temp_sensor].sdr_ptr][38])
			{   //higher than upper non-critical
				sens[temp_sensor].state = TEMP_STATE_HIGH;
				ipmi_event_send(temp_sensor, ASSERTION_EVENT, &unc_gh, 1); //upper non critical going high
			}
			break;

		case TEMP_STATE_HIGH:
			if (sens[temp_sensor].value >= sdrPtr[sens[temp_sensor].sdr_ptr][37])
			{   //higher than upper critical
				sens[temp_sensor].state = TEMP_STATE_HIGH_CRIT;
				ipmi_event_send(temp_sensor, ASSERTION_EVENT, &uc_gh, 1); //upper critical going high
			}
			if (sens[temp_sensor].value < (sdrPtr[sens[temp_sensor].sdr_ptr][38] - sdrPtr[sens[temp_sensor].sdr_ptr][42]))
			{   //lower than upper non-critical
				sens[temp_sensor].state = TEMP_STATE_NORMAL;
				ipmi_event_send(temp_sensor, DEASSERTION_EVENT, &unc_gl, 1); //upper non critical going low
			}
			break;
		case TEMP_STATE_HIGH_CRIT:
			if (sens[temp_sensor].value >= sdrPtr[sens[temp_sensor].sdr_ptr][36])
			{   //higher than upper non-recoverable
				sens[temp_sensor].state = TEMP_STATE_HIGH_NON_REC;
				ipmi_event_send(temp_sensor, ASSERTION_EVENT, &unr_gh, 1); //upper non-recoverable going high
			}
			if (sens[temp_sensor].value < (sdrPtr[sens[temp_sensor].sdr_ptr][37] - sdrPtr[sens[temp_sensor].sdr_ptr][42]))
			{   //lower than upper critical
				sens[temp_sensor].state = TEMP_STATE_HIGH;
				ipmi_event_send(temp_sensor, DEASSERTION_EVENT, &uc_gl, 1); //upper critical going low
			}
			break;
		case TEMP_STATE_HIGH_NON_REC:
			if (sens[temp_sensor].value < (sdrPtr[sens[temp_sensor].sdr_ptr][36] - sdrPtr[sens[temp_sensor].sdr_ptr][42]))
			{   //below upper non-recoverable
				sens[temp_sensor].state = TEMP_STATE_HIGH_CRIT;
				ipmi_event_send(temp_sensor, DEASSERTION_EVENT, &unr_gl, 1); //upper non-recoverable going low
			}
			break;
		default:   //MJ: should we signal an error if we end up here?
			break;
    }
}


//****************************************/
u08 ipmi_sdr_info(u08* buf, u08 sdr_sensor)  //Called from ipmi_if.c
//****************************************/
{
	#ifdef USE_RTM
		if (rtm_sensor_enable){
			if (sdr_sensor) buf[0] = SDR_CNT;							//Number of sdr records
			else			buf[0] = SENSOR_CNT;						//Number of sensors
		}
		else if (rtm_hs_enable){
			if (sdr_sensor) buf[0] = SDR_MAX_ID+1 + 2;					//Number of sdr records, just device locator and HS for the RTM
			else			buf[0] = SDR_MAX_ID + 1;					//Number of sensors, just HS for the RTM
		}
		else{	
			if (sdr_sensor) buf[0] = SDR_MAX_ID+1;						//Number of sdr records
			else			buf[0] = SDR_MAX_ID;						//Number of sensors
		}
		
	#else
		if (sdr_sensor) 	buf[0] = SDR_CNT;							//Number of sdr records
		else				buf[0] = SENSOR_CNT;						//Number of sensors
	#endif
		
    buf[1] = 0x01;														//static population / Lun 0 ob
    return 2;
}


//****************************************************/
u08 ipmi_sdr_get(u08* id, u08 offs, u08 size, u08* buf)  //Called from ipmi_if.c
//****************************************************/
{
    u16 recordID;
    u08 len = 0, i;
    u08 max_sens;

	#ifdef USE_RTM
		if (rtm_sensor_enable)	max_sens = SDR_CNT;
		else if (rtm_hs_enable)	max_sens = SDR_MAX_ID+1 + 2;                    //Only device locator and hotSwap sensor enabled so far (waiting for payload power)
		else					max_sens = SDR_MAX_ID+1;
	#else
		max_sens = SDR_CNT;
	#endif
	
    recordID = id[0] | (id[1] << 8);

    if (recordID > max_sens || size > MAX_BYTES_READ || offs > MAX_RECORD_SIZE)
        return(0xff);

    if ((offs + size) > sdrLen[recordID])
        return(0xff);

    if ((recordID + 1) < max_sens)
    {
        buf[len++] = (recordID + 1) & 0xff; //next record ID
        buf[len++] = (recordID + 1) >> 8;   //next record ID
    }
    else
    {
        buf[len++] = 0xff;                  //last record ID
        buf[len++] = 0xff;                  //last record ID
    }

    for (i = 0; i < size; i++)
        buf[len++] = sdrPtr[recordID][i + offs];

    return(len);
}


//*******************************************************/
u08 ipmi_get_sensor_threshold(u08 sensor_number, u08* buf)  //Called from ipmi_if.c
//*******************************************************/
{
    u08 max_sens;

	#ifdef USE_RTM
		if (rtm_sensor_enable)      max_sens = SENSOR_CNT;
		else if (rtm_hs_enable)     max_sens = SDR_MAX_ID + 1;
		else				        max_sens = SDR_MAX_ID;
	#else
		max_sens = SENSOR_CNT;
	#endif
	
    if (sensor_number > (max_sens - 1))
        return(0xff);

    //Only full sensors have limits. Therefore we have to check what sensor we are dealing with
    if(sdrPtr[sens[sensor_number].sdr_ptr][3] == 0x1)        //If it is a full sensor it has to have thresholds
    {
        buf[0] = 0x3f;    //MJ: We claim that all 6 thresholds are readable. This may not be true for all sensors. Therefore this hard coded value
                          //MJ: should be replaced by a sensor specific value. It is not yet clear how to implement this
                          //MJ: In any case the function should (partially) be moved into the user code section

        buf[1] = sdrPtr[sens[sensor_number].sdr_ptr][41];    //Lower non critical
        buf[2] = sdrPtr[sens[sensor_number].sdr_ptr][40];    //Lower critical
        buf[3] = sdrPtr[sens[sensor_number].sdr_ptr][39];    //Lower non recoverable
        buf[4] = sdrPtr[sens[sensor_number].sdr_ptr][38];    //Upper non critical
        buf[5] = sdrPtr[sens[sensor_number].sdr_ptr][37];    //Upper critical
        buf[6] = sdrPtr[sens[sensor_number].sdr_ptr][36];    //Upper non recoverable
    }
    else
    {
        buf[0] = 0x0;    //There are no thresholds
        buf[1] = 0x0;    //Dummy (Lower non critical)
        buf[2] = 0x0;    //Dummy (Lower critical)
        buf[3] = 0x0;    //Dummy (Lower non recoverable)
        buf[4] = 0x0;    //Dummy (Upper non critical)
        buf[5] = 0x0;    //Dummy (Upper critical)
        buf[6] = 0x0;    //Dummy (Upper non recoverable)
    }
    return(7);
}


//*************************************************************************************************************/
u08 ipmi_set_sensor_threshold(u08 sensor_number, u08 mask, u08 lnc, u08 lcr, u08 lnr, u08 unc, u08 ucr, u08 unr)  //Called from ipmi_if.c
//*************************************************************************************************************/
{
    u08 max_sens;

    //Note: Bits in the bytes 19 & 20 in the full sensor SDR define which thresholds can be modified
    //This function does not evaluate these bits. I assume the MCH takes care of that

    #ifdef USE_RTM
		if (rtm_sensor_enable)      max_sens = SENSOR_CNT;
		else if (rtm_hs_enable)     max_sens = SDR_MAX_ID + 1;
		else				        max_sens = SDR_MAX_ID;
    #else
		max_sens = SENSOR_CNT;
    #endif
	
    if (sensor_number > (max_sens - 1))
        return(0xff);

    if(sdrPtr[sens[sensor_number].sdr_ptr][3] == 0x1)                     //If it is a full sensor it has to have thresholds
    {
        if (mask & 0x01) sdrPtr[sens[sensor_number].sdr_ptr][41] = lnc;   //Lower non critical
        if (mask & 0x02) sdrPtr[sens[sensor_number].sdr_ptr][40] = lcr;   //Lower critical
        if (mask & 0x04) sdrPtr[sens[sensor_number].sdr_ptr][39] = lnr;   //Lower non recoverable
        if (mask & 0x08) sdrPtr[sens[sensor_number].sdr_ptr][38] = unc;   //Upper non critical
        if (mask & 0x10) sdrPtr[sens[sensor_number].sdr_ptr][37] = ucr;   //Upper critical
        if (mask & 0x20) sdrPtr[sens[sensor_number].sdr_ptr][36] = unr;   //Upper non recoverable
    }

    return(0);
}


//*****************************************************/
u08 ipmi_get_sensor_reading(u08 sensor_number, u08* buf)  //Called from ipmi_if.c
//*****************************************************/
{
    u08 max_sens;

    #ifdef USE_RTM
		if (rtm_sensor_enable)      max_sens = SENSOR_CNT;
		else if (rtm_hs_enable)     max_sens = SDR_MAX_ID + 1;
		else				        max_sens = SDR_MAX_ID;
    #else
		max_sens = SENSOR_CNT;
    #endif

    if (sensor_number > (max_sens - 1))
        return(0xff);

    buf[0] = sens[sensor_number].value;           //raw value of sensor. For the RTM hot swap sensor this is just "0"
	
	#ifdef USE_RTM
		if (sensor_number >= FIRST_RTM_SENSOR)
			buf[1] = sens[sensor_number].evnt_scan;   //events and scanning only enabled if RTM is active
		else
			buf[1] = 0xc0;                            //all events and scanning enabled
	#else
		  buf[1] = 0xc0;                            //all events and scanning enabled
	#endif
	
    buf[2] = sens[sensor_number].state;

    //Section 35.14 of IPMI 1.5 suggests that we should return "buf[3] = 0". When reading a sensor Natview shows
    //State = 0xrr00 with "rr" being a random value if "buf[3] = 0" is missing
    buf[3] = 0;

#ifdef USE_RTM
    if(sensor_number == RTM_HOT_SWAP_SENSOR)      //See MTCA.4, Table 3-2, Response data(5)
    {
        if (sens[RTM_HOT_SWAP_SENSOR].state & RTM_COMPATIBLE)
            buf[3] = 0x80;
        else
            buf[3] = 0x81;
    }
#endif

    return(4);
}


//****************************************/
u08 ipmi_picmg_get_device_locator(u08* buf)  //Called from ipmi_if.c
//****************************************/
{
    u08 len = 0;

    buf[len++] = SDR0[0]; //record ID LSB
    buf[len++] = SDR0[1]; //record ID MSB

    return(len);
}

void set_sensor_value(u08 sensID, u08 val){
	sens[sensID].value = val;
}