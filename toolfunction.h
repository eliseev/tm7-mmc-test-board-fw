/*
 * toolfunction.h
 *
 *  Created: 19/09/2014 10:13:25
 *  Author: Julian Mendez <julian.mendez@cern.ch>
 *
 *  Description : Tool functions used in the source code
 */ 


#ifndef TOOLFUNCTION_H_
#define TOOLFUNCTION_H_

#include "avrlibdefs.h"
#include "avrlibtypes.h"

int isinarray(u08 val, u08 *arr, int size);

#endif /* TOOLFUNCTION_H_ */