/*
 * toolfunctions.c
 *
 * Created: 19/09/2014 10:13:03
 *  Author: Julian Mendez <julian.mendez@cern.ch>
 */ 

#include "avrlibdefs.h"
#include "avrlibtypes.h"

int isinarray(u08 val, u08 *arr, int size){
	int i;
	for (i=0; i < size; i++) {
		if (arr[i] == val)
			return 0x01;
	}
	return 0x00;
}