/*
 * PLL_config.cT_PLL
 *
 * Created: 12/08/2014 18:32:07
 *  Author: atriossi
 * Modified: 11/27/2018
 *  By: J.Sastre
 * Comments added and dead-code cleaning: 15/06/2021
 * By: D. Eliseev
 * The programming procedure for the PLL over I2C is given in Fig. 9 (Datasheet Si5338A, page 23)
 * Register map of Si5338 can be found in: Si5338 Reference Manual (Configuring the Si5338 without clock builder pro)
 */ 


#include "PLL_config.h"
#include "../led.h"

int rd_byte;
int cp_byte;


int I2C_Start(void)
{
	DDRD |= (1<<SDA_PLL);
	// Start condition
	// SCL_PLL_PLL=1
	PORTD |= (1<<SCL_PLL);
	
	// SDA_PLL=1
	PORTD |= (1<<SDA_PLL);
	_delay_ms(0.002*T_PLL);

	// SCL_PLL=1
	PORTD |= (1<<SCL_PLL);
	// SDA_PLL=0
	PORTD &= ~(1<<SDA_PLL);
	_delay_ms(0.002*T_PLL);

	return 1;
}

int I2C_Stop(void)
{
	DDRD |= (1<<SDA_PLL);
	// Start condition
	// SCL_PLL=1
	PORTD |= (1<<SCL_PLL);
	
	// SDA_PLL=0
	PORTD &= ~(1<<SDA_PLL);
	
	_delay_ms(0.002*T_PLL);

	// SCL_PLL=1
	PORTD |= (1<<SCL_PLL);
	
	// SDA_PLL=1
	PORTD |= (1<<SDA_PLL);
	_delay_ms(0.004*T_PLL);

	return 1;
}

int I2C_Write_Byte(int byte)
{
	int bit;
	int SDA_PLL_aux;
	
	DDRD |= (1<<SDA_PLL);

	// SCL_PLL_PLL=0
	PORTD &= ~(1<<SCL_PLL);

	for(bit=0x80; bit>0; bit=bit>>1)
	{
		if (byte & bit)
			SDA_PLL_aux = 1;
		else
			SDA_PLL_aux = 0;
		// SDA_PLL=SDA_PLL_aux
		if (SDA_PLL_aux)
			PORTD |= (1<<SDA_PLL);
		else
			PORTD &= ~(1<<SDA_PLL);
		
		// SCL_PLL=1
		PORTD |= (1<<SCL_PLL);
		_delay_ms(0.004*T_PLL);

		// SCL_PLL=0
		PORTD &= ~(1<<SCL_PLL);
	}
	return 1;
}

int I2C_Read_Byte(int *byte)
{
	int bit, j;
	int read_bit, read_value;

	DDRD &= ~(1<<SDA_PLL);

	// SCL_PLL_PLL=0
	PORTD &= ~(1<<SCL_PLL);

	read_value = 0x0;
	for(bit=0x80,j=0;bit;bit = bit >> 1,j++)
	{
		// SCL_PLL=0
		PORTD &= ~(1<<SCL_PLL);
		_delay_ms(0.004*T_PLL);

		// SCL_PLL=1
		PORTD |= (1<<SCL_PLL);

		read_bit = (PIND & (1<<SDA_PLL))>>SDA_PLL;
		read_value += (read_bit & 0x1) << (7-j);

		// SCL_PLL=1
		PORTD |= (1<<SCL_PLL);
		//_delay_ms(0.004*T_PLL);	
		// SCL_PLL=0
		PORTD &= ~(1<<SCL_PLL);
	}
	*byte = read_value;

	return 1;
}

int I2C_Master_ACK_bit(int bit)
{
	DDRD |= (1<<SDA_PLL);
	
	if (bit)
	PORTD |= (1<<SDA_PLL);
	else
	PORTD &= ~(1<<SDA_PLL);
	
	// SCL_PLL=0
	PORTD &= ~(1<<SCL_PLL);

	// SCL_PLL=1
	PORTD |= (1<<SCL_PLL);
	_delay_ms(0.01*T_PLL);

	// SCL_PLL=0
	PORTD &= ~(1<<SCL_PLL);

	return 1;
}

int I2C_ACK_bit(int *bit)
{
	int read_bit;
	//PORTD |= (1<<SDA_PLL);
	DDRD &= ~(1<<SDA_PLL);
	// SDA_PLL=1
	//DDRD |= (1<<SDA_PLL);
	//PORTD |= (1<<SDA_PLL);

	// SCL_PLL=0
	PORTD &= ~(1<<SCL_PLL);
	//TimerDelay(1); // delay

	// SCL_PLL=1
	PORTD |= (1<<SCL_PLL);
	//TimerDelay(1); // delay
	_delay_ms(0.01*T_PLL);
	//DDRD &= ~(1<<SDA_PLL);	
	read_bit = (PIND & (1<<SDA_PLL))>>SDA_PLL;

	// SCL_PLL=1
	PORTD |= (1<<SCL_PLL);
	//TimerDelay(1); // delay

	// SCL_PLL=0
	PORTD &= ~(1<<SCL_PLL);
	//TimerDelay(1); // delay

	*bit = read_bit;

	return 1;
}

int I2C_write_One_byte(int slaveAddress, int byte_1)
{
	int address, bit_aux;
	
	DDRD |= (1<<SCL_PLL);

	// The slave_address is 7-bit long
	address = slaveAddress & 0x7F;

	// Start condition
	if(I2C_Start() != 1)
		return 0;
	
	// Slave address + write bit
	bit_aux = (address << 1) + 0x0;
	if(I2C_Write_Byte(bit_aux) != 1)
		return 0;
	
	// ACK/NACK bit
	if(I2C_ACK_bit(&bit_aux) != 1)
		return 0;
	if(bit_aux == 0x1) // NACK condition
		return 0;

	// byte_1
	if(I2C_Write_Byte(byte_1) != 1)
		return 0;

	// ACK/NACK bit
	if(I2C_ACK_bit(&bit_aux) != 1)
		return 0;
	/*if(bit_aux == 0x1) // NACK condition
		return SERIAL_FAIL;*/

	// Stop condition
	if(I2C_Stop() != 1)
		return 0;

	//TimerDelay(1000); // delay (longer)

	return 1;
}

int I2C_write_Two_byte(int slaveAddress, int byte_1, int byte_2)
{
	int address, bit_aux;
	
	DDRD |= (1<<SCL_PLL);

	// The slave_address is 7-bit long
	address = slaveAddress & 0x7F;

	// Start condition
	if(I2C_Start() != 1)
		return 0;
	
	// Slave address + write bit
	bit_aux = (address << 1) + 0x0;
	if(I2C_Write_Byte(bit_aux) != 1)
		return 0;
	
	// ACK/NACK bit
	if(I2C_ACK_bit(&bit_aux) != 1)
		return 0;
	if(bit_aux == 0x1) // NACK condition
		return 0;

	// byte_1
	if(I2C_Write_Byte(byte_1) != 1)
		return 0;

	// ACK/NACK bit
	if(I2C_ACK_bit(&bit_aux) != 1)
		return 0;
	/*if(bit_aux == 0x1) // NACK condition
		return SERIAL_FAIL;*/
	
	// byte_2
	if(I2C_Write_Byte(byte_2) != 1)
		return 0;

	// ACK/NACK bit
	if(I2C_ACK_bit(&bit_aux) != 1)
		return 0;
	/*if(bit_aux == 0x1) // NACK condition
		return SERIAL_FAIL;*/

	// Stop condition
	if(I2C_Stop() != 1)
		return 0;

	//TimerDelay(1000); // delay (longer)

	return 1;
}

int I2C_read_One_byte(int slaveAddress, int *byte_1)
{
	int address, bit_aux;

	// The slave_address is 7-bit long
	address = slaveAddress & 0x7F;
	// Start condition
	if(I2C_Start() != 1)
		return 0;
	
	// Slave address + read bit
	bit_aux = (address << 1) + 0x1;
	if(I2C_Write_Byte(bit_aux) != 1)
		return 0;
	
	// ACK/NACK bit
	if(I2C_ACK_bit(&bit_aux) != 1)
		return 0;
	if(bit_aux == 0x1) // NACK condition
		return 0;

	// byte_1
	if(I2C_Read_Byte(byte_1) != 1)
		return 0;

	// NACK bit by master
	if(I2C_Master_ACK_bit(0x1) != 1)
	return 0;

	// Stop condition
	if(I2C_Stop() != 1)
		return 0;
	
	return 1;
}



void PLL_config()         // I2C ADDRESS = 0x71
{
	
	int go_on = 0;
 
//	DDRB|= (1<<5);
//
	//while (1) {
 //
    //PORTB &= (0<<5);
    //_delay_ms(500);
	//PORTB |= (1<<5);
	//_delay_ms(500);
	//
	
	DDRE |= (1<<PS0_PLL);
	DDRE |= (1<<PS1_PLL);
	PORTE &= ~(1<<PS0_PLL);
	PORTE &= ~(1<<PS1_PLL);
	//PORTE |= (1<<PS1_PLL);
	
	SFIOR |= (1<<PUD);
	
	DDRD |= (1<<SDA_PLL);
	DDRD |= (1<<SCL_PLL);
	PORTD |= (1<<SCL_PLL);
	PORTD |= (1<<SDA_PLL);
	
	_delay_ms(1000);
	
	I2C_write_One_byte(0x74,0x08);
	
	_delay_ms(10);
	
	I2C_write_Two_byte(0x71,230,0x10);
	//
	_delay_ms(1);
	
	I2C_write_Two_byte(0x71,241,0xE5);
	//
	_delay_ms(1);
	//
	//I2C_write_Two_byte(0x71,0,0x00);
	//I2C_write_Two_byte(0x71,1,0x00);
	//I2C_write_Two_byte(0x71,2,0x00);
	//I2C_write_Two_byte(0x71,3,0x00);
	//I2C_write_Two_byte(0x71,4,0x00);
	//I2C_write_Two_byte(0x71,5,0x00);
	I2C_write_One_byte(0x71,6);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte |= (1<<3);
	I2C_write_Two_byte(0x71,6,rd_byte); //I2C_write_Two_byte(0x71,6,0x08);
	//I2C_write_Two_byte(0x71,7,0x00);
	//I2C_write_Two_byte(0x71,8,0x70);
	//I2C_write_Two_byte(0x71,9,0x0F);
	//I2C_write_Two_byte(0x71,10,0x00);
	//I2C_write_Two_byte(0x71,11,0x00);
	//I2C_write_Two_byte(0x71,12,0x00);
	//I2C_write_Two_byte(0x71,13,0x00);
	//I2C_write_Two_byte(0x71,14,0x00);
	//I2C_write_Two_byte(0x71,15,0x00);
	//I2C_write_Two_byte(0x71,16,0x00);
	//I2C_write_Two_byte(0x71,17,0x00);
	//I2C_write_Two_byte(0x71,18,0x00);
	//I2C_write_Two_byte(0x71,19,0x00);
	//I2C_write_Two_byte(0x71,20,0x00);
	//I2C_write_Two_byte(0x71,21,0x00);
	//I2C_write_Two_byte(0x71,22,0x00);
	//I2C_write_Two_byte(0x71,23,0x00);
	//I2C_write_Two_byte(0x71,24,0x00);
	//I2C_write_Two_byte(0x71,25,0x00);
	//I2C_write_Two_byte(0x71,26,0x00);
	
	//I2C_write_One_byte(0x71,27);
	//_delay_ms(0.1);
	//I2C_read_One_byte(0x71,&rd_byte);
	//rd_byte |= (1<<4);
	//rd_byte |= (1<<6);
	//I2C_write_Two_byte(0x71,27,rd_byte);//I2C_write_Two_byte(0x71,27,0x70);
	
	I2C_write_Two_byte(0x71,28,0x16);
	I2C_write_Two_byte(0x71,29,0x90);
	I2C_write_Two_byte(0x71,30,0xB0);
	I2C_write_Two_byte(0x71,31,0xC0);
	I2C_write_Two_byte(0x71,32,0xE3);
	I2C_write_Two_byte(0x71,33,0xE3);
	I2C_write_Two_byte(0x71,34,0xE3);
	I2C_write_Two_byte(0x71,35,0x01);
	
	I2C_write_One_byte(0x71,36);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte |= (1<<1);
	rd_byte |= (1<<2);
	I2C_write_Two_byte(0x71,36,rd_byte);//I2C_write_Two_byte(0x71,36,0x06);
	
	I2C_write_One_byte(0x71,37);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	I2C_write_Two_byte(0x71,37,rd_byte);//I2C_write_Two_byte(0x71,37,0x00);
	
	I2C_write_One_byte(0x71,38);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	I2C_write_Two_byte(0x71,38,rd_byte);//I2C_write_Two_byte(0x71,38,0x00);
	
	I2C_write_One_byte(0x71,39);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	I2C_write_Two_byte(0x71,39,rd_byte);//I2C_write_Two_byte(0x71,39,0x00);
	I2C_write_Two_byte(0x71,40,0x64);
	
	I2C_write_One_byte(0x71,41);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte |= (1<<2);
	rd_byte |= (1<<3);
	I2C_write_Two_byte(0x71,41,rd_byte);//I2C_write_Two_byte(0x71,41,0x0C);
	
	I2C_write_One_byte(0x71,42);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte |= (1<<0);
	rd_byte |= (1<<1);
	rd_byte |= (1<<5);
	I2C_write_Two_byte(0x71,42,rd_byte);//I2C_write_Two_byte(0x71,42,0x23);
	//I2C_write_Two_byte(0x71,43,0x00);
	//I2C_write_Two_byte(0x71,44,0x00);
	I2C_write_Two_byte(0x71,45,0x00);
	I2C_write_Two_byte(0x71,46,0x00);
	I2C_write_Two_byte(0x71,47,0x14);
	I2C_write_Two_byte(0x71,48,0x2F);
	I2C_write_Two_byte(0x71,49,0x10);
	I2C_write_Two_byte(0x71,50,0xC5);
	I2C_write_Two_byte(0x71,51,0x07);
	
	I2C_write_One_byte(0x71,52);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte |= (1<<4);
	I2C_write_Two_byte(0x71,52,rd_byte);//I2C_write_Two_byte(0x71,52,0x10);
	I2C_write_Two_byte(0x71,53,0x00);
	I2C_write_Two_byte(0x71,54,0x04);
	I2C_write_Two_byte(0x71,55,0x00);
	I2C_write_Two_byte(0x71,56,0x00);
	I2C_write_Two_byte(0x71,57,0x00);
	I2C_write_Two_byte(0x71,58,0x00);
	I2C_write_Two_byte(0x71,59,0x01);
	I2C_write_Two_byte(0x71,60,0x00);
	I2C_write_Two_byte(0x71,61,0x00);
	
	I2C_write_One_byte(0x71,62);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	rd_byte &= ~(1<<5);
	I2C_write_Two_byte(0x71,62,rd_byte);//I2C_write_Two_byte(0x71,62,0x00);
	
	I2C_write_One_byte(0x71,63);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte |= (1<<4);
	I2C_write_Two_byte(0x71,63,rd_byte);//I2C_write_Two_byte(0x71,63,0x10);
	I2C_write_Two_byte(0x71,64,0x00);
	I2C_write_Two_byte(0x71,65,0x00);
	I2C_write_Two_byte(0x71,66,0x00);
	I2C_write_Two_byte(0x71,67,0x00);
	I2C_write_Two_byte(0x71,68,0x00);
	I2C_write_Two_byte(0x71,69,0x00);
	I2C_write_Two_byte(0x71,70,0x00);
	I2C_write_Two_byte(0x71,71,0x00);
	I2C_write_Two_byte(0x71,72,0x00);
	
	I2C_write_One_byte(0x71,73);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	rd_byte &= ~(1<<5);
	I2C_write_Two_byte(0x71,73,rd_byte);//I2C_write_Two_byte(0x71,73,0x00);
	
	I2C_write_One_byte(0x71,74);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte |= (1<<4);
	I2C_write_Two_byte(0x71,74,rd_byte);//I2C_write_Two_byte(0x71,74,0x10);
	I2C_write_Two_byte(0x71,75,0x00);
	I2C_write_Two_byte(0x71,76,0x00);
	I2C_write_Two_byte(0x71,77,0x00);
	I2C_write_Two_byte(0x71,78,0x00);
	I2C_write_Two_byte(0x71,79,0x00);
	I2C_write_Two_byte(0x71,80,0x00);
	I2C_write_Two_byte(0x71,81,0x00);
	I2C_write_Two_byte(0x71,82,0x00);
	I2C_write_Two_byte(0x71,83,0x00);
	
	I2C_write_One_byte(0x71,84);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	rd_byte &= ~(1<<5);
	I2C_write_Two_byte(0x71,84,rd_byte);//I2C_write_Two_byte(0x71,84,0x00);
	
	I2C_write_One_byte(0x71,85);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte |= (1<<4);
	I2C_write_Two_byte(0x71,85,rd_byte);//I2C_write_Two_byte(0x71,85,0x10);
	I2C_write_Two_byte(0x71,86,0x00);
	I2C_write_Two_byte(0x71,87,0x00);
	I2C_write_Two_byte(0x71,88,0x00);
	I2C_write_Two_byte(0x71,89,0x00);
	I2C_write_Two_byte(0x71,90,0x00);
	I2C_write_Two_byte(0x71,91,0x00);
	I2C_write_Two_byte(0x71,92,0x00);
	I2C_write_Two_byte(0x71,93,0x00);
	I2C_write_Two_byte(0x71,94,0x00);
	
	I2C_write_One_byte(0x71,95);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	rd_byte &= ~(1<<5);
	I2C_write_Two_byte(0x71,95,rd_byte);//I2C_write_Two_byte(0x71,95,0x00);
	//I2C_write_Two_byte(0x71,96,0x10);
	I2C_write_Two_byte(0x71,97,0x00);
	I2C_write_Two_byte(0x71,98,0x2E);
	I2C_write_Two_byte(0x71,99,0x00);
	I2C_write_Two_byte(0x71,100,0x00);
	I2C_write_Two_byte(0x71,101,0x00);
	I2C_write_Two_byte(0x71,102,0x00);
	I2C_write_Two_byte(0x71,103,0x01);
	I2C_write_Two_byte(0x71,104,0x00);
	I2C_write_Two_byte(0x71,105,0x00);
	
	I2C_write_One_byte(0x71,106);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte |= (1<<7);
	I2C_write_Two_byte(0x71,106,rd_byte);//I2C_write_Two_byte(0x71,106,0x80);
	I2C_write_Two_byte(0x71,107,0x00);
	
	I2C_write_One_byte(0x71,108);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	rd_byte &= ~(1<<5);
	rd_byte &= ~(1<<6);
	I2C_write_Two_byte(0x71,108,rd_byte);//I2C_write_Two_byte(0x71,108,0x00);
	I2C_write_Two_byte(0x71,109,0x00);
	I2C_write_Two_byte(0x71,110,0x40);
	I2C_write_Two_byte(0x71,111,0x00);
	
	I2C_write_One_byte(0x71,112);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	rd_byte &= ~(1<<5);
	rd_byte &= ~(1<<6);
	I2C_write_Two_byte(0x71,112,rd_byte);//I2C_write_Two_byte(0x71,112,0x00);
	I2C_write_Two_byte(0x71,113,0x00);
	I2C_write_Two_byte(0x71,114,0x40);
	I2C_write_Two_byte(0x71,115,0x00);
	I2C_write_Two_byte(0x71,116,0x80);
	I2C_write_Two_byte(0x71,117,0x00);
	I2C_write_Two_byte(0x71,118,0x40);
	I2C_write_Two_byte(0x71,119,0x00);
	I2C_write_Two_byte(0x71,120,0x00);
	I2C_write_Two_byte(0x71,121,0x00);
	I2C_write_Two_byte(0x71,122,0x40);
	I2C_write_Two_byte(0x71,123,0x00);
	I2C_write_Two_byte(0x71,124,0x00);
	I2C_write_Two_byte(0x71,125,0x00);
	I2C_write_Two_byte(0x71,126,0x00);
	I2C_write_Two_byte(0x71,127,0x00);
	I2C_write_Two_byte(0x71,128,0x00);
	
	I2C_write_One_byte(0x71,129);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	I2C_write_Two_byte(0x71,129,rd_byte);//I2C_write_Two_byte(0x71,129,0x00);
	
	I2C_write_One_byte(0x71,130);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	I2C_write_Two_byte(0x71,130,rd_byte);//I2C_write_Two_byte(0x71,130,0x00);
	I2C_write_Two_byte(0x71,131,0x00);
	I2C_write_Two_byte(0x71,132,0x00);
	I2C_write_Two_byte(0x71,133,0x00);
	I2C_write_Two_byte(0x71,134,0x00);
	I2C_write_Two_byte(0x71,135,0x00);
	I2C_write_Two_byte(0x71,136,0x00);
	I2C_write_Two_byte(0x71,137,0x00);
	I2C_write_Two_byte(0x71,138,0x00);
	I2C_write_Two_byte(0x71,139,0x00);
	I2C_write_Two_byte(0x71,140,0x00);
	I2C_write_Two_byte(0x71,141,0x00);
	I2C_write_Two_byte(0x71,142,0x00);
	I2C_write_Two_byte(0x71,143,0x00);
	I2C_write_Two_byte(0x71,144,0x00);
	//I2C_write_Two_byte(0x71,145,0x00);
	//I2C_write_Two_byte(0x71,146,0xFF);
	//I2C_write_Two_byte(0x71,147,0x00);
	//I2C_write_Two_byte(0x71,148,0x00);
	//I2C_write_Two_byte(0x71,149,0x00);
	//I2C_write_Two_byte(0x71,150,0x00);
	//I2C_write_Two_byte(0x71,151,0x00);
	I2C_write_Two_byte(0x71,152,0x00);
	I2C_write_Two_byte(0x71,153,0x00);
	I2C_write_Two_byte(0x71,154,0x00);
	I2C_write_Two_byte(0x71,155,0x00);
	I2C_write_Two_byte(0x71,156,0x00);
	I2C_write_Two_byte(0x71,157,0x00);
	
	I2C_write_One_byte(0x71,158);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	I2C_write_Two_byte(0x71,158,rd_byte);//I2C_write_Two_byte(0x71,158,0x00);
	
	I2C_write_One_byte(0x71,159);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	I2C_write_Two_byte(0x71,159,rd_byte);//I2C_write_Two_byte(0x71,159,0x00);
	I2C_write_Two_byte(0x71,160,0x00);
	I2C_write_Two_byte(0x71,161,0x00);
	I2C_write_Two_byte(0x71,162,0x00);
	I2C_write_Two_byte(0x71,163,0x00);
	I2C_write_Two_byte(0x71,164,0x00);
	I2C_write_Two_byte(0x71,165,0x00);
	I2C_write_Two_byte(0x71,166,0x00);
	I2C_write_Two_byte(0x71,167,0x00);
	I2C_write_Two_byte(0x71,168,0x00);
	I2C_write_Two_byte(0x71,169,0x00);
	I2C_write_Two_byte(0x71,170,0x00);
	I2C_write_Two_byte(0x71,171,0x00);
	I2C_write_Two_byte(0x71,172,0x00);
	I2C_write_Two_byte(0x71,173,0x00);
	I2C_write_Two_byte(0x71,174,0x00);
	I2C_write_Two_byte(0x71,175,0x00);
	I2C_write_Two_byte(0x71,176,0x00);
	I2C_write_Two_byte(0x71,177,0x00);
	I2C_write_Two_byte(0x71,178,0x00);
	I2C_write_Two_byte(0x71,179,0x00);
	I2C_write_Two_byte(0x71,180,0x00);
	
	I2C_write_One_byte(0x71,181);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	I2C_write_Two_byte(0x71,181,rd_byte);//I2C_write_Two_byte(0x71,181,0x00);
	I2C_write_Two_byte(0x71,182,0x00);
	I2C_write_Two_byte(0x71,183,0x00);
	I2C_write_Two_byte(0x71,184,0x00);
	I2C_write_Two_byte(0x71,185,0x00);
	I2C_write_Two_byte(0x71,186,0x00);
	I2C_write_Two_byte(0x71,187,0x00);
	I2C_write_Two_byte(0x71,188,0x00);
	I2C_write_Two_byte(0x71,189,0x00);
	I2C_write_Two_byte(0x71,190,0x00);
	I2C_write_Two_byte(0x71,191,0x00);
	I2C_write_Two_byte(0x71,192,0x00);
	I2C_write_Two_byte(0x71,193,0x00);
	I2C_write_Two_byte(0x71,194,0x00);
	I2C_write_Two_byte(0x71,195,0x00);
	I2C_write_Two_byte(0x71,196,0x00);
	I2C_write_Two_byte(0x71,197,0x00);
	I2C_write_Two_byte(0x71,198,0x00);
	I2C_write_Two_byte(0x71,199,0x00);
	I2C_write_Two_byte(0x71,200,0x00);
	I2C_write_Two_byte(0x71,201,0x00);
	I2C_write_Two_byte(0x71,202,0x00);
	
	I2C_write_One_byte(0x71,203);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	I2C_write_Two_byte(0x71,203,rd_byte);//I2C_write_Two_byte(0x71,203,0x00);
	I2C_write_Two_byte(0x71,204,0x00);
	I2C_write_Two_byte(0x71,205,0x00);
	I2C_write_Two_byte(0x71,206,0x00);
	I2C_write_Two_byte(0x71,207,0x00);
	I2C_write_Two_byte(0x71,208,0x00);
	I2C_write_Two_byte(0x71,209,0x00);
	I2C_write_Two_byte(0x71,210,0x00);
	I2C_write_Two_byte(0x71,211,0x00);
	I2C_write_Two_byte(0x71,212,0x00);
	I2C_write_Two_byte(0x71,213,0x00);
	I2C_write_Two_byte(0x71,214,0x00);
	I2C_write_Two_byte(0x71,215,0x00);
	I2C_write_Two_byte(0x71,216,0x00);
	I2C_write_Two_byte(0x71,217,0x00);
	//I2C_write_Two_byte(0x71,218,0x00);
	//I2C_write_Two_byte(0x71,219,0x00);
	//I2C_write_Two_byte(0x71,220,0x00);
	//I2C_write_Two_byte(0x71,221,0x0D);
	//I2C_write_Two_byte(0x71,222,0x00);
	//I2C_write_Two_byte(0x71,223,0x00);
	//I2C_write_Two_byte(0x71,224,0xF4);
	//I2C_write_Two_byte(0x71,225,0xF0);
	
	I2C_write_One_byte(0x71,226);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte &= ~(1<<2);
	I2C_write_Two_byte(0x71,226,rd_byte);//I2C_write_Two_byte(0x71,226,0x00);
	//I2C_write_Two_byte(0x71,227,0x00);
	//I2C_write_Two_byte(0x71,228,0x00);
	//I2C_write_Two_byte(0x71,229,0x00);
	//I2C_write_Two_byte(0x71,231,0x00);
	//I2C_write_Two_byte(0x71,232,0x00);
	//I2C_write_Two_byte(0x71,233,0x00);
	//I2C_write_Two_byte(0x71,234,0x00);
	//I2C_write_Two_byte(0x71,235,0x00);
	//I2C_write_Two_byte(0x71,236,0x00);
	//I2C_write_Two_byte(0x71,237,0x00);
	//I2C_write_Two_byte(0x71,238,0x14);
	//I2C_write_Two_byte(0x71,239,0x00);
	//I2C_write_Two_byte(0x71,240,0x00);
	
	I2C_write_One_byte(0x71,242);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte &= ~(1<<1);
	I2C_write_Two_byte(0x71,242,rd_byte);//I2C_write_Two_byte(0x71,242,0x00);
	//I2C_write_Two_byte(0x71,243,0xF0);
	//I2C_write_Two_byte(0x71,244,0x00);
	//I2C_write_Two_byte(0x71,245,0x00);
	//I2C_write_Two_byte(0x71,247,0x00);
	//I2C_write_Two_byte(0x71,248,0x00);
	//I2C_write_Two_byte(0x71,249,0xA8);
	//I2C_write_Two_byte(0x71,250,0x00);
	//I2C_write_Two_byte(0x71,251,0x84);
	//I2C_write_Two_byte(0x71,252,0x00);
	//I2C_write_Two_byte(0x71,253,0x00);
	//I2C_write_Two_byte(0x71,254,0x00);
	I2C_write_Two_byte(0x71,255,0x00);

_delay_ms(1);

while (go_on == 0)
{
	I2C_write_One_byte(0x71,218);
	_delay_ms(0.2);
	I2C_read_One_byte(0x71,&rd_byte);
	if (rd_byte == 0x08)
		go_on = 1;
	else
		go_on = 0;
	_delay_ms(1);
}

	go_on = 0;
	
	I2C_write_One_byte(0x71,49);
	_delay_ms(0.2);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte &= ~(1<<7);
	I2C_write_Two_byte(0x71,49,rd_byte);
	
	_delay_ms(1);

	I2C_write_Two_byte(0x71,246,0x02);
	
_delay_ms(25);

	I2C_write_Two_byte(0x71,241,0x65);
	
_delay_ms(1);

while (go_on == 0)
{
	I2C_write_One_byte(0x71,218);
	_delay_ms(0.2);
	I2C_read_One_byte(0x71,&rd_byte);
	if (rd_byte == 0x08)
		go_on = 1;
	else
		go_on = 0;
	_delay_ms(1);
}

	I2C_write_One_byte(0x71,237);
	_delay_ms(0.2);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	rd_byte &= ~(1<<5);
	rd_byte &= ~(1<<6);
	rd_byte &= ~(1<<7);
	rd_byte |= 0x14;
	I2C_write_Two_byte(0x71,47,rd_byte);
	
	_delay_ms(0.3);
	
	I2C_write_One_byte(0x71,236);
	_delay_ms(0.2);
	I2C_read_One_byte(0x71,&rd_byte);
	I2C_write_Two_byte(0x71,46,rd_byte);
	
	_delay_ms(0.3);
	
	I2C_write_One_byte(0x71,235);
	_delay_ms(0.2);
	I2C_read_One_byte(0x71,&rd_byte);
	I2C_write_Two_byte(0x71,45,rd_byte);
	
	_delay_ms(0.3);

	I2C_write_One_byte(0x71,49);
	_delay_ms(0.2);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte |= (1<<7);
	I2C_write_Two_byte(0x71,49,rd_byte);
	
	_delay_ms(1);

	I2C_write_Two_byte(0x71,230,0x00);  
	
	
	//I2C_write_Two_byte(0x71,230,0x10);
	//_delay_ms(1);
	//I2C_write_One_byte(0x71,230);
	//_delay_ms(0.2);
	//I2C_read_One_byte(0x71,&rd_byte);
	//_delay_ms(1);
	//I2C_write_Two_byte(0x71,230,rd_byte);
	
	//if(rd_byte==0)
		//PORTB &= ~(1<<5);
	//else
		//PORTB |= (1<<5);
		
		//SFIOR |= (1<<PUD);
		//
		//DDRD &= ~(1<<SDA_PLL);
		//DDRD &= ~(1<<SCL_PLL);
		
  //return(0);
}

void PLL_config_120()     // I2C ADDRESS = 0x70
{
int go_on = 0;
	
	DDRE |= (1<<PS0_PLL);   // defines the lines PS0 and PS1 as output and disables their pull-ups
	DDRE |= (1<<PS1_PLL);
	PORTE &= ~(1<<PS0_PLL);
	PORTE &= ~(1<<PS1_PLL);
	//PORTE |= (1<<PS1_PLL);
	
	SFIOR |= (1<<PUD);
	
	DDRD |= (1<<SDA_PLL);   // defines the lines SDA_PLL and SCL_PLL as output an enables their pull-ups
	DDRD |= (1<<SCL_PLL);
	PORTD |= (1<<SCL_PLL);
	PORTD |= (1<<SDA_PLL);
	
	_delay_ms(1000);
	
	I2C_write_One_byte(0x74,0x04);  // enables the channel #2 of the i2c switch
	
	_delay_ms(10);
  
  // start of I2c programming procedure
	
  // disable outputs
	I2C_write_Two_byte(0x70,230,0x10);  
	
	_delay_ms(1);
	
  // pause LOL  
	I2C_write_Two_byte(0x70,241,0xE5);  
	
	_delay_ms(1);
	
  
  // Start of writing new configuration
	I2C_write_One_byte(0x70,6);         // 
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte |= (1<<3);
	I2C_write_Two_byte(0x70,6,rd_byte); //I2C_write_Two_byte(0x70,6,0x08);
	
	I2C_write_Two_byte(0x70,28,0x03);
	I2C_write_Two_byte(0x70,29,0x00);
	I2C_write_Two_byte(0x70,30,0xB0);
	I2C_write_Two_byte(0x70,31,0xC0);
	I2C_write_Two_byte(0x70,32,0xC0);
	I2C_write_Two_byte(0x70,33,0xE3);
	I2C_write_Two_byte(0x70,34,0xE3);
	I2C_write_Two_byte(0x70,35,0x09);
	
	I2C_write_One_byte(0x70,36);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte |= (1<<1);
	rd_byte |= (1<<2);
	I2C_write_Two_byte(0x70,36,rd_byte);//I2C_write_Two_byte(0x71,36,0x06);
	
	I2C_write_One_byte(0x70,37);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte |= (1<<1);
	rd_byte |= (1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	I2C_write_Two_byte(0x70,37,rd_byte);//I2C_write_Two_byte(0x71,37,0x00);
	
	I2C_write_One_byte(0x70,38);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	I2C_write_Two_byte(0x70,38,rd_byte);//I2C_write_Two_byte(0x71,38,0x00);
	
	I2C_write_One_byte(0x70,39);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	I2C_write_Two_byte(0x70,39,rd_byte);//I2C_write_Two_byte(0x71,39,0x00);
	I2C_write_Two_byte(0x70,40,0x84);
	
	I2C_write_One_byte(0x70,41);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte |= (1<<2);
	rd_byte |= (1<<3);
	I2C_write_Two_byte(0x70,41,rd_byte);//I2C_write_Two_byte(0x71,41,0x0C);
	
	I2C_write_One_byte(0x70,42);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte |= (1<<0);
	rd_byte |= (1<<1);
	rd_byte |= (1<<5);
	I2C_write_Two_byte(0x70,42,rd_byte);//I2C_write_Two_byte(0x71,42,0x23);
	//I2C_write_Two_byte(0x71,43,0x00);
	//I2C_write_Two_byte(0x71,44,0x00);
	I2C_write_Two_byte(0x70,45,0x00);
	I2C_write_Two_byte(0x70,46,0x00);
	I2C_write_Two_byte(0x70,47,0x14);
	I2C_write_Two_byte(0x70,48,0x1D);
	I2C_write_Two_byte(0x70,49,0x10);
	I2C_write_Two_byte(0x70,50,0xC5);
	I2C_write_Two_byte(0x70,51,0x07);
	
	I2C_write_One_byte(0x70,52);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte |= (1<<4);
	I2C_write_Two_byte(0x70,52,rd_byte);//I2C_write_Two_byte(0x71,52,0x10);
	I2C_write_Two_byte(0x70,53,0x00);
	I2C_write_Two_byte(0x70,54,0x08); //JSA 0x04->0x08
	I2C_write_Two_byte(0x70,55,0x00);
	I2C_write_Two_byte(0x70,56,0x00);
	I2C_write_Two_byte(0x70,57,0x00);
	I2C_write_Two_byte(0x70,58,0x00);
	I2C_write_Two_byte(0x70,59,0x01);
	I2C_write_Two_byte(0x70,60,0x00);
	I2C_write_Two_byte(0x70,61,0x00);
	
	I2C_write_One_byte(0x70,62);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	rd_byte &= ~(1<<5);
	I2C_write_Two_byte(0x70,62,rd_byte);//I2C_write_Two_byte(0x71,62,0x00);
	
	I2C_write_One_byte(0x70,63);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte |= (1<<4);
	I2C_write_Two_byte(0x70,63,rd_byte);//I2C_write_Two_byte(0x71,63,0x10);
	I2C_write_Two_byte(0x70,64,0x00);
	I2C_write_Two_byte(0x70,65,0x1C);
	I2C_write_Two_byte(0x70,66,0x00);
	I2C_write_Two_byte(0x70,67,0x00);
	I2C_write_Two_byte(0x70,68,0x00);
	I2C_write_Two_byte(0x70,69,0x00);
	I2C_write_Two_byte(0x70,70,0x01);
	I2C_write_Two_byte(0x70,71,0x00);
	I2C_write_Two_byte(0x70,72,0x00);
	
	I2C_write_One_byte(0x70,73);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	rd_byte &= ~(1<<5);
	I2C_write_Two_byte(0x70,73,rd_byte);//I2C_write_Two_byte(0x71,73,0x00);
	
	I2C_write_One_byte(0x70,74);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte |= (1<<4);
	I2C_write_Two_byte(0x70,74,rd_byte);//I2C_write_Two_byte(0x71,74,0x10);
	I2C_write_Two_byte(0x70,75,0x00);
	I2C_write_Two_byte(0x70,76,0x00);
	I2C_write_Two_byte(0x70,77,0x00);
	I2C_write_Two_byte(0x70,78,0x00);
	I2C_write_Two_byte(0x70,79,0x00);
	I2C_write_Two_byte(0x70,80,0x00);
	I2C_write_Two_byte(0x70,81,0x00);
	I2C_write_Two_byte(0x70,82,0x00);
	I2C_write_Two_byte(0x70,83,0x00);
	
	I2C_write_One_byte(0x70,84);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	rd_byte &= ~(1<<5);
	I2C_write_Two_byte(0x70,84,rd_byte);//I2C_write_Two_byte(0x71,84,0x00);
	
	I2C_write_One_byte(0x70,85);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte |= (1<<4);
	I2C_write_Two_byte(0x70,85,rd_byte);//I2C_write_Two_byte(0x71,85,0x10);
	I2C_write_Two_byte(0x70,86,0x00);
	I2C_write_Two_byte(0x70,87,0x00);
	I2C_write_Two_byte(0x70,88,0x00);
	I2C_write_Two_byte(0x70,89,0x00);
	I2C_write_Two_byte(0x70,90,0x00);
	I2C_write_Two_byte(0x70,91,0x00);
	I2C_write_Two_byte(0x70,92,0x00);
	I2C_write_Two_byte(0x70,93,0x00);
	I2C_write_Two_byte(0x70,94,0x00);
	
	I2C_write_One_byte(0x70,95);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	rd_byte &= ~(1<<5);
	I2C_write_Two_byte(0x70,95,rd_byte);//I2C_write_Two_byte(0x71,95,0x00);
	//I2C_write_Two_byte(0x71,96,0x10);
	I2C_write_Two_byte(0x70,97,0x00);
	I2C_write_Two_byte(0x70,98,0x1C);
	I2C_write_Two_byte(0x70,99,0x00);
	I2C_write_Two_byte(0x70,100,0x00);
	I2C_write_Two_byte(0x70,101,0x00);
	I2C_write_Two_byte(0x70,102,0x00);
	I2C_write_Two_byte(0x70,103,0x01);
	I2C_write_Two_byte(0x70,104,0x00);
	I2C_write_Two_byte(0x70,105,0x00);
	
	I2C_write_One_byte(0x70,106);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte |= (1<<7);
	I2C_write_Two_byte(0x70,106,rd_byte);//I2C_write_Two_byte(0x70,106,0x80);
	I2C_write_Two_byte(0x70,107,0x00);
	
	I2C_write_One_byte(0x70,108);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	rd_byte &= ~(1<<5);
	rd_byte &= ~(1<<6);
	I2C_write_Two_byte(0x70,108,rd_byte);//I2C_write_Two_byte(0x70,108,0x00);
	I2C_write_Two_byte(0x70,109,0x00);
	I2C_write_Two_byte(0x70,110,0x40);
	I2C_write_Two_byte(0x70,111,0x00);
	
	I2C_write_One_byte(0x70,112);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	rd_byte &= ~(1<<5);
	rd_byte &= ~(1<<6);
	I2C_write_Two_byte(0x70,112,rd_byte);//I2C_write_Two_byte(0x70,112,0x00);
	I2C_write_Two_byte(0x70,113,0x00);
	I2C_write_Two_byte(0x70,114,0x40);
	I2C_write_Two_byte(0x70,115,0x00);
	I2C_write_Two_byte(0x70,116,0x80);
	I2C_write_Two_byte(0x70,117,0x00);
	I2C_write_Two_byte(0x70,118,0x40);
	I2C_write_Two_byte(0x70,119,0x00);
	I2C_write_Two_byte(0x70,120,0x00);
	I2C_write_Two_byte(0x70,121,0x00);
	I2C_write_Two_byte(0x70,122,0x40);
	I2C_write_Two_byte(0x70,123,0x00);
	I2C_write_Two_byte(0x70,124,0x00);
	I2C_write_Two_byte(0x70,125,0x00);
	I2C_write_Two_byte(0x70,126,0x00);
	I2C_write_Two_byte(0x70,127,0x00);
	I2C_write_Two_byte(0x70,128,0x00);
	
	I2C_write_One_byte(0x70,129);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	I2C_write_Two_byte(0x70,129,rd_byte);//I2C_write_Two_byte(0x70,129,0x00);
	
	I2C_write_One_byte(0x70,130);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	I2C_write_Two_byte(0x70,130,rd_byte);//I2C_write_Two_byte(0x70,130,0x00);
	I2C_write_Two_byte(0x70,131,0x00);
	I2C_write_Two_byte(0x70,132,0x00);
	I2C_write_Two_byte(0x70,133,0x00);
	I2C_write_Two_byte(0x70,134,0x00);
	I2C_write_Two_byte(0x70,135,0x00);
	I2C_write_Two_byte(0x70,136,0x00);
	I2C_write_Two_byte(0x70,137,0x00);
	I2C_write_Two_byte(0x70,138,0x00);
	I2C_write_Two_byte(0x70,139,0x00);
	I2C_write_Two_byte(0x70,140,0x00);
	I2C_write_Two_byte(0x70,141,0x00);
	I2C_write_Two_byte(0x70,142,0x00);
	I2C_write_Two_byte(0x70,143,0x00);
	I2C_write_Two_byte(0x70,144,0x00);
	//I2C_write_Two_byte(0x70,145,0x00);
	//I2C_write_Two_byte(0x70,146,0xFF);
	//I2C_write_Two_byte(0x70,147,0x00);
	//I2C_write_Two_byte(0x70,148,0x00);
	//I2C_write_Two_byte(0x70,149,0x00);
	//I2C_write_Two_byte(0x70,150,0x00);
	//I2C_write_Two_byte(0x70,151,0x00);
	I2C_write_Two_byte(0x70,152,0x00);
	I2C_write_Two_byte(0x70,153,0x00);
	I2C_write_Two_byte(0x70,154,0x00);
	I2C_write_Two_byte(0x70,155,0x00);
	I2C_write_Two_byte(0x70,156,0x00);
	I2C_write_Two_byte(0x70,157,0x00);
	
	I2C_write_One_byte(0x70,158);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	I2C_write_Two_byte(0x70,158,rd_byte);//I2C_write_Two_byte(0x70,158,0x00);
	
	I2C_write_One_byte(0x70,159);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	I2C_write_Two_byte(0x70,159,rd_byte);//I2C_write_Two_byte(0x70,159,0x00);
	I2C_write_Two_byte(0x70,160,0x00);
	I2C_write_Two_byte(0x70,161,0x00);
	I2C_write_Two_byte(0x70,162,0x00);
	I2C_write_Two_byte(0x70,163,0x00);
	I2C_write_Two_byte(0x70,164,0x00);
	I2C_write_Two_byte(0x70,165,0x00);
	I2C_write_Two_byte(0x70,166,0x00);
	I2C_write_Two_byte(0x70,167,0x00);
	I2C_write_Two_byte(0x70,168,0x00);
	I2C_write_Two_byte(0x70,169,0x00);
	I2C_write_Two_byte(0x70,170,0x00);
	I2C_write_Two_byte(0x70,171,0x00);
	I2C_write_Two_byte(0x70,172,0x00);
	I2C_write_Two_byte(0x70,173,0x00);
	I2C_write_Two_byte(0x70,174,0x00);
	I2C_write_Two_byte(0x70,175,0x00);
	I2C_write_Two_byte(0x70,176,0x00);
	I2C_write_Two_byte(0x70,177,0x00);
	I2C_write_Two_byte(0x70,178,0x00);
	I2C_write_Two_byte(0x70,179,0x00);
	I2C_write_Two_byte(0x70,180,0x00);
	
	I2C_write_One_byte(0x70,181);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	I2C_write_Two_byte(0x70,181,rd_byte);//I2C_write_Two_byte(0x70,181,0x00);
	I2C_write_Two_byte(0x70,182,0x00);
	I2C_write_Two_byte(0x70,183,0x00);
	I2C_write_Two_byte(0x70,184,0x00);
	I2C_write_Two_byte(0x70,185,0x00);
	I2C_write_Two_byte(0x70,186,0x00);
	I2C_write_Two_byte(0x70,187,0x00);
	I2C_write_Two_byte(0x70,188,0x00);
	I2C_write_Two_byte(0x70,189,0x00);
	I2C_write_Two_byte(0x70,190,0x00);
	I2C_write_Two_byte(0x70,191,0x00);
	I2C_write_Two_byte(0x70,192,0x00);
	I2C_write_Two_byte(0x70,193,0x00);
	I2C_write_Two_byte(0x70,194,0x00);
	I2C_write_Two_byte(0x70,195,0x00);
	I2C_write_Two_byte(0x70,196,0x00);
	I2C_write_Two_byte(0x70,197,0x00);
	I2C_write_Two_byte(0x70,198,0x00);
	I2C_write_Two_byte(0x70,199,0x00);
	I2C_write_Two_byte(0x70,200,0x00);
	I2C_write_Two_byte(0x70,201,0x00);
	I2C_write_Two_byte(0x70,202,0x00);
	
	I2C_write_One_byte(0x70,203);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	I2C_write_Two_byte(0x70,203,rd_byte);//I2C_write_Two_byte(0x70,203,0x00);
	I2C_write_Two_byte(0x70,204,0x00);
	I2C_write_Two_byte(0x70,205,0x00);
	I2C_write_Two_byte(0x70,206,0x00);
	I2C_write_Two_byte(0x70,207,0x00);
	I2C_write_Two_byte(0x70,208,0x00);
	I2C_write_Two_byte(0x70,209,0x00);
	I2C_write_Two_byte(0x70,210,0x00);
	I2C_write_Two_byte(0x70,211,0x00);
	I2C_write_Two_byte(0x70,212,0x00);
	I2C_write_Two_byte(0x70,213,0x00);
	I2C_write_Two_byte(0x70,214,0x00);
	I2C_write_Two_byte(0x70,215,0x00);
	I2C_write_Two_byte(0x70,216,0x00);
	I2C_write_Two_byte(0x70,217,0x00);
	//I2C_write_Two_byte(0x70,218,0x00);
	//I2C_write_Two_byte(0x70,219,0x00);
	//I2C_write_Two_byte(0x70,220,0x00);
	//I2C_write_Two_byte(0x70,221,0x0D);
	//I2C_write_Two_byte(0x70,222,0x00);
	//I2C_write_Two_byte(0x70,223,0x00);
	//I2C_write_Two_byte(0x70,224,0xF4);
	//I2C_write_Two_byte(0x70,225,0xF0);
	
	I2C_write_One_byte(0x70,226);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<2);
	I2C_write_Two_byte(0x70,226,rd_byte);//I2C_write_Two_byte(0x70,226,0x00);
	//I2C_write_Two_byte(0x70,227,0x00);
	//I2C_write_Two_byte(0x70,228,0x00);
	//I2C_write_Two_byte(0x70,229,0x00);
	//I2C_write_Two_byte(0x70,231,0x00);
	//I2C_write_Two_byte(0x70,232,0x00);
	//I2C_write_Two_byte(0x70,233,0x00);
	//I2C_write_Two_byte(0x70,234,0x00);
	//I2C_write_Two_byte(0x70,235,0x00);
	//I2C_write_Two_byte(0x70,236,0x00);
	//I2C_write_Two_byte(0x70,237,0x00);
	//I2C_write_Two_byte(0x70,238,0x14);
	//I2C_write_Two_byte(0x70,239,0x00);
	//I2C_write_Two_byte(0x70,240,0x00);
	
	I2C_write_One_byte(0x70,242);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<1);
	I2C_write_Two_byte(0x70,242,rd_byte);//I2C_write_Two_byte(0x70,242,0x00);
	//I2C_write_Two_byte(0x70,243,0xF0);
	//I2C_write_Two_byte(0x70,244,0x00);
	//I2C_write_Two_byte(0x70,245,0x00);
	//I2C_write_Two_byte(0x70,247,0x00);
	//I2C_write_Two_byte(0x70,248,0x00);
	//I2C_write_Two_byte(0x70,249,0xA8);
	//I2C_write_Two_byte(0x70,250,0x00);
	//I2C_write_Two_byte(0x70,251,0x84);
	//I2C_write_Two_byte(0x70,252,0x00);
	//I2C_write_Two_byte(0x70,253,0x00);
	//I2C_write_Two_byte(0x70,254,0x00);
	I2C_write_Two_byte(0x70,255,0x00);

	_delay_ms(1);
  
  // End of writing new configuration 
	
  // Validate input clock status
  for (int i; i<10; i++)                // waits until reg218 = 0x08  
	{	if (go_on == 1) {break;}
		I2C_write_One_byte(0x70,218);
		_delay_ms(0.2);
		I2C_read_One_byte(0x70,&rd_byte);
		if (rd_byte == 0x08)
		go_on = 1;
		else
		go_on = 0;
		_delay_ms(1);
	}





	go_on = 0;
	
  
  //Configure PLL for locking
	I2C_write_One_byte(0x70,49);          // writes '0' on bit 7 of reg 49 (FCAL_OVRD_EN = 0)
	_delay_ms(0.2);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<7);
	I2C_write_Two_byte(0x70,49,rd_byte);
	
	_delay_ms(1);

  // Initiate Locking of PLL
	I2C_write_Two_byte(0x70,246,0x02);  
	
	_delay_ms(25);

  // Restart LOL
	I2C_write_Two_byte(0x70,241,0x65);
	
	_delay_ms(1);

	//while (go_on == 0)
	//{
		//I2C_write_One_byte(0x70,218);
		//_delay_ms(0.2);
		//I2C_read_One_byte(0x70,&rd_byte);
		//if (rd_byte == 0x08)
		//go_on = 1;
		//else
		//go_on = 0;
		//_delay_ms(1);
	//}
	
  // Confirm PLL lock status 
	for (int i; i<10; i++)
	{	if (go_on == 1) {break;}
	I2C_write_One_byte(0x70,218);
	_delay_ms(0.2);
	I2C_read_One_byte(0x70,&rd_byte);
	if (rd_byte == 0x08)
	go_on = 1;
	else
	go_on = 0;
	_delay_ms(1);
	}

  // Copy FCAL values to active registers
	I2C_write_One_byte(0x70,237);
	_delay_ms(0.2);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	rd_byte &= ~(1<<5);
	rd_byte &= ~(1<<6);
	rd_byte &= ~(1<<7);
	rd_byte |= 0x14;
	I2C_write_Two_byte(0x70,47,rd_byte);
	
	_delay_ms(0.3);
	
	I2C_write_One_byte(0x70,236);
	_delay_ms(0.2);
	I2C_read_One_byte(0x70,&rd_byte);
	I2C_write_Two_byte(0x70,46,rd_byte);
	
	_delay_ms(0.3);
	
	I2C_write_One_byte(0x70,235);
	_delay_ms(0.2);
	I2C_read_One_byte(0x70,&rd_byte);
	I2C_write_Two_byte(0x70,45,rd_byte);
	
	_delay_ms(0.3);


  // Set PLL to use FCAL values
	I2C_write_One_byte(0x70,49);
	_delay_ms(0.2);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte |= (1<<7);
	I2C_write_Two_byte(0x70,49,rd_byte);
	
	_delay_ms(1);


  // Enable Outputs
	I2C_write_Two_byte(0x70,230,0x00);
	
}

void PLL_config_320()     // I2C ADDRESS = 0x70
{
	int go_on = 0;
	
	DDRE |= (1<<PS0_PLL);   // defines the lines PS0 and PS1 as output and disables their pull-ups
	DDRE |= (1<<PS1_PLL);
	PORTE &= ~(1<<PS0_PLL);
	PORTE &= ~(1<<PS1_PLL);
	//PORTE |= (1<<PS1_PLL);
	
	SFIOR |= (1<<PUD);
	
	DDRD |= (1<<SDA_PLL);   // defines the lines SDA_PLL and SCL_PLL as output an enables their pull-ups
	DDRD |= (1<<SCL_PLL);
	PORTD |= (1<<SCL_PLL);
	PORTD |= (1<<SDA_PLL);
	
	_delay_ms(1000);
	
	I2C_write_One_byte(0x74,0x04);  // enables the channel #2 of the i2c switch
	
	_delay_ms(10);
	
	// start of I2c programming procedure
	
	// disable outputs
	I2C_write_Two_byte(0x70,230,0x10);
	
	_delay_ms(1);
	
	// pause LOL
	I2C_write_Two_byte(0x70,241,0xE5);
	
	_delay_ms(1);
	
	
	// Start of writing new configuration
	I2C_write_One_byte(0x70,6);         //
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte |= (1<<3);
	I2C_write_Two_byte(0x70,6,rd_byte); //I2C_write_Two_byte(0x70,6,0x08);
	
	I2C_write_Two_byte(0x70,28,0x03);
	I2C_write_Two_byte(0x70,29,0x00);
	I2C_write_Two_byte(0x70,30,0xB0);
	I2C_write_Two_byte(0x70,31,0xC0);
	I2C_write_Two_byte(0x70,32,0xC0);
	I2C_write_Two_byte(0x70,33,0xE3);
	I2C_write_Two_byte(0x70,34,0xE3);
	I2C_write_Two_byte(0x70,35,0x09);
	
	I2C_write_One_byte(0x70,36);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte |= (1<<1);
	rd_byte |= (1<<2);
	I2C_write_Two_byte(0x70,36,rd_byte);//I2C_write_Two_byte(0x71,36,0x06);
	
	I2C_write_One_byte(0x70,37);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte |= (1<<1);
	rd_byte |= (1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	I2C_write_Two_byte(0x70,37,rd_byte);//I2C_write_Two_byte(0x71,37,0x00);
	
	I2C_write_One_byte(0x70,38);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	I2C_write_Two_byte(0x70,38,rd_byte);//I2C_write_Two_byte(0x71,38,0x00);
	
	I2C_write_One_byte(0x70,39);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	I2C_write_Two_byte(0x70,39,rd_byte);//I2C_write_Two_byte(0x71,39,0x00);
	I2C_write_Two_byte(0x70,40,0x63); //I2C_write_Two_byte(0x70,40,0x84);
	
	I2C_write_One_byte(0x70,41);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte |= (1<<2);
	rd_byte |= (1<<3);
	rd_byte |= (1<<7);  // JSA: 0x0C -> 0x8C
	I2C_write_Two_byte(0x70,41,rd_byte);//I2C_write_Two_byte(0x71,41,0x0C);
	
	I2C_write_One_byte(0x70,42);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte |= (1<<0);
	rd_byte |= (1<<1);
	rd_byte |= (1<<5);
	I2C_write_Two_byte(0x70,42,rd_byte);//I2C_write_Two_byte(0x71,42,0x23);
	//I2C_write_Two_byte(0x71,43,0x00);
	//I2C_write_Two_byte(0x71,44,0x00);
	I2C_write_Two_byte(0x70,45,0x00);
	I2C_write_Two_byte(0x70,46,0x00);
	I2C_write_Two_byte(0x70,47,0x14);
	I2C_write_Two_byte(0x70,48,0x22); //JSA: 0x1D --> 0x22
	I2C_write_Two_byte(0x70,49,0x80); //JSA: OX10 --> 0X80
	I2C_write_Two_byte(0x70,50,0xC4); //JSA: 0xC5 --> 0xC4
	I2C_write_Two_byte(0x70,51,0x07);
	
	I2C_write_One_byte(0x70,52);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte |= (1<<4);
	I2C_write_Two_byte(0x70,52,rd_byte);//I2C_write_Two_byte(0x71,52,0x10);
	I2C_write_Two_byte(0x70,53,0x00);
	I2C_write_Two_byte(0x70,54,0x02); //JSA 0x04->0x08-> 0x02
	I2C_write_Two_byte(0x70,55,0x00);
	I2C_write_Two_byte(0x70,56,0x00);
	I2C_write_Two_byte(0x70,57,0x00);
	I2C_write_Two_byte(0x70,58,0x00);
	I2C_write_Two_byte(0x70,59,0x01);
	I2C_write_Two_byte(0x70,60,0x00);
	I2C_write_Two_byte(0x70,61,0x00);
	
	I2C_write_One_byte(0x70,62);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	rd_byte &= ~(1<<5);
	I2C_write_Two_byte(0x70,62,rd_byte);//I2C_write_Two_byte(0x71,62,0x00);
	
	I2C_write_One_byte(0x70,63);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte |= (1<<4);
	I2C_write_Two_byte(0x70,63,rd_byte);//I2C_write_Two_byte(0x71,63,0x10);
	I2C_write_Two_byte(0x70,64,0x00);
	I2C_write_Two_byte(0x70,65,0x1E); // JSA: 0x1C -> 0x1E
	I2C_write_Two_byte(0x70,66,0x00);
	I2C_write_Two_byte(0x70,67,0x00);
	I2C_write_Two_byte(0x70,68,0x00);
	I2C_write_Two_byte(0x70,69,0x00);
	I2C_write_Two_byte(0x70,70,0x01);
	I2C_write_Two_byte(0x70,71,0x00);
	I2C_write_Two_byte(0x70,72,0x00);
	
	I2C_write_One_byte(0x70,73);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	rd_byte &= ~(1<<5);
	I2C_write_Two_byte(0x70,73,rd_byte);//I2C_write_Two_byte(0x71,73,0x00);
	
	I2C_write_One_byte(0x70,74);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte |= (1<<4);
	I2C_write_Two_byte(0x70,74,rd_byte);//I2C_write_Two_byte(0x71,74,0x10);
	I2C_write_Two_byte(0x70,75,0x00);
	I2C_write_Two_byte(0x70,76,0x00);
	I2C_write_Two_byte(0x70,77,0x00);
	I2C_write_Two_byte(0x70,78,0x00);
	I2C_write_Two_byte(0x70,79,0x00);
	I2C_write_Two_byte(0x70,80,0x00);
	I2C_write_Two_byte(0x70,81,0x00);
	I2C_write_Two_byte(0x70,82,0x00);
	I2C_write_Two_byte(0x70,83,0x00);
	
	I2C_write_One_byte(0x70,84);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	rd_byte &= ~(1<<5);
	I2C_write_Two_byte(0x70,84,rd_byte);//I2C_write_Two_byte(0x71,84,0x00);
	
	I2C_write_One_byte(0x70,85);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte |= (1<<4);
	I2C_write_Two_byte(0x70,85,rd_byte);//I2C_write_Two_byte(0x71,85,0x10);
	I2C_write_Two_byte(0x70,86,0x00);
	I2C_write_Two_byte(0x70,87,0x00);
	I2C_write_Two_byte(0x70,88,0x00);
	I2C_write_Two_byte(0x70,89,0x00);
	I2C_write_Two_byte(0x70,90,0x00);
	I2C_write_Two_byte(0x70,91,0x00);
	I2C_write_Two_byte(0x70,92,0x00);
	I2C_write_Two_byte(0x70,93,0x00);
	I2C_write_Two_byte(0x70,94,0x00);
	
	I2C_write_One_byte(0x70,95);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	rd_byte &= ~(1<<5);
	I2C_write_Two_byte(0x70,95,rd_byte);//I2C_write_Two_byte(0x71,95,0x00);
	//I2C_write_Two_byte(0x71,96,0x10);
	I2C_write_Two_byte(0x70,97,0x00);
	I2C_write_Two_byte(0x70,98,0x1E);  // JSA: 0x1C -> 0x1E
	I2C_write_Two_byte(0x70,99,0x00);
	I2C_write_Two_byte(0x70,100,0x00);
	I2C_write_Two_byte(0x70,101,0x00);
	I2C_write_Two_byte(0x70,102,0x00);
	I2C_write_Two_byte(0x70,103,0x01);
	I2C_write_Two_byte(0x70,104,0x00);
	I2C_write_Two_byte(0x70,105,0x00);
	
	I2C_write_One_byte(0x70,106);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte |= (1<<7);
	I2C_write_Two_byte(0x70,106,rd_byte);//I2C_write_Two_byte(0x70,106,0x80);
	I2C_write_Two_byte(0x70,107,0x00);
	
	I2C_write_One_byte(0x70,108);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	rd_byte &= ~(1<<5);
	rd_byte &= ~(1<<6);
	I2C_write_Two_byte(0x70,108,rd_byte);//I2C_write_Two_byte(0x70,108,0x00);
	I2C_write_Two_byte(0x70,109,0x00);
	I2C_write_Two_byte(0x70,110,0x40);
	I2C_write_Two_byte(0x70,111,0x00);
	
	I2C_write_One_byte(0x70,112);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	rd_byte &= ~(1<<5);
	rd_byte &= ~(1<<6);
	I2C_write_Two_byte(0x70,112,rd_byte);//I2C_write_Two_byte(0x70,112,0x00);
	I2C_write_Two_byte(0x70,113,0x00);
	I2C_write_Two_byte(0x70,114,0x40);
	I2C_write_Two_byte(0x70,115,0x00);
	I2C_write_Two_byte(0x70,116,0x80);
	I2C_write_Two_byte(0x70,117,0x00);
	I2C_write_Two_byte(0x70,118,0x40);
	I2C_write_Two_byte(0x70,119,0x00);
	I2C_write_Two_byte(0x70,120,0x00);
	I2C_write_Two_byte(0x70,121,0x00);
	I2C_write_Two_byte(0x70,122,0x40);
	I2C_write_Two_byte(0x70,123,0x00);
	I2C_write_Two_byte(0x70,124,0x00);
	I2C_write_Two_byte(0x70,125,0x00);
	I2C_write_Two_byte(0x70,126,0x00);
	I2C_write_Two_byte(0x70,127,0x00);
	I2C_write_Two_byte(0x70,128,0x00);
	
	I2C_write_One_byte(0x70,129);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	I2C_write_Two_byte(0x70,129,rd_byte);//I2C_write_Two_byte(0x70,129,0x00);
	
	I2C_write_One_byte(0x70,130);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	I2C_write_Two_byte(0x70,130,rd_byte);//I2C_write_Two_byte(0x70,130,0x00);
	I2C_write_Two_byte(0x70,131,0x00);
	I2C_write_Two_byte(0x70,132,0x00);
	I2C_write_Two_byte(0x70,133,0x00);
	I2C_write_Two_byte(0x70,134,0x00);
	I2C_write_Two_byte(0x70,135,0x00);
	I2C_write_Two_byte(0x70,136,0x00);
	I2C_write_Two_byte(0x70,137,0x00);
	I2C_write_Two_byte(0x70,138,0x00);
	I2C_write_Two_byte(0x70,139,0x00);
	I2C_write_Two_byte(0x70,140,0x00);
	I2C_write_Two_byte(0x70,141,0x00);
	I2C_write_Two_byte(0x70,142,0x00);
	I2C_write_Two_byte(0x70,143,0x00);
	I2C_write_Two_byte(0x70,144,0x00);
	//I2C_write_Two_byte(0x70,145,0x00);
	//I2C_write_Two_byte(0x70,146,0xFF);
	//I2C_write_Two_byte(0x70,147,0x00);
	//I2C_write_Two_byte(0x70,148,0x00);
	//I2C_write_Two_byte(0x70,149,0x00);
	//I2C_write_Two_byte(0x70,150,0x00);
	//I2C_write_Two_byte(0x70,151,0x00);
	I2C_write_Two_byte(0x70,152,0x00);
	I2C_write_Two_byte(0x70,153,0x00);
	I2C_write_Two_byte(0x70,154,0x00);
	I2C_write_Two_byte(0x70,155,0x00);
	I2C_write_Two_byte(0x70,156,0x00);
	I2C_write_Two_byte(0x70,157,0x00);
	
	I2C_write_One_byte(0x70,158);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	I2C_write_Two_byte(0x70,158,rd_byte);//I2C_write_Two_byte(0x70,158,0x00);
	
	I2C_write_One_byte(0x70,159);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	I2C_write_Two_byte(0x70,159,rd_byte);//I2C_write_Two_byte(0x70,159,0x00);
	I2C_write_Two_byte(0x70,160,0x00);
	I2C_write_Two_byte(0x70,161,0x00);
	I2C_write_Two_byte(0x70,162,0x00);
	I2C_write_Two_byte(0x70,163,0x00);
	I2C_write_Two_byte(0x70,164,0x00);
	I2C_write_Two_byte(0x70,165,0x00);
	I2C_write_Two_byte(0x70,166,0x00);
	I2C_write_Two_byte(0x70,167,0x00);
	I2C_write_Two_byte(0x70,168,0x00);
	I2C_write_Two_byte(0x70,169,0x00);
	I2C_write_Two_byte(0x70,170,0x00);
	I2C_write_Two_byte(0x70,171,0x00);
	I2C_write_Two_byte(0x70,172,0x00);
	I2C_write_Two_byte(0x70,173,0x00);
	I2C_write_Two_byte(0x70,174,0x00);
	I2C_write_Two_byte(0x70,175,0x00);
	I2C_write_Two_byte(0x70,176,0x00);
	I2C_write_Two_byte(0x70,177,0x00);
	I2C_write_Two_byte(0x70,178,0x00);
	I2C_write_Two_byte(0x70,179,0x00);
	I2C_write_Two_byte(0x70,180,0x00);
	
	I2C_write_One_byte(0x70,181);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	I2C_write_Two_byte(0x70,181,rd_byte);//I2C_write_Two_byte(0x70,181,0x00);
	I2C_write_Two_byte(0x70,182,0x00);
	I2C_write_Two_byte(0x70,183,0x00);
	I2C_write_Two_byte(0x70,184,0x00);
	I2C_write_Two_byte(0x70,185,0x00);
	I2C_write_Two_byte(0x70,186,0x00);
	I2C_write_Two_byte(0x70,187,0x00);
	I2C_write_Two_byte(0x70,188,0x00);
	I2C_write_Two_byte(0x70,189,0x00);
	I2C_write_Two_byte(0x70,190,0x00);
	I2C_write_Two_byte(0x70,191,0x00);
	I2C_write_Two_byte(0x70,192,0x00);
	I2C_write_Two_byte(0x70,193,0x00);
	I2C_write_Two_byte(0x70,194,0x00);
	I2C_write_Two_byte(0x70,195,0x00);
	I2C_write_Two_byte(0x70,196,0x00);
	I2C_write_Two_byte(0x70,197,0x00);
	I2C_write_Two_byte(0x70,198,0x00);
	I2C_write_Two_byte(0x70,199,0x00);
	I2C_write_Two_byte(0x70,200,0x00);
	I2C_write_Two_byte(0x70,201,0x00);
	I2C_write_Two_byte(0x70,202,0x00);
	
	I2C_write_One_byte(0x70,203);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	I2C_write_Two_byte(0x70,203,rd_byte);//I2C_write_Two_byte(0x70,203,0x00);
	I2C_write_Two_byte(0x70,204,0x00);
	I2C_write_Two_byte(0x70,205,0x00);
	I2C_write_Two_byte(0x70,206,0x00);
	I2C_write_Two_byte(0x70,207,0x00);
	I2C_write_Two_byte(0x70,208,0x00);
	I2C_write_Two_byte(0x70,209,0x00);
	I2C_write_Two_byte(0x70,210,0x00);
	I2C_write_Two_byte(0x70,211,0x00);
	I2C_write_Two_byte(0x70,212,0x00);
	I2C_write_Two_byte(0x70,213,0x00);
	I2C_write_Two_byte(0x70,214,0x00);
	I2C_write_Two_byte(0x70,215,0x00);
	I2C_write_Two_byte(0x70,216,0x00);
	I2C_write_Two_byte(0x70,217,0x00);
	//I2C_write_Two_byte(0x70,218,0x00);
	//I2C_write_Two_byte(0x70,219,0x00);
	//I2C_write_Two_byte(0x70,220,0x00);
	//I2C_write_Two_byte(0x70,221,0x0D);
	//I2C_write_Two_byte(0x70,222,0x00);
	//I2C_write_Two_byte(0x70,223,0x00);
	//I2C_write_Two_byte(0x70,224,0xF4);
	//I2C_write_Two_byte(0x70,225,0xF0);
	
	I2C_write_One_byte(0x70,226);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<2);
	I2C_write_Two_byte(0x70,226,rd_byte);//I2C_write_Two_byte(0x70,226,0x00);
	//I2C_write_Two_byte(0x70,227,0x00);
	//I2C_write_Two_byte(0x70,228,0x00);
	//I2C_write_Two_byte(0x70,229,0x00);
	//I2C_write_Two_byte(0x70,231,0x00);
	//I2C_write_Two_byte(0x70,232,0x00);
	//I2C_write_Two_byte(0x70,233,0x00);
	//I2C_write_Two_byte(0x70,234,0x00);
	//I2C_write_Two_byte(0x70,235,0x00);
	//I2C_write_Two_byte(0x70,236,0x00);
	//I2C_write_Two_byte(0x70,237,0x00);
	//I2C_write_Two_byte(0x70,238,0x14);
	//I2C_write_Two_byte(0x70,239,0x00);
	//I2C_write_Two_byte(0x70,240,0x00);
	
	I2C_write_One_byte(0x70,242);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<1);
	I2C_write_Two_byte(0x70,242,rd_byte);//I2C_write_Two_byte(0x70,242,0x00);
	//I2C_write_Two_byte(0x70,243,0xF0);
	//I2C_write_Two_byte(0x70,244,0x00);
	//I2C_write_Two_byte(0x70,245,0x00);
	//I2C_write_Two_byte(0x70,247,0x00);
	//I2C_write_Two_byte(0x70,248,0x00);
	//I2C_write_Two_byte(0x70,249,0xA8);
	//I2C_write_Two_byte(0x70,250,0x00);
	//I2C_write_Two_byte(0x70,251,0x84);
	//I2C_write_Two_byte(0x70,252,0x00);
	//I2C_write_Two_byte(0x70,253,0x00);
	//I2C_write_Two_byte(0x70,254,0x00);
	I2C_write_Two_byte(0x70,255,0x00);

	_delay_ms(1);
	
	// End of writing new configuration
	
	// Validate input clock status
	for (int i; i<10; i++)                // waits until reg218 = 0x08
	{	if (go_on == 1) {break;}
	I2C_write_One_byte(0x70,218);
	_delay_ms(0.2);
	I2C_read_One_byte(0x70,&rd_byte);
	if (rd_byte == 0x08)
	go_on = 1;
	else
	go_on = 0;
	_delay_ms(1);
	}





	go_on = 0;
	
	
	//Configure PLL for locking
	I2C_write_One_byte(0x70,49);          // writes '0' on bit 7 of reg 49 (FCAL_OVRD_EN = 0)
	_delay_ms(0.2);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<7);
	I2C_write_Two_byte(0x70,49,rd_byte);
	
	_delay_ms(1);

	// Initiate Locking of PLL
	I2C_write_Two_byte(0x70,246,0x02);
	
	_delay_ms(25);

	// Restart LOL
	I2C_write_Two_byte(0x70,241,0x65);
	
	_delay_ms(1);

	//while (go_on == 0)
	//{
	//I2C_write_One_byte(0x70,218);
	//_delay_ms(0.2);
	//I2C_read_One_byte(0x70,&rd_byte);
	//if (rd_byte == 0x08)
	//go_on = 1;
	//else
	//go_on = 0;
	//_delay_ms(1);
	//}
	
	// Confirm PLL lock status
	for (int i; i<10; i++)
	{	if (go_on == 1) {break;}
	I2C_write_One_byte(0x70,218);
	_delay_ms(0.2);
	I2C_read_One_byte(0x70,&rd_byte);
	if (rd_byte == 0x08)
	go_on = 1;
	else
	go_on = 0;
	_delay_ms(1);
	}

// Copy FCAL values to active registers
I2C_write_One_byte(0x70,237);
_delay_ms(0.2);
I2C_read_One_byte(0x70,&rd_byte);
rd_byte &= ~(1<<2);
rd_byte &= ~(1<<3);
rd_byte &= ~(1<<4);
rd_byte &= ~(1<<5);
rd_byte &= ~(1<<6);
rd_byte &= ~(1<<7);
rd_byte |= 0x14;
I2C_write_Two_byte(0x70,47,rd_byte);

_delay_ms(0.3);

I2C_write_One_byte(0x70,236);
_delay_ms(0.2);
I2C_read_One_byte(0x70,&rd_byte);
I2C_write_Two_byte(0x70,46,rd_byte);

_delay_ms(0.3);

I2C_write_One_byte(0x70,235);
_delay_ms(0.2);
I2C_read_One_byte(0x70,&rd_byte);
I2C_write_Two_byte(0x70,45,rd_byte);

_delay_ms(0.3);


// Set PLL to use FCAL values
I2C_write_One_byte(0x70,49);
_delay_ms(0.2);
I2C_read_One_byte(0x70,&rd_byte);
rd_byte |= (1<<7);
I2C_write_Two_byte(0x70,49,rd_byte);

_delay_ms(1);


// Enable Outputs
I2C_write_Two_byte(0x70,230,0x00);

}


void PLL_config_125()     // I2C ADDRESS = 0x71
{
	
	int go_on = 0;
	
	//	DDRB|= (1<<5);
	//
	//while (1) {
	//
	//PORTB &= (0<<5);
	//_delay_ms(500);
	//PORTB |= (1<<5);
	//_delay_ms(500);
	//	
	DDRE |= (1<<PS0_PLL);
	DDRE |= (1<<PS1_PLL);
	PORTE &= ~(1<<PS0_PLL);
	PORTE &= ~(1<<PS1_PLL);
	//PORTE |= (1<<PS1_PLL);
	
	SFIOR |= (1<<PUD);
	
	DDRD |= (1<<SDA_PLL);
	DDRD |= (1<<SCL_PLL);
	PORTD |= (1<<SCL_PLL);
	PORTD |= (1<<SDA_PLL);
	
	_delay_ms(1000);
	
	I2C_write_One_byte(0x74,0x08);
	
	_delay_ms(10);
	
	I2C_write_Two_byte(0x71,230,0x10);
	//
	_delay_ms(1);
	
	I2C_write_Two_byte(0x71,241,0xE5);
	//
	_delay_ms(1);
	//
	//I2C_write_Two_byte(0x71,0,0x00);
	//I2C_write_Two_byte(0x71,1,0x00);
	//I2C_write_Two_byte(0x71,2,0x00);
	//I2C_write_Two_byte(0x71,3,0x00);
	//I2C_write_Two_byte(0x71,4,0x00);
	//I2C_write_Two_byte(0x71,5,0x00);
	I2C_write_One_byte(0x71,6);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte |= (1<<3);
	I2C_write_Two_byte(0x71,6,rd_byte); //I2C_write_Two_byte(0x71,6,0x08);
	//I2C_write_Two_byte(0x71,7,0x00);
	//I2C_write_Two_byte(0x71,8,0x70);
	//I2C_write_Two_byte(0x71,9,0x0F);
	//I2C_write_Two_byte(0x71,10,0x00);
	//I2C_write_Two_byte(0x71,11,0x00);
	//I2C_write_Two_byte(0x71,12,0x00);
	//I2C_write_Two_byte(0x71,13,0x00);
	//I2C_write_Two_byte(0x71,14,0x00);
	//I2C_write_Two_byte(0x71,15,0x00);
	//I2C_write_Two_byte(0x71,16,0x00);
	//I2C_write_Two_byte(0x71,17,0x00);
	//I2C_write_Two_byte(0x71,18,0x00);
	//I2C_write_Two_byte(0x71,19,0x00);
	//I2C_write_Two_byte(0x71,20,0x00);
	//I2C_write_Two_byte(0x71,21,0x00);
	//I2C_write_Two_byte(0x71,22,0x00);
	//I2C_write_Two_byte(0x71,23,0x00);
	//I2C_write_Two_byte(0x71,24,0x00);
	//I2C_write_Two_byte(0x71,25,0x00);
	//I2C_write_Two_byte(0x71,26,0x00);
	
	//I2C_write_One_byte(0x71,27);
	//_delay_ms(0.1);
	//I2C_read_One_byte(0x71,&rd_byte);
	//rd_byte |= (1<<4);
	//rd_byte |= (1<<6);
	//I2C_write_Two_byte(0x71,27,rd_byte);//I2C_write_Two_byte(0x71,27,0x70);
	
	I2C_write_Two_byte(0x71,28,0x16);
	I2C_write_Two_byte(0x71,29,0x90);
	I2C_write_Two_byte(0x71,30,0xB0);
	I2C_write_Two_byte(0x71,31,0xC0);
	I2C_write_Two_byte(0x71,32,0xE3);
	I2C_write_Two_byte(0x71,33,0xE3);
	I2C_write_Two_byte(0x71,34,0xE3);
	I2C_write_Two_byte(0x71,35,0x01);
	
	I2C_write_One_byte(0x71,36);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte |= (1<<1);
	rd_byte |= (1<<2);
	I2C_write_Two_byte(0x71,36,rd_byte);//I2C_write_Two_byte(0x71,36,0x06);
	
	I2C_write_One_byte(0x71,37);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	I2C_write_Two_byte(0x71,37,rd_byte);//I2C_write_Two_byte(0x71,37,0x00);
	
	I2C_write_One_byte(0x71,38);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	I2C_write_Two_byte(0x71,38,rd_byte);//I2C_write_Two_byte(0x71,38,0x00);
	
	I2C_write_One_byte(0x71,39);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	I2C_write_Two_byte(0x71,39,rd_byte);//I2C_write_Two_byte(0x71,39,0x00);
	I2C_write_Two_byte(0x71,40,0x64);
	
	I2C_write_One_byte(0x71,41);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte |= (1<<2);
	rd_byte |= (1<<3);
	I2C_write_Two_byte(0x71,41,rd_byte);//I2C_write_Two_byte(0x71,41,0x0C);
	
	I2C_write_One_byte(0x71,42);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte |= (1<<0);
	rd_byte |= (1<<1);
	rd_byte |= (1<<5);
	I2C_write_Two_byte(0x71,42,rd_byte);//I2C_write_Two_byte(0x71,42,0x23);
	//I2C_write_Two_byte(0x71,43,0x00);
	//I2C_write_Two_byte(0x71,44,0x00);
	I2C_write_Two_byte(0x71,45,0x00);
	I2C_write_Two_byte(0x71,46,0x00);
	I2C_write_Two_byte(0x71,47,0x14);
	I2C_write_Two_byte(0x71,48,0x3A);
	I2C_write_Two_byte(0x71,49,0x00);
	I2C_write_Two_byte(0x71,50,0xC4);
	I2C_write_Two_byte(0x71,51,0x07);
	
	I2C_write_One_byte(0x71,52);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte |= (1<<4);
	I2C_write_Two_byte(0x71,52,rd_byte);//I2C_write_Two_byte(0x71,52,0x10);
	I2C_write_Two_byte(0x71,53,0x00);
	I2C_write_Two_byte(0x71,54,0x08);
	I2C_write_Two_byte(0x71,55,0x00);
	I2C_write_Two_byte(0x71,56,0x00);
	I2C_write_Two_byte(0x71,57,0x00);
	I2C_write_Two_byte(0x71,58,0x00);
	I2C_write_Two_byte(0x71,59,0x01);
	I2C_write_Two_byte(0x71,60,0x00);
	I2C_write_Two_byte(0x71,61,0x00);
	
	I2C_write_One_byte(0x71,62);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	rd_byte &= ~(1<<5);
	I2C_write_Two_byte(0x71,62,rd_byte);//I2C_write_Two_byte(0x71,62,0x00);
	
	I2C_write_One_byte(0x71,63);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte |= (1<<4);
	I2C_write_Two_byte(0x71,63,rd_byte);//I2C_write_Two_byte(0x71,63,0x10);
	I2C_write_Two_byte(0x71,64,0x00);
	I2C_write_Two_byte(0x71,65,0x00);
	I2C_write_Two_byte(0x71,66,0x00);
	I2C_write_Two_byte(0x71,67,0x00);
	I2C_write_Two_byte(0x71,68,0x00);
	I2C_write_Two_byte(0x71,69,0x00);
	I2C_write_Two_byte(0x71,70,0x00);
	I2C_write_Two_byte(0x71,71,0x00);
	I2C_write_Two_byte(0x71,72,0x00);
	
	I2C_write_One_byte(0x71,73);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	rd_byte &= ~(1<<5);
	I2C_write_Two_byte(0x71,73,rd_byte);//I2C_write_Two_byte(0x71,73,0x00);
	
	I2C_write_One_byte(0x71,74);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte |= (1<<4);
	I2C_write_Two_byte(0x71,74,rd_byte);//I2C_write_Two_byte(0x71,74,0x10);
	I2C_write_Two_byte(0x71,75,0x00);
	I2C_write_Two_byte(0x71,76,0x00);
	I2C_write_Two_byte(0x71,77,0x00);
	I2C_write_Two_byte(0x71,78,0x00);
	I2C_write_Two_byte(0x71,79,0x00);
	I2C_write_Two_byte(0x71,80,0x00);
	I2C_write_Two_byte(0x71,81,0x00);
	I2C_write_Two_byte(0x71,82,0x00);
	I2C_write_Two_byte(0x71,83,0x00);
	
	I2C_write_One_byte(0x71,84);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	rd_byte &= ~(1<<5);
	I2C_write_Two_byte(0x71,84,rd_byte);//I2C_write_Two_byte(0x71,84,0x00);
	
	I2C_write_One_byte(0x71,85);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte |= (1<<4);
	I2C_write_Two_byte(0x71,85,rd_byte);//I2C_write_Two_byte(0x71,85,0x10);
	I2C_write_Two_byte(0x71,86,0x00);
	I2C_write_Two_byte(0x71,87,0x00);
	I2C_write_Two_byte(0x71,88,0x00);
	I2C_write_Two_byte(0x71,89,0x00);
	I2C_write_Two_byte(0x71,90,0x00);
	I2C_write_Two_byte(0x71,91,0x00);
	I2C_write_Two_byte(0x71,92,0x00);
	I2C_write_Two_byte(0x71,93,0x00);
	I2C_write_Two_byte(0x71,94,0x00);
	
	I2C_write_One_byte(0x71,95);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	rd_byte &= ~(1<<5);
	I2C_write_Two_byte(0x71,95,rd_byte);//I2C_write_Two_byte(0x71,95,0x00);
	//I2C_write_Two_byte(0x71,96,0x10);
	I2C_write_Two_byte(0x71,97,0x00);
	I2C_write_Two_byte(0x71,98,0x30);
	I2C_write_Two_byte(0x71,99,0x00);
	I2C_write_Two_byte(0x71,100,0x00);
	I2C_write_Two_byte(0x71,101,0x00);
	I2C_write_Two_byte(0x71,102,0x00);
	I2C_write_Two_byte(0x71,103,0x01);
	I2C_write_Two_byte(0x71,104,0x00);
	I2C_write_Two_byte(0x71,105,0x00);
	
	I2C_write_One_byte(0x71,106);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte |= (1<<7);
	I2C_write_Two_byte(0x71,106,rd_byte);//I2C_write_Two_byte(0x71,106,0x80);
	I2C_write_Two_byte(0x71,107,0x00);
	
	I2C_write_One_byte(0x71,108);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	rd_byte &= ~(1<<5);
	rd_byte &= ~(1<<6);
	I2C_write_Two_byte(0x71,108,rd_byte);//I2C_write_Two_byte(0x71,108,0x00);
	I2C_write_Two_byte(0x71,109,0x00);
	I2C_write_Two_byte(0x71,110,0x40);
	I2C_write_Two_byte(0x71,111,0x00);
	
	I2C_write_One_byte(0x71,112);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	rd_byte &= ~(1<<5);
	rd_byte &= ~(1<<6);
	I2C_write_Two_byte(0x71,112,rd_byte);//I2C_write_Two_byte(0x71,112,0x00);
	I2C_write_Two_byte(0x71,113,0x00);
	I2C_write_Two_byte(0x71,114,0x40);
	I2C_write_Two_byte(0x71,115,0x00);
	I2C_write_Two_byte(0x71,116,0x80);
	I2C_write_Two_byte(0x71,117,0x00);
	I2C_write_Two_byte(0x71,118,0x40);
	I2C_write_Two_byte(0x71,119,0x00);
	I2C_write_Two_byte(0x71,120,0x00);
	I2C_write_Two_byte(0x71,121,0x00);
	I2C_write_Two_byte(0x71,122,0x40);
	I2C_write_Two_byte(0x71,123,0x00);
	I2C_write_Two_byte(0x71,124,0x00);
	I2C_write_Two_byte(0x71,125,0x00);
	I2C_write_Two_byte(0x71,126,0x00);
	I2C_write_Two_byte(0x71,127,0x00);
	I2C_write_Two_byte(0x71,128,0x00);
	
	I2C_write_One_byte(0x71,129);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	I2C_write_Two_byte(0x71,129,rd_byte);//I2C_write_Two_byte(0x71,129,0x00);
	
	I2C_write_One_byte(0x71,130);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	I2C_write_Two_byte(0x71,130,rd_byte);//I2C_write_Two_byte(0x71,130,0x00);
	I2C_write_Two_byte(0x71,131,0x00);
	I2C_write_Two_byte(0x71,132,0x00);
	I2C_write_Two_byte(0x71,133,0x00);
	I2C_write_Two_byte(0x71,134,0x00);
	I2C_write_Two_byte(0x71,135,0x00);
	I2C_write_Two_byte(0x71,136,0x00);
	I2C_write_Two_byte(0x71,137,0x00);
	I2C_write_Two_byte(0x71,138,0x00);
	I2C_write_Two_byte(0x71,139,0x00);
	I2C_write_Two_byte(0x71,140,0x00);
	I2C_write_Two_byte(0x71,141,0x00);
	I2C_write_Two_byte(0x71,142,0x00);
	I2C_write_Two_byte(0x71,143,0x00);
	I2C_write_Two_byte(0x71,144,0x00);
	//I2C_write_Two_byte(0x71,145,0x00);
	//I2C_write_Two_byte(0x71,146,0xFF);
	//I2C_write_Two_byte(0x71,147,0x00);
	//I2C_write_Two_byte(0x71,148,0x00);
	//I2C_write_Two_byte(0x71,149,0x00);
	//I2C_write_Two_byte(0x71,150,0x00);
	//I2C_write_Two_byte(0x71,151,0x00);
	I2C_write_Two_byte(0x71,152,0x00);
	I2C_write_Two_byte(0x71,153,0x00);
	I2C_write_Two_byte(0x71,154,0x00);
	I2C_write_Two_byte(0x71,155,0x00);
	I2C_write_Two_byte(0x71,156,0x00);
	I2C_write_Two_byte(0x71,157,0x00);
	
	I2C_write_One_byte(0x71,158);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	I2C_write_Two_byte(0x71,158,rd_byte);//I2C_write_Two_byte(0x71,158,0x00);
	
	I2C_write_One_byte(0x71,159);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	I2C_write_Two_byte(0x71,159,rd_byte);//I2C_write_Two_byte(0x71,159,0x00);
	I2C_write_Two_byte(0x71,160,0x00);
	I2C_write_Two_byte(0x71,161,0x00);
	I2C_write_Two_byte(0x71,162,0x00);
	I2C_write_Two_byte(0x71,163,0x00);
	I2C_write_Two_byte(0x71,164,0x00);
	I2C_write_Two_byte(0x71,165,0x00);
	I2C_write_Two_byte(0x71,166,0x00);
	I2C_write_Two_byte(0x71,167,0x00);
	I2C_write_Two_byte(0x71,168,0x00);
	I2C_write_Two_byte(0x71,169,0x00);
	I2C_write_Two_byte(0x71,170,0x00);
	I2C_write_Two_byte(0x71,171,0x00);
	I2C_write_Two_byte(0x71,172,0x00);
	I2C_write_Two_byte(0x71,173,0x00);
	I2C_write_Two_byte(0x71,174,0x00);
	I2C_write_Two_byte(0x71,175,0x00);
	I2C_write_Two_byte(0x71,176,0x00);
	I2C_write_Two_byte(0x71,177,0x00);
	I2C_write_Two_byte(0x71,178,0x00);
	I2C_write_Two_byte(0x71,179,0x00);
	I2C_write_Two_byte(0x71,180,0x00);
	
	I2C_write_One_byte(0x71,181);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	I2C_write_Two_byte(0x71,181,rd_byte);//I2C_write_Two_byte(0x71,181,0x00);
	I2C_write_Two_byte(0x71,182,0x00);
	I2C_write_Two_byte(0x71,183,0x00);
	I2C_write_Two_byte(0x71,184,0x00);
	I2C_write_Two_byte(0x71,185,0x00);
	I2C_write_Two_byte(0x71,186,0x00);
	I2C_write_Two_byte(0x71,187,0x00);
	I2C_write_Two_byte(0x71,188,0x00);
	I2C_write_Two_byte(0x71,189,0x00);
	I2C_write_Two_byte(0x71,190,0x00);
	I2C_write_Two_byte(0x71,191,0x00);
	I2C_write_Two_byte(0x71,192,0x00);
	I2C_write_Two_byte(0x71,193,0x00);
	I2C_write_Two_byte(0x71,194,0x00);
	I2C_write_Two_byte(0x71,195,0x00);
	I2C_write_Two_byte(0x71,196,0x00);
	I2C_write_Two_byte(0x71,197,0x00);
	I2C_write_Two_byte(0x71,198,0x00);
	I2C_write_Two_byte(0x71,199,0x00);
	I2C_write_Two_byte(0x71,200,0x00);
	I2C_write_Two_byte(0x71,201,0x00);
	I2C_write_Two_byte(0x71,202,0x00);
	
	I2C_write_One_byte(0x71,203);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	I2C_write_Two_byte(0x71,203,rd_byte);//I2C_write_Two_byte(0x71,203,0x00);
	I2C_write_Two_byte(0x71,204,0x00);
	I2C_write_Two_byte(0x71,205,0x00);
	I2C_write_Two_byte(0x71,206,0x00);
	I2C_write_Two_byte(0x71,207,0x00);
	I2C_write_Two_byte(0x71,208,0x00);
	I2C_write_Two_byte(0x71,209,0x00);
	I2C_write_Two_byte(0x71,210,0x00);
	I2C_write_Two_byte(0x71,211,0x00);
	I2C_write_Two_byte(0x71,212,0x00);
	I2C_write_Two_byte(0x71,213,0x00);
	I2C_write_Two_byte(0x71,214,0x00);
	I2C_write_Two_byte(0x71,215,0x00);
	I2C_write_Two_byte(0x71,216,0x00);
	I2C_write_Two_byte(0x71,217,0x00);
	//I2C_write_Two_byte(0x71,218,0x00);
	//I2C_write_Two_byte(0x71,219,0x00);
	//I2C_write_Two_byte(0x71,220,0x00);
	//I2C_write_Two_byte(0x71,221,0x0D);
	//I2C_write_Two_byte(0x71,222,0x00);
	//I2C_write_Two_byte(0x71,223,0x00);
	//I2C_write_Two_byte(0x71,224,0xF4);
	//I2C_write_Two_byte(0x71,225,0xF0);
	
	I2C_write_One_byte(0x71,226);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte &= ~(1<<2);
	I2C_write_Two_byte(0x71,226,rd_byte);//I2C_write_Two_byte(0x71,226,0x00);
	//I2C_write_Two_byte(0x71,227,0x00);
	//I2C_write_Two_byte(0x71,228,0x00);
	//I2C_write_Two_byte(0x71,229,0x00);
	//I2C_write_Two_byte(0x71,231,0x00);
	//I2C_write_Two_byte(0x71,232,0x00);
	//I2C_write_Two_byte(0x71,233,0x00);
	//I2C_write_Two_byte(0x71,234,0x00);
	//I2C_write_Two_byte(0x71,235,0x00);
	//I2C_write_Two_byte(0x71,236,0x00);
	//I2C_write_Two_byte(0x71,237,0x00);
	//I2C_write_Two_byte(0x71,238,0x14);
	//I2C_write_Two_byte(0x71,239,0x00);
	//I2C_write_Two_byte(0x71,240,0x00);
	
	I2C_write_One_byte(0x71,242);
	_delay_ms(0.1);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte &= ~(1<<1);
	I2C_write_Two_byte(0x71,242,rd_byte);//I2C_write_Two_byte(0x71,242,0x00);
	//I2C_write_Two_byte(0x71,243,0xF0);
	//I2C_write_Two_byte(0x71,244,0x00);
	//I2C_write_Two_byte(0x71,245,0x00);
	//I2C_write_Two_byte(0x71,247,0x00);
	//I2C_write_Two_byte(0x71,248,0x00);
	//I2C_write_Two_byte(0x71,249,0xA8);
	//I2C_write_Two_byte(0x71,250,0x00);
	//I2C_write_Two_byte(0x71,251,0x84);
	//I2C_write_Two_byte(0x71,252,0x00);
	//I2C_write_Two_byte(0x71,253,0x00);
	//I2C_write_Two_byte(0x71,254,0x00);
	I2C_write_Two_byte(0x71,255,0x00);

	_delay_ms(1);

	while (go_on == 0)
	{
		I2C_write_One_byte(0x71,218);
		_delay_ms(0.2);
		I2C_read_One_byte(0x71,&rd_byte);
		if (rd_byte == 0x08)
		go_on = 1;
		else
		go_on = 0;
		_delay_ms(1);
	}

	go_on = 0;
	
	I2C_write_One_byte(0x71,49);
	_delay_ms(0.2);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte &= ~(1<<7);
	I2C_write_Two_byte(0x71,49,rd_byte);
	
	_delay_ms(1);

	I2C_write_Two_byte(0x71,246,0x02);
	
	_delay_ms(25);

	I2C_write_Two_byte(0x71,241,0x65);
	
	_delay_ms(1);

	while (go_on == 0)
	{
		I2C_write_One_byte(0x71,218);
		_delay_ms(0.2);
		I2C_read_One_byte(0x71,&rd_byte);
		if (rd_byte == 0x08)
		go_on = 1;
		else
		go_on = 0;
		_delay_ms(1);
	}

	I2C_write_One_byte(0x71,237);
	_delay_ms(0.2);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	rd_byte &= ~(1<<5);
	rd_byte &= ~(1<<6);
	rd_byte &= ~(1<<7);
	rd_byte |= 0x14;
	I2C_write_Two_byte(0x71,47,rd_byte);
	
	_delay_ms(0.3);
	
	I2C_write_One_byte(0x71,236);
	_delay_ms(0.2);
	I2C_read_One_byte(0x71,&rd_byte);
	I2C_write_Two_byte(0x71,46,rd_byte);
	
	_delay_ms(0.3);
	
	I2C_write_One_byte(0x71,235);
	_delay_ms(0.2);
	I2C_read_One_byte(0x71,&rd_byte);
	I2C_write_Two_byte(0x71,45,rd_byte);
	
	_delay_ms(0.3);

	I2C_write_One_byte(0x71,49);
	_delay_ms(0.2);
	I2C_read_One_byte(0x71,&rd_byte);
	rd_byte |= (1<<7);
	I2C_write_Two_byte(0x71,49,rd_byte);
	
	_delay_ms(1);

	I2C_write_Two_byte(0x71,230,0x00);
	
	
	//I2C_write_Two_byte(0x71,230,0x10);
	//_delay_ms(1);
	//I2C_write_One_byte(0x71,230);
	//_delay_ms(0.2);
	//I2C_read_One_byte(0x71,&rd_byte);
	//_delay_ms(1);
	//I2C_write_Two_byte(0x71,230,rd_byte);
	
	//if(rd_byte==0)
	//PORTB &= ~(1<<5);
	//else
	//PORTB |= (1<<5);
	
	//SFIOR |= (1<<PUD);
	//
	//DDRD &= ~(1<<SDA_PLL);
	//DDRD &= ~(1<<SCL_PLL);
	
	//return(0);
}



void PLL_config_LHC()     // I2C ADDRESS = 0x70
{
	
	int go_on = 0;
	
	//	DDRB|= (1<<5);
	//
	//while (1) {
	//
	//PORTB &= (0<<5);
	//_delay_ms(500);
	//PORTB |= (1<<5);
	//_delay_ms(500);
	//
	
	DDRE |= (1<<PS0_PLL);
	DDRE |= (1<<PS1_PLL);
	PORTE &= ~(1<<PS0_PLL);
	PORTE &= ~(1<<PS1_PLL);
	//PORTE |= (1<<PS1_PLL);
	
	SFIOR |= (1<<PUD);
	
	DDRD |= (1<<SDA_PLL);
	DDRD |= (1<<SCL_PLL);
	PORTD |= (1<<SCL_PLL);
	PORTD |= (1<<SDA_PLL);
	
	_delay_ms(1000);
	
	I2C_write_One_byte(0x74,0x04);
	
	_delay_ms(10);
	
	I2C_write_Two_byte(0x70,230,0x10);
	//
	_delay_ms(1);
	
	I2C_write_Two_byte(0x70,241,0xE5);
	//
	_delay_ms(1);
	//
	//I2C_write_Two_byte(0x71,0,0x00);
	//I2C_write_Two_byte(0x71,1,0x00);
	//I2C_write_Two_byte(0x71,2,0x00);
	//I2C_write_Two_byte(0x71,3,0x00);
	//I2C_write_Two_byte(0x71,4,0x00);
	//I2C_write_Two_byte(0x71,5,0x00);
	I2C_write_One_byte(0x70,6);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte |= (1<<3);
	I2C_write_Two_byte(0x70,6,rd_byte); //I2C_write_Two_byte(0x71,6,0x08);
	//I2C_write_Two_byte(0x71,7,0x00);
	//I2C_write_Two_byte(0x71,8,0x70);
	//I2C_write_Two_byte(0x71,9,0x0F);
	//I2C_write_Two_byte(0x71,10,0x00);
	//I2C_write_Two_byte(0x71,11,0x00);
	//I2C_write_Two_byte(0x71,12,0x00);
	//I2C_write_Two_byte(0x71,13,0x00);
	//I2C_write_Two_byte(0x71,14,0x00);
	//I2C_write_Two_byte(0x71,15,0x00);
	//I2C_write_Two_byte(0x71,16,0x00);
	//I2C_write_Two_byte(0x71,17,0x00);
	//I2C_write_Two_byte(0x71,18,0x00);
	//I2C_write_Two_byte(0x71,19,0x00);
	//I2C_write_Two_byte(0x71,20,0x00);
	//I2C_write_Two_byte(0x71,21,0x00);
	//I2C_write_Two_byte(0x71,22,0x00);
	//I2C_write_Two_byte(0x71,23,0x00);
	//I2C_write_Two_byte(0x71,24,0x00);
	//I2C_write_Two_byte(0x71,25,0x00);
	//I2C_write_Two_byte(0x71,26,0x00);
	
	//I2C_write_One_byte(0x71,27);
	//_delay_ms(0.1);
	//I2C_read_One_byte(0x71,&rd_byte);
	//rd_byte |= (1<<4);
	//rd_byte |= (1<<6);
	//I2C_write_Two_byte(0x71,27,rd_byte);//I2C_write_Two_byte(0x71,27,0x70);
	
	I2C_write_Two_byte(0x70,28,0x03);
	I2C_write_Two_byte(0x70,29,0x00);
	I2C_write_Two_byte(0x70,30,0xB0);
	I2C_write_Two_byte(0x70,31,0xC0);
	I2C_write_Two_byte(0x70,32,0xE3);
	I2C_write_Two_byte(0x70,33,0xE3);
	I2C_write_Two_byte(0x70,34,0xE3);
	I2C_write_Two_byte(0x70,35,0x01);
	
	I2C_write_One_byte(0x70,36);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte |= (1<<1);
	rd_byte |= (1<<2);
	I2C_write_Two_byte(0x70,36,rd_byte);//I2C_write_Two_byte(0x71,36,0x06);
	
	I2C_write_One_byte(0x70,37);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	I2C_write_Two_byte(0x70,37,rd_byte);//I2C_write_Two_byte(0x71,37,0x00);
	
	I2C_write_One_byte(0x70,38);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	I2C_write_Two_byte(0x70,38,rd_byte);//I2C_write_Two_byte(0x71,38,0x00);
	
	I2C_write_One_byte(0x70,39);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	I2C_write_Two_byte(0x70,39,rd_byte);//I2C_write_Two_byte(0x71,39,0x00);
	I2C_write_Two_byte(0x70,40,0x64);
	
	I2C_write_One_byte(0x70,41);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte |= (1<<2);
	rd_byte |= (1<<3);
	I2C_write_Two_byte(0x70,41,rd_byte);//I2C_write_Two_byte(0x71,41,0x0C);
	
	I2C_write_One_byte(0x70,42);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte |= (1<<0);
	rd_byte |= (1<<1);
	rd_byte |= (1<<5);
	I2C_write_Two_byte(0x70,42,rd_byte);//I2C_write_Two_byte(0x71,42,0x23);
	//I2C_write_Two_byte(0x71,43,0x00);
	//I2C_write_Two_byte(0x71,44,0x00);
	I2C_write_Two_byte(0x70,45,0x00);
	I2C_write_Two_byte(0x70,46,0x00);
	I2C_write_Two_byte(0x70,47,0x14);
	I2C_write_Two_byte(0x70,48,0x1D);
	I2C_write_Two_byte(0x70,49,0x10);
	I2C_write_Two_byte(0x70,50,0xC5);
	I2C_write_Two_byte(0x70,51,0x07);
	
	I2C_write_One_byte(0x70,52);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte |= (1<<4);
	I2C_write_Two_byte(0x70,52,rd_byte);//I2C_write_Two_byte(0x71,52,0x10);
	I2C_write_Two_byte(0x70,53,0x00);
	I2C_write_Two_byte(0x70,54,0x04);
	I2C_write_Two_byte(0x70,55,0x00);
	I2C_write_Two_byte(0x70,56,0x00);
	I2C_write_Two_byte(0x70,57,0x00);
	I2C_write_Two_byte(0x70,58,0x00);
	I2C_write_Two_byte(0x70,59,0x01);
	I2C_write_Two_byte(0x70,60,0x00);
	I2C_write_Two_byte(0x70,61,0x00);
	
	I2C_write_One_byte(0x70,62);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	rd_byte &= ~(1<<5);
	I2C_write_Two_byte(0x70,62,rd_byte);//I2C_write_Two_byte(0x71,62,0x00);
	
	I2C_write_One_byte(0x70,63);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte |= (1<<4);
	I2C_write_Two_byte(0x70,63,rd_byte);//I2C_write_Two_byte(0x71,63,0x10);
	I2C_write_Two_byte(0x70,64,0x00);
	I2C_write_Two_byte(0x70,65,0x00);
	I2C_write_Two_byte(0x70,66,0x00);
	I2C_write_Two_byte(0x70,67,0x00);
	I2C_write_Two_byte(0x70,68,0x00);
	I2C_write_Two_byte(0x70,69,0x00);
	I2C_write_Two_byte(0x70,70,0x00);
	I2C_write_Two_byte(0x70,71,0x00);
	I2C_write_Two_byte(0x70,72,0x00);
	
	I2C_write_One_byte(0x70,73);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	rd_byte &= ~(1<<5);
	I2C_write_Two_byte(0x70,73,rd_byte);//I2C_write_Two_byte(0x71,73,0x00);
	
	I2C_write_One_byte(0x70,74);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte |= (1<<4);
	I2C_write_Two_byte(0x70,74,rd_byte);//I2C_write_Two_byte(0x71,74,0x10);
	I2C_write_Two_byte(0x70,75,0x00);
	I2C_write_Two_byte(0x70,76,0x00);
	I2C_write_Two_byte(0x70,77,0x00);
	I2C_write_Two_byte(0x70,78,0x00);
	I2C_write_Two_byte(0x70,79,0x00);
	I2C_write_Two_byte(0x70,80,0x00);
	I2C_write_Two_byte(0x70,81,0x00);
	I2C_write_Two_byte(0x70,82,0x00);
	I2C_write_Two_byte(0x70,83,0x00);
	
	I2C_write_One_byte(0x70,84);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	rd_byte &= ~(1<<5);
	I2C_write_Two_byte(0x70,84,rd_byte);//I2C_write_Two_byte(0x71,84,0x00);
	
	I2C_write_One_byte(0x70,85);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte |= (1<<4);
	I2C_write_Two_byte(0x70,85,rd_byte);//I2C_write_Two_byte(0x71,85,0x10);
	I2C_write_Two_byte(0x70,86,0x00);
	I2C_write_Two_byte(0x70,87,0x00);
	I2C_write_Two_byte(0x70,88,0x00);
	I2C_write_Two_byte(0x70,89,0x00);
	I2C_write_Two_byte(0x70,90,0x00);
	I2C_write_Two_byte(0x70,91,0x00);
	I2C_write_Two_byte(0x70,92,0x00);
	I2C_write_Two_byte(0x70,93,0x00);
	I2C_write_Two_byte(0x70,94,0x00);
	
	I2C_write_One_byte(0x70,95);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	rd_byte &= ~(1<<5);
	I2C_write_Two_byte(0x70,95,rd_byte);//I2C_write_Two_byte(0x71,95,0x00);
	//I2C_write_Two_byte(0x71,96,0x10);
	I2C_write_Two_byte(0x70,97,0x00);
	I2C_write_Two_byte(0x70,98,0x1C);
	I2C_write_Two_byte(0x70,99,0x00);
	I2C_write_Two_byte(0x70,100,0x00);
	I2C_write_Two_byte(0x70,101,0x00);
	I2C_write_Two_byte(0x70,102,0x00);
	I2C_write_Two_byte(0x70,103,0x01);
	I2C_write_Two_byte(0x70,104,0x00);
	I2C_write_Two_byte(0x70,105,0x00);
	
	I2C_write_One_byte(0x70,106);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte |= (1<<7);
	I2C_write_Two_byte(0x70,106,rd_byte);//I2C_write_Two_byte(0x70,106,0x80);
	I2C_write_Two_byte(0x70,107,0x00);
	
	I2C_write_One_byte(0x70,108);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	rd_byte &= ~(1<<5);
	rd_byte &= ~(1<<6);
	I2C_write_Two_byte(0x70,108,rd_byte);//I2C_write_Two_byte(0x70,108,0x00);
	I2C_write_Two_byte(0x70,109,0x00);
	I2C_write_Two_byte(0x70,110,0x40);
	I2C_write_Two_byte(0x70,111,0x00);
	
	I2C_write_One_byte(0x70,112);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	rd_byte &= ~(1<<5);
	rd_byte &= ~(1<<6);
	I2C_write_Two_byte(0x70,112,rd_byte);//I2C_write_Two_byte(0x70,112,0x00);
	I2C_write_Two_byte(0x70,113,0x00);
	I2C_write_Two_byte(0x70,114,0x40);
	I2C_write_Two_byte(0x70,115,0x00);
	I2C_write_Two_byte(0x70,116,0x80);
	I2C_write_Two_byte(0x70,117,0x00);
	I2C_write_Two_byte(0x70,118,0x40);
	I2C_write_Two_byte(0x70,119,0x00);
	I2C_write_Two_byte(0x70,120,0x00);
	I2C_write_Two_byte(0x70,121,0x00);
	I2C_write_Two_byte(0x70,122,0x40);
	I2C_write_Two_byte(0x70,123,0x00);
	I2C_write_Two_byte(0x70,124,0x00);
	I2C_write_Two_byte(0x70,125,0x00);
	I2C_write_Two_byte(0x70,126,0x00);
	I2C_write_Two_byte(0x70,127,0x00);
	I2C_write_Two_byte(0x70,128,0x00);
	
	I2C_write_One_byte(0x70,129);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	I2C_write_Two_byte(0x70,129,rd_byte);//I2C_write_Two_byte(0x70,129,0x00);
	
	I2C_write_One_byte(0x70,130);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	I2C_write_Two_byte(0x70,130,rd_byte);//I2C_write_Two_byte(0x70,130,0x00);
	I2C_write_Two_byte(0x70,131,0x00);
	I2C_write_Two_byte(0x70,132,0x00);
	I2C_write_Two_byte(0x70,133,0x00);
	I2C_write_Two_byte(0x70,134,0x00);
	I2C_write_Two_byte(0x70,135,0x00);
	I2C_write_Two_byte(0x70,136,0x00);
	I2C_write_Two_byte(0x70,137,0x00);
	I2C_write_Two_byte(0x70,138,0x00);
	I2C_write_Two_byte(0x70,139,0x00);
	I2C_write_Two_byte(0x70,140,0x00);
	I2C_write_Two_byte(0x70,141,0x00);
	I2C_write_Two_byte(0x70,142,0x00);
	I2C_write_Two_byte(0x70,143,0x00);
	I2C_write_Two_byte(0x70,144,0x00);
	//I2C_write_Two_byte(0x70,145,0x00);
	//I2C_write_Two_byte(0x70,146,0xFF);
	//I2C_write_Two_byte(0x70,147,0x00);
	//I2C_write_Two_byte(0x70,148,0x00);
	//I2C_write_Two_byte(0x70,149,0x00);
	//I2C_write_Two_byte(0x70,150,0x00);
	//I2C_write_Two_byte(0x70,151,0x00);
	I2C_write_Two_byte(0x70,152,0x00);
	I2C_write_Two_byte(0x70,153,0x00);
	I2C_write_Two_byte(0x70,154,0x00);
	I2C_write_Two_byte(0x70,155,0x00);
	I2C_write_Two_byte(0x70,156,0x00);
	I2C_write_Two_byte(0x70,157,0x00);
	
	I2C_write_One_byte(0x70,158);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	I2C_write_Two_byte(0x70,158,rd_byte);//I2C_write_Two_byte(0x70,158,0x00);
	
	I2C_write_One_byte(0x70,159);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	I2C_write_Two_byte(0x70,159,rd_byte);//I2C_write_Two_byte(0x70,159,0x00);
	I2C_write_Two_byte(0x70,160,0x00);
	I2C_write_Two_byte(0x70,161,0x00);
	I2C_write_Two_byte(0x70,162,0x00);
	I2C_write_Two_byte(0x70,163,0x00);
	I2C_write_Two_byte(0x70,164,0x00);
	I2C_write_Two_byte(0x70,165,0x00);
	I2C_write_Two_byte(0x70,166,0x00);
	I2C_write_Two_byte(0x70,167,0x00);
	I2C_write_Two_byte(0x70,168,0x00);
	I2C_write_Two_byte(0x70,169,0x00);
	I2C_write_Two_byte(0x70,170,0x00);
	I2C_write_Two_byte(0x70,171,0x00);
	I2C_write_Two_byte(0x70,172,0x00);
	I2C_write_Two_byte(0x70,173,0x00);
	I2C_write_Two_byte(0x70,174,0x00);
	I2C_write_Two_byte(0x70,175,0x00);
	I2C_write_Two_byte(0x70,176,0x00);
	I2C_write_Two_byte(0x70,177,0x00);
	I2C_write_Two_byte(0x70,178,0x00);
	I2C_write_Two_byte(0x70,179,0x00);
	I2C_write_Two_byte(0x70,180,0x00);
	
	I2C_write_One_byte(0x70,181);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	I2C_write_Two_byte(0x70,181,rd_byte);//I2C_write_Two_byte(0x70,181,0x00);
	I2C_write_Two_byte(0x70,182,0x00);
	I2C_write_Two_byte(0x70,183,0x00);
	I2C_write_Two_byte(0x70,184,0x00);
	I2C_write_Two_byte(0x70,185,0x00);
	I2C_write_Two_byte(0x70,186,0x00);
	I2C_write_Two_byte(0x70,187,0x00);
	I2C_write_Two_byte(0x70,188,0x00);
	I2C_write_Two_byte(0x70,189,0x00);
	I2C_write_Two_byte(0x70,190,0x00);
	I2C_write_Two_byte(0x70,191,0x00);
	I2C_write_Two_byte(0x70,192,0x00);
	I2C_write_Two_byte(0x70,193,0x00);
	I2C_write_Two_byte(0x70,194,0x00);
	I2C_write_Two_byte(0x70,195,0x00);
	I2C_write_Two_byte(0x70,196,0x00);
	I2C_write_Two_byte(0x70,197,0x00);
	I2C_write_Two_byte(0x70,198,0x00);
	I2C_write_Two_byte(0x70,199,0x00);
	I2C_write_Two_byte(0x70,200,0x00);
	I2C_write_Two_byte(0x70,201,0x00);
	I2C_write_Two_byte(0x70,202,0x00);
	
	I2C_write_One_byte(0x70,203);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	I2C_write_Two_byte(0x70,203,rd_byte);//I2C_write_Two_byte(0x70,203,0x00);
	I2C_write_Two_byte(0x70,204,0x00);
	I2C_write_Two_byte(0x70,205,0x00);
	I2C_write_Two_byte(0x70,206,0x00);
	I2C_write_Two_byte(0x70,207,0x00);
	I2C_write_Two_byte(0x70,208,0x00);
	I2C_write_Two_byte(0x70,209,0x00);
	I2C_write_Two_byte(0x70,210,0x00);
	I2C_write_Two_byte(0x70,211,0x00);
	I2C_write_Two_byte(0x70,212,0x00);
	I2C_write_Two_byte(0x70,213,0x00);
	I2C_write_Two_byte(0x70,214,0x00);
	I2C_write_Two_byte(0x70,215,0x00);
	I2C_write_Two_byte(0x70,216,0x00);
	I2C_write_Two_byte(0x70,217,0x00);
	//I2C_write_Two_byte(0x70,218,0x00);
	//I2C_write_Two_byte(0x70,219,0x00);
	//I2C_write_Two_byte(0x70,220,0x00);
	//I2C_write_Two_byte(0x70,221,0x0D);
	//I2C_write_Two_byte(0x70,222,0x00);
	//I2C_write_Two_byte(0x70,223,0x00);
	//I2C_write_Two_byte(0x70,224,0xF4);
	//I2C_write_Two_byte(0x70,225,0xF0);
	
	I2C_write_One_byte(0x70,226);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<2);
	I2C_write_Two_byte(0x70,226,rd_byte);//I2C_write_Two_byte(0x70,226,0x00);
	//I2C_write_Two_byte(0x70,227,0x00);
	//I2C_write_Two_byte(0x70,228,0x00);
	//I2C_write_Two_byte(0x70,229,0x00);
	//I2C_write_Two_byte(0x70,231,0x00);
	//I2C_write_Two_byte(0x70,232,0x00);
	//I2C_write_Two_byte(0x70,233,0x00);
	//I2C_write_Two_byte(0x70,234,0x00);
	//I2C_write_Two_byte(0x70,235,0x00);
	//I2C_write_Two_byte(0x70,236,0x00);
	//I2C_write_Two_byte(0x70,237,0x00);
	//I2C_write_Two_byte(0x70,238,0x14);
	//I2C_write_Two_byte(0x70,239,0x00);
	//I2C_write_Two_byte(0x70,240,0x00);
	
	I2C_write_One_byte(0x70,242);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<1);
	I2C_write_Two_byte(0x70,242,rd_byte);//I2C_write_Two_byte(0x70,242,0x00);
	//I2C_write_Two_byte(0x70,243,0xF0);
	//I2C_write_Two_byte(0x70,244,0x00);
	//I2C_write_Two_byte(0x70,245,0x00);
	//I2C_write_Two_byte(0x70,247,0x00);
	//I2C_write_Two_byte(0x70,248,0x00);
	//I2C_write_Two_byte(0x70,249,0xA8);
	//I2C_write_Two_byte(0x70,250,0x00);
	//I2C_write_Two_byte(0x70,251,0x84);
	//I2C_write_Two_byte(0x70,252,0x00);
	//I2C_write_Two_byte(0x70,253,0x00);
	//I2C_write_Two_byte(0x70,254,0x00);
	I2C_write_Two_byte(0x70,255,0x00);

	_delay_ms(1);

	while (go_on == 0)
	{
		I2C_write_One_byte(0x70,218);
		_delay_ms(0.2);
		I2C_read_One_byte(0x70,&rd_byte);
		if (rd_byte == 0x08)
		go_on = 1;
		else
		go_on = 0;
		_delay_ms(1);
	}

	go_on = 0;
	
	I2C_write_One_byte(0x70,49);
	_delay_ms(0.2);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<7);
	I2C_write_Two_byte(0x70,49,rd_byte);
	
	_delay_ms(1);

	I2C_write_Two_byte(0x70,246,0x02);
	
	_delay_ms(25);

	I2C_write_Two_byte(0x70,241,0x65);
	
	_delay_ms(1);

	while (go_on == 0)
	{
		I2C_write_One_byte(0x70,218);
		_delay_ms(0.2);
		I2C_read_One_byte(0x70,&rd_byte);
		if (rd_byte == 0x08)
		go_on = 1;
		else
		go_on = 0;
		_delay_ms(1);
	}

	I2C_write_One_byte(0x70,237);
	_delay_ms(0.2);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	rd_byte &= ~(1<<5);
	rd_byte &= ~(1<<6);
	rd_byte &= ~(1<<7);
	rd_byte |= 0x14;
	I2C_write_Two_byte(0x70,47,rd_byte);
	
	_delay_ms(0.3);
	
	I2C_write_One_byte(0x70,236);
	_delay_ms(0.2);
	I2C_read_One_byte(0x70,&rd_byte);
	I2C_write_Two_byte(0x70,46,rd_byte);
	
	_delay_ms(0.3);
	
	I2C_write_One_byte(0x70,235);
	_delay_ms(0.2);
	I2C_read_One_byte(0x70,&rd_byte);
	I2C_write_Two_byte(0x70,45,rd_byte);
	
	_delay_ms(0.3);

	I2C_write_One_byte(0x70,49);
	_delay_ms(0.2);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte |= (1<<7);
	I2C_write_Two_byte(0x70,49,rd_byte);
	
	_delay_ms(1);

	I2C_write_Two_byte(0x70,230,0x00);
	
	
	//I2C_write_Two_byte(0x71,230,0x10);
	//_delay_ms(1);
	//I2C_write_One_byte(0x71,230);
	//_delay_ms(0.2);
	//I2C_read_One_byte(0x71,&rd_byte);
	//_delay_ms(1);
	//I2C_write_Two_byte(0x71,230,rd_byte);
	
	//if(rd_byte==0)
	//PORTB &= ~(1<<5);
	//else
	//PORTB |= (1<<5);
	
	//SFIOR |= (1<<PUD);
	//
	//DDRD &= ~(1<<SDA_PLL);
	//DDRD &= ~(1<<SCL_PLL);
	
	//return(0);
}

void PLL_config_LHC_40()  // I2C ADDRESS = 0x70
{
	
	int go_on = 0;
	

	//DE: why is it needed to actively set these pins low???? Being configured as inputs - the PS0 and PS1 pins are naturally low when the board is inserted in the backplane.
	PORTE &= ~(1<<PS0_PLL); // DE: first the value must be overwritten 
	PORTE &= ~(1<<PS1_PLL);	
	DDRE |= (1<<PS0_PLL);	// and then - the direction can be changed to output. Otherwise an unwanted pin state can arise for a moment (before the PORTE is written)
	DDRE |= (1<<PS1_PLL);

	
	SFIOR |= (1<<PUD);	// DE: pull-ups disabled for the whole chip
	
	DDRD |= (1<<SDA_PLL);
	DDRD |= (1<<SCL_PLL);
	PORTD |= (1<<SCL_PLL);
	PORTD |= (1<<SDA_PLL);
	
	_delay_ms(1000);
	
	I2C_write_One_byte(0x74,0x04); //DE: the I2C mux PCA9548APW is configured so that only SDA2 and SCA2 signals remain connected to the I2C bus
	
	_delay_ms(10);
	
	I2C_write_Two_byte(0x70,230,0x10); // DE: all clock outputs of the chip Si5338A are disabled
	//
	_delay_ms(1);
	
	I2C_write_Two_byte(0x70,241,0xE5); //DE: PLL_LOL status in register 218 of the SiTime chip is prevented from asserting.
	//
	_delay_ms(1);
	//
	I2C_write_One_byte(0x70,6);		//DE: Prepare read from the Register 6 of the Si5338A
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte); 
	rd_byte |= (1<<3);	
	I2C_write_Two_byte(0x70,6,rd_byte); //I2C_write_Two_byte(0x71,6,0x08); //DE: the Si5338A is configured to not generate an interrupt if feedback is lost.
	
	I2C_write_Two_byte(0x70,28,0x03);	//DE: highest possible signal frequency is selected (26-30MHZ)
	I2C_write_Two_byte(0x70,29,0x00);	//DE: P1DIV_IN selected, Clock from IN2 selected (i.e. FCLK, coming from the backplane); P1 divider: divide by 1
	I2C_write_Two_byte(0x70,30,0xB0);	//DE: [7:5]= 101: No Clock selected for the PFD feedback input, {Register28[5]=0 & [4:3]=10} = 010 - Reserved value?! [2:0] = 000 Divide feedback clock by 1.
	I2C_write_Two_byte(0x70,31,0xC0);	//DE: [7:5]= 110: MultiSynth0 is selected as input for R0 divider, [4:2] = 000 - R0 output divider - divide by 1; [1]=0 MS0 MultiSynth powered up; [0] = 0 R0 output divider and CLK0 driver powered up
	I2C_write_Two_byte(0x70,32,0xC0);	//DE: same configuration for the output 1 as the config for the output 0.
	I2C_write_Two_byte(0x70,33,0xE3);	//DE: [7:5]= 111: No clock is selected as input for R2 divider, [4:2] = 00 - R0 output divider - divide by 1; [1]=1, [0]=1 MS2 MultiSynth and R2 output divider and CLK2 are all off
	I2C_write_Two_byte(0x70,34,0xE3);   //DE: The CLK3 output is also switched off as CLK2 above
	I2C_write_Two_byte(0x70,35,0x09);	//DE: [7:6] = CLK3 output driver voltage is 3v3; [5:4] = CLK2 output driver voltage is 3v3; [3:2] = CLK1 output driver voltage is 1v8; [1:0] = CLK0 output driver voltage is 2v5
	
	I2C_write_One_byte(0x70,36);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte |= (1<<1);
	rd_byte |= (1<<2);
	I2C_write_Two_byte(0x70,36,rd_byte);//I2C_write_Two_byte(0x71,36,0x06); //DE: CLK0 signal format is either LVDS or HCSL (depending on what was in the LSB) Question: why not just write 0x06 to be sure it is LVDS?
	
	I2C_write_One_byte(0x70,37);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte |= (1<<1);
	rd_byte |= (1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	I2C_write_Two_byte(0x70,37,rd_byte);//I2C_write_Two_byte(0x71,37,0x00); //DE: CLK1 signal format is LVDS. Question: why then CLK0 output is configured in a different way?
	
	I2C_write_One_byte(0x70,38);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	I2C_write_Two_byte(0x70,38,rd_byte);//I2C_write_Two_byte(0x71,38,0x00); //DE: CLK2 signal format is in the "reserved" mode. 
	
	I2C_write_One_byte(0x70,39);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	I2C_write_Two_byte(0x70,39,rd_byte);//I2C_write_Two_byte(0x71,39,0x00); //DE: CLK3 signal format is in the "reserved" mode. 
	I2C_write_Two_byte(0x70,40,0x84); //DE: the value for the TRIM bits, which should be written to regs 40, 41, 42 are calculated automatically in the ClockBuilder Pro
	
	I2C_write_One_byte(0x70,41);		//DE: the value for the TRIM bits, which should be written to regs 40, 41, 42 are calculated automatically in the ClockBuilder Pro
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte |= (1<<2);
	rd_byte |= (1<<3);
	I2C_write_Two_byte(0x70,41,rd_byte);//I2C_write_Two_byte(0x71,41,0x0C);
	
	I2C_write_One_byte(0x70,42);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte |= (1<<0);
	rd_byte |= (1<<1);
	rd_byte |= (1<<5);
	I2C_write_Two_byte(0x70,42,rd_byte);//I2C_write_Two_byte(0x71,42,0x23);

	I2C_write_Two_byte(0x70,45,0x00); //DE: no frequency calibration for VCO?
	I2C_write_Two_byte(0x70,46,0x00); //DE: no frequency calibration for VCO?
	I2C_write_Two_byte(0x70,47,0x14); //DE: Must write 000101b to these bits if the device is not factory programmed (isso)
	I2C_write_Two_byte(0x70,48,0x1D); //DE: [7] = 0 - intern feedback for the PFD; [6:0] =  Charge pump current, value comes from Clock Builder Pro
	I2C_write_Two_byte(0x70,49,0x10); //DE: [7] = 0 - do not use FCAL value in registers 45,46,47; [6:4]= 001 - VCO Gain=1; [3:0]=0000 Values for loop filter (come from clock builder pro) 
	I2C_write_Two_byte(0x70,50,0xC5); //DE: [7:6] = 11 PLL is enabled; [5:0] - MultiSynth Calibration Value - comes from clockbuilder Pro
	I2C_write_Two_byte(0x70,51,0x07); //DE: [7:4]= 0000 - MultiSynth[1-4] implement fractional divide ratios between 8 and 568, [2:0] - must be b111
	
	I2C_write_One_byte(0x70,52);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte |= (1<<4); //DE: MultiSynth0 Frequency Increment/Decrement is disabled
	I2C_write_Two_byte(0x70,52,rd_byte);//I2C_write_Two_byte(0x71,52,0x10); 
	I2C_write_Two_byte(0x70,53,0x00); // DE: Regs 53-61 are the parameters for calculating the divider value 
	I2C_write_Two_byte(0x70,54,0x08); // of the MultiSynth0. 
	I2C_write_Two_byte(0x70,55,0x00); // This value can be calculated automatically with Clockbuilder Pro.
	I2C_write_Two_byte(0x70,56,0x00); // The manual calculation procedure for these parameters is given in 
	I2C_write_Two_byte(0x70,57,0x00); // SiLabs application note AN619 (Rev 0.8), page 3.
	I2C_write_Two_byte(0x70,58,0x00); //
	I2C_write_Two_byte(0x70,59,0x01); //
	I2C_write_Two_byte(0x70,60,0x00); //
	I2C_write_Two_byte(0x70,61,0x00); //
	
	I2C_write_One_byte(0x70,62);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);		 
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	rd_byte &= ~(1<<5);
	I2C_write_Two_byte(0x70,62,rd_byte);//I2C_write_Two_byte(0x71,62,0x00);
	
	I2C_write_One_byte(0x70,63);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte |= (1<<4);
	I2C_write_Two_byte(0x70,63,rd_byte);//I2C_write_Two_byte(0x71,63,0x10);
	I2C_write_Two_byte(0x70,64,0x00);
	I2C_write_Two_byte(0x70,65,0x1C);
	I2C_write_Two_byte(0x70,66,0x00);
	I2C_write_Two_byte(0x70,67,0x00);
	I2C_write_Two_byte(0x70,68,0x00);
	I2C_write_Two_byte(0x70,69,0x00);
	I2C_write_Two_byte(0x70,70,0x01);
	I2C_write_Two_byte(0x70,71,0x00);
	I2C_write_Two_byte(0x70,72,0x00);
	
	I2C_write_One_byte(0x70,73);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	rd_byte &= ~(1<<5);
	I2C_write_Two_byte(0x70,73,rd_byte);//I2C_write_Two_byte(0x71,73,0x00);
	
	I2C_write_One_byte(0x70,74);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte |= (1<<4);
	I2C_write_Two_byte(0x70,74,rd_byte);//I2C_write_Two_byte(0x71,74,0x10);
	I2C_write_Two_byte(0x70,75,0x00);
	I2C_write_Two_byte(0x70,76,0x00);
	I2C_write_Two_byte(0x70,77,0x00);
	I2C_write_Two_byte(0x70,78,0x00);
	I2C_write_Two_byte(0x70,79,0x00);
	I2C_write_Two_byte(0x70,80,0x00);
	I2C_write_Two_byte(0x70,81,0x00);
	I2C_write_Two_byte(0x70,82,0x00);
	I2C_write_Two_byte(0x70,83,0x00);
	
	I2C_write_One_byte(0x70,84);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	rd_byte &= ~(1<<5);
	I2C_write_Two_byte(0x70,84,rd_byte);//I2C_write_Two_byte(0x71,84,0x00);
	
	I2C_write_One_byte(0x70,85);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte |= (1<<4);
	I2C_write_Two_byte(0x70,85,rd_byte);//I2C_write_Two_byte(0x71,85,0x10);
	I2C_write_Two_byte(0x70,86,0x00);
	I2C_write_Two_byte(0x70,87,0x00);
	I2C_write_Two_byte(0x70,88,0x00);
	I2C_write_Two_byte(0x70,89,0x00);
	I2C_write_Two_byte(0x70,90,0x00);
	I2C_write_Two_byte(0x70,91,0x00);
	I2C_write_Two_byte(0x70,92,0x00);
	I2C_write_Two_byte(0x70,93,0x00);
	I2C_write_Two_byte(0x70,94,0x00);
	
	I2C_write_One_byte(0x70,95);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	rd_byte &= ~(1<<5);
	I2C_write_Two_byte(0x70,95,rd_byte);//I2C_write_Two_byte(0x71,95,0x00);
	//I2C_write_Two_byte(0x71,96,0x10);
	I2C_write_Two_byte(0x70,97,0x00);
	I2C_write_Two_byte(0x70,98,0x1C);
	I2C_write_Two_byte(0x70,99,0x00);
	I2C_write_Two_byte(0x70,100,0x00);
	I2C_write_Two_byte(0x70,101,0x00);
	I2C_write_Two_byte(0x70,102,0x00);
	I2C_write_Two_byte(0x70,103,0x01);
	I2C_write_Two_byte(0x70,104,0x00);
	I2C_write_Two_byte(0x70,105,0x00);
	
	I2C_write_One_byte(0x70,106);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte |= (1<<7);
	I2C_write_Two_byte(0x70,106,rd_byte);//I2C_write_Two_byte(0x70,106,0x80);
	I2C_write_Two_byte(0x70,107,0x00);
	
	I2C_write_One_byte(0x70,108);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	rd_byte &= ~(1<<5);
	rd_byte &= ~(1<<6);
	I2C_write_Two_byte(0x70,108,rd_byte);//I2C_write_Two_byte(0x70,108,0x00);
	I2C_write_Two_byte(0x70,109,0x00);
	I2C_write_Two_byte(0x70,110,0x40);
	I2C_write_Two_byte(0x70,111,0x00);
	
	I2C_write_One_byte(0x70,112);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	rd_byte &= ~(1<<5);
	rd_byte &= ~(1<<6);
	I2C_write_Two_byte(0x70,112,rd_byte);//I2C_write_Two_byte(0x70,112,0x00);
	I2C_write_Two_byte(0x70,113,0x00);
	I2C_write_Two_byte(0x70,114,0x40);
	I2C_write_Two_byte(0x70,115,0x00);
	I2C_write_Two_byte(0x70,116,0x80);
	I2C_write_Two_byte(0x70,117,0x00);
	I2C_write_Two_byte(0x70,118,0x40);
	I2C_write_Two_byte(0x70,119,0x00);
	I2C_write_Two_byte(0x70,120,0x00);
	I2C_write_Two_byte(0x70,121,0x00);
	I2C_write_Two_byte(0x70,122,0x40);
	I2C_write_Two_byte(0x70,123,0x00);
	I2C_write_Two_byte(0x70,124,0x00);
	I2C_write_Two_byte(0x70,125,0x00);
	I2C_write_Two_byte(0x70,126,0x00);
	I2C_write_Two_byte(0x70,127,0x00);
	I2C_write_Two_byte(0x70,128,0x00);
	
	I2C_write_One_byte(0x70,129);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	I2C_write_Two_byte(0x70,129,rd_byte);//I2C_write_Two_byte(0x70,129,0x00);
	
	I2C_write_One_byte(0x70,130);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	I2C_write_Two_byte(0x70,130,rd_byte);//I2C_write_Two_byte(0x70,130,0x00);
	I2C_write_Two_byte(0x70,131,0x00);
	I2C_write_Two_byte(0x70,132,0x00);
	I2C_write_Two_byte(0x70,133,0x00);
	I2C_write_Two_byte(0x70,134,0x00);
	I2C_write_Two_byte(0x70,135,0x00);
	I2C_write_Two_byte(0x70,136,0x00);
	I2C_write_Two_byte(0x70,137,0x00);
	I2C_write_Two_byte(0x70,138,0x00);
	I2C_write_Two_byte(0x70,139,0x00);
	I2C_write_Two_byte(0x70,140,0x00);
	I2C_write_Two_byte(0x70,141,0x00);
	I2C_write_Two_byte(0x70,142,0x00);
	I2C_write_Two_byte(0x70,143,0x00);
	I2C_write_Two_byte(0x70,144,0x00);
	//I2C_write_Two_byte(0x70,145,0x00);
	//I2C_write_Two_byte(0x70,146,0xFF);
	//I2C_write_Two_byte(0x70,147,0x00);
	//I2C_write_Two_byte(0x70,148,0x00);
	//I2C_write_Two_byte(0x70,149,0x00);
	//I2C_write_Two_byte(0x70,150,0x00);
	//I2C_write_Two_byte(0x70,151,0x00);
	I2C_write_Two_byte(0x70,152,0x00);
	I2C_write_Two_byte(0x70,153,0x00);
	I2C_write_Two_byte(0x70,154,0x00);
	I2C_write_Two_byte(0x70,155,0x00);
	I2C_write_Two_byte(0x70,156,0x00);
	I2C_write_Two_byte(0x70,157,0x00);
	
	I2C_write_One_byte(0x70,158);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	I2C_write_Two_byte(0x70,158,rd_byte);//I2C_write_Two_byte(0x70,158,0x00);
	
	I2C_write_One_byte(0x70,159);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	I2C_write_Two_byte(0x70,159,rd_byte);//I2C_write_Two_byte(0x70,159,0x00);
	I2C_write_Two_byte(0x70,160,0x00);
	I2C_write_Two_byte(0x70,161,0x00);
	I2C_write_Two_byte(0x70,162,0x00);
	I2C_write_Two_byte(0x70,163,0x00);
	I2C_write_Two_byte(0x70,164,0x00);
	I2C_write_Two_byte(0x70,165,0x00);
	I2C_write_Two_byte(0x70,166,0x00);
	I2C_write_Two_byte(0x70,167,0x00);
	I2C_write_Two_byte(0x70,168,0x00);
	I2C_write_Two_byte(0x70,169,0x00);
	I2C_write_Two_byte(0x70,170,0x00);
	I2C_write_Two_byte(0x70,171,0x00);
	I2C_write_Two_byte(0x70,172,0x00);
	I2C_write_Two_byte(0x70,173,0x00);
	I2C_write_Two_byte(0x70,174,0x00);
	I2C_write_Two_byte(0x70,175,0x00);
	I2C_write_Two_byte(0x70,176,0x00);
	I2C_write_Two_byte(0x70,177,0x00);
	I2C_write_Two_byte(0x70,178,0x00);
	I2C_write_Two_byte(0x70,179,0x00);
	I2C_write_Two_byte(0x70,180,0x00);
	
	I2C_write_One_byte(0x70,181);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	I2C_write_Two_byte(0x70,181,rd_byte);//I2C_write_Two_byte(0x70,181,0x00);
	I2C_write_Two_byte(0x70,182,0x00);
	I2C_write_Two_byte(0x70,183,0x00);
	I2C_write_Two_byte(0x70,184,0x00);
	I2C_write_Two_byte(0x70,185,0x00);
	I2C_write_Two_byte(0x70,186,0x00);
	I2C_write_Two_byte(0x70,187,0x00);
	I2C_write_Two_byte(0x70,188,0x00);
	I2C_write_Two_byte(0x70,189,0x00);
	I2C_write_Two_byte(0x70,190,0x00);
	I2C_write_Two_byte(0x70,191,0x00);
	I2C_write_Two_byte(0x70,192,0x00);
	I2C_write_Two_byte(0x70,193,0x00);
	I2C_write_Two_byte(0x70,194,0x00);
	I2C_write_Two_byte(0x70,195,0x00);
	I2C_write_Two_byte(0x70,196,0x00);
	I2C_write_Two_byte(0x70,197,0x00);
	I2C_write_Two_byte(0x70,198,0x00);
	I2C_write_Two_byte(0x70,199,0x00);
	I2C_write_Two_byte(0x70,200,0x00);
	I2C_write_Two_byte(0x70,201,0x00);
	I2C_write_Two_byte(0x70,202,0x00);
	
	I2C_write_One_byte(0x70,203);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<0);
	rd_byte &= ~(1<<1);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	I2C_write_Two_byte(0x70,203,rd_byte);//I2C_write_Two_byte(0x70,203,0x00);
	I2C_write_Two_byte(0x70,204,0x00);
	I2C_write_Two_byte(0x70,205,0x00);
	I2C_write_Two_byte(0x70,206,0x00);
	I2C_write_Two_byte(0x70,207,0x00);
	I2C_write_Two_byte(0x70,208,0x00);
	I2C_write_Two_byte(0x70,209,0x00);
	I2C_write_Two_byte(0x70,210,0x00);
	I2C_write_Two_byte(0x70,211,0x00);
	I2C_write_Two_byte(0x70,212,0x00);
	I2C_write_Two_byte(0x70,213,0x00);
	I2C_write_Two_byte(0x70,214,0x00);
	I2C_write_Two_byte(0x70,215,0x00);
	I2C_write_Two_byte(0x70,216,0x00);
	I2C_write_Two_byte(0x70,217,0x00);
	//I2C_write_Two_byte(0x70,218,0x00);
	//I2C_write_Two_byte(0x70,219,0x00);
	//I2C_write_Two_byte(0x70,220,0x00);
	//I2C_write_Two_byte(0x70,221,0x0D);
	//I2C_write_Two_byte(0x70,222,0x00);
	//I2C_write_Two_byte(0x70,223,0x00);
	//I2C_write_Two_byte(0x70,224,0xF4);
	//I2C_write_Two_byte(0x70,225,0xF0);
	
	//I2C_write_One_byte(0x70,226); //DE: In the Si5338 reference manual is stated: do not use read-modify-write procedure to perform soft reset.  Therefore the block is commented out by me.
	//_delay_ms(0.1);
	//I2C_read_One_byte(0x70,&rd_byte);
	//rd_byte &= ~(1<<2);
	//I2C_write_Two_byte(0x70,226,rd_byte);
	
	I2C_write_Two_byte(0x70,226,0x00); //DE: Si5338 reference manual: All Multisynth blocks will remain in reset until a 0 is written to this bit.
	
	//I2C_write_Two_byte(0x70,227,0x00);
	//I2C_write_Two_byte(0x70,228,0x00);
	//I2C_write_Two_byte(0x70,229,0x00);
	//I2C_write_Two_byte(0x70,231,0x00);
	//I2C_write_Two_byte(0x70,232,0x00);
	//I2C_write_Two_byte(0x70,233,0x00);
	//I2C_write_Two_byte(0x70,234,0x00);
	//I2C_write_Two_byte(0x70,235,0x00);
	//I2C_write_Two_byte(0x70,236,0x00);
	//I2C_write_Two_byte(0x70,237,0x00);
	//I2C_write_Two_byte(0x70,238,0x14);
	//I2C_write_Two_byte(0x70,239,0x00);
	//I2C_write_Two_byte(0x70,240,0x00);
	
	I2C_write_One_byte(0x70,242);
	_delay_ms(0.1);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<1);
	I2C_write_Two_byte(0x70,242,rd_byte);//I2C_write_Two_byte(0x70,242,0x00); //DE: I.e. the frequency inc/dec logic is not disabled?
	//I2C_write_Two_byte(0x70,243,0xF0);
	//I2C_write_Two_byte(0x70,244,0x00);
	//I2C_write_Two_byte(0x70,245,0x00);
	//I2C_write_Two_byte(0x70,247,0x00);
	//I2C_write_Two_byte(0x70,248,0x00);
	//I2C_write_Two_byte(0x70,249,0xA8);
	//I2C_write_Two_byte(0x70,250,0x00);
	//I2C_write_Two_byte(0x70,251,0x84);
	//I2C_write_Two_byte(0x70,252,0x00);
	//I2C_write_Two_byte(0x70,253,0x00);
	//I2C_write_Two_byte(0x70,254,0x00);
	I2C_write_Two_byte(0x70,255,0x00);

	_delay_ms(1);

	for (int i=0; i<10; i++)
	{
		I2C_write_One_byte(0x70,218);
		_delay_ms(0.2);
		I2C_read_One_byte(0x70,&rd_byte);
		if (rd_byte == 0x08) //DE: why the bit LOS_FDBK (Loss of Signal on Feedback Clock from IN5,6 or IN4) is being checked? Isn't it better to readout the SYS_CAL bit? (Device Calibration in Process)?
			break;
	}
	_delay_ms(1);
	
	I2C_write_One_byte(0x70,49);
	_delay_ms(0.2);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<7);
	I2C_write_Two_byte(0x70,49,rd_byte);
	
	_delay_ms(1);

	I2C_write_Two_byte(0x70,246,0x02); //DE: Soft reset the PLL (configuration registers values are not being reset)
	
	_delay_ms(25);

	I2C_write_Two_byte(0x70,241,0x65);
	
	_delay_ms(1);


	for (int i=0; i<10; i++)
	{
		I2C_write_One_byte(0x70,218);
		_delay_ms(0.2);
		I2C_read_One_byte(0x70,&rd_byte);
		if (rd_byte == 0x08) 
			break;
	}
	_delay_ms(1);
	
	I2C_write_One_byte(0x70,237);
	_delay_ms(0.2);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte &= ~(1<<2);
	rd_byte &= ~(1<<3);
	rd_byte &= ~(1<<4);
	rd_byte &= ~(1<<5);
	rd_byte &= ~(1<<6);
	rd_byte &= ~(1<<7);
	rd_byte |= 0x14;		// DE: The bits 2 and 4 are set above to 0 and then immediately set to 1. What is the reason?
	I2C_write_Two_byte(0x70,47,rd_byte);
	
	_delay_ms(0.3);
	
	I2C_write_One_byte(0x70,236);
	_delay_ms(0.2);
	I2C_read_One_byte(0x70,&rd_byte);
	I2C_write_Two_byte(0x70,46,rd_byte);
	
	_delay_ms(0.3);
	
	I2C_write_One_byte(0x70,235);
	_delay_ms(0.2);
	I2C_read_One_byte(0x70,&rd_byte);
	I2C_write_Two_byte(0x70,45,rd_byte);
	
	_delay_ms(0.3);

	I2C_write_One_byte(0x70,49);
	_delay_ms(0.2);
	I2C_read_One_byte(0x70,&rd_byte);
	rd_byte |= (1<<7);
	I2C_write_Two_byte(0x70,49,rd_byte);
	
	_delay_ms(1);

	I2C_write_Two_byte(0x70,230,0x00); //DE: all CLK outputs are enabled. Why? For example CLK2 output is just left unconnected in the schematics.
	
	
	//I2C_write_Two_byte(0x71,230,0x10);
	//_delay_ms(1);
	//I2C_write_One_byte(0x71,230);
	//_delay_ms(0.2);
	//I2C_read_One_byte(0x71,&rd_byte);
	//_delay_ms(1);
	//I2C_write_Two_byte(0x71,230,rd_byte);
	
	//if(rd_byte==0)
	//PORTB &= ~(1<<5);
	//else
	//PORTB |= (1<<5);
	
	//SFIOR |= (1<<PUD);
	//
	//DDRD &= ~(1<<SDA_PLL);
	//DDRD &= ~(1<<SCL_PLL);
	
	//return(0);
}

//void PLL_restart_120()
//{	
//
//	DDRD |= (1<<SDA_PLL);   // defines the lines SDA_PLL and SCL_PLL as output an enables their pull-ups
//	DDRD |= (1<<SCL_PLL);
//	PORTD |= (1<<SCL_PLL);
//	PORTD |= (1<<SDA_PLL);
//	
//	local_led_control(RED_LED, LED_OFF);
//  
//  _delay_ms(80);
//
//	local_led_control(RED_LED, LED_ON);
//	
//	I2C_write_One_byte(0x74,0x04);  // enables the channel #2 of the i2c switch
//	
//	_delay_ms(10);
//
//	local_led_control(RED_LED, LED_OFF);
//  
//	I2C_write_Two_byte(0x70,246,0x02);
//	//
//	
//	PLL_config_120();
//	//PLL_config_125();
//	//local_led_control(RED_LED, LED_ON);
//}
