/*
 * PLL_config.h
 *
 * Created: 12/08/2014 18:29:49
 *  Author: atriossi
 */ 

#include <avr/io.h>
#include <util/delay.h>
#include <stdio.h>

#define PS0_PLL		3
#define PS1_PLL		2

#define SCL_PLL		4
#define SDA_PLL		5

#define T_PLL		1

void PLL_config();
void PLL_config_LHC();
void PLL_config_LHC_40();
void PLL_config_125();
void PLL_config_120();
void PLL_config_320();

void PLL_restart_120();

int I2C_write_One_byte(int slaveAddress, int byte_1);
