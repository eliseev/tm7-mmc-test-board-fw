/*
 * config.h
 *
 * Created: 19/03/2015 18:20:14
 *  Author: jumendez
 * Modified: 27/11/2018
 *  By: J. Sastre
 */ 


#ifndef CONFIG_H_
#define CONFIG_H_

#include "../iodeclaration.h"
#include "../led.h"
#include "PLL_config.h"

/** USER GPIO INITIALIZATION */
#define GPIO0_DIR		INPUT		//PC2	MMC_RESET_FPGA_HR
#define GPIO1_DIR		INPUT		//PC4	INIT_B_33
#define GPIO2_DIR		INPUT		//PC0	unconnected
#define GPIO3_DIR		INPUT		//PC5	DONE
#define GPIO4_DIR		OUTPUT		//PC6	CTRL_ENABLE
#define GPIO5_DIR		OUTPUT		//PC7	DCDC_ENABLE
#define GPIO6_DIR		INPUT		//PF1	AIR_TEMP1
#define GPIO7_DIR		INPUT		//PF2	AIR_TEMP2
#define GPIO8_DIR		INPUT		//PF3	unconnected
#define GPIO9_DIR		INPUT		//PE6	USR_3_HR
#define GPIO10_DIR		OUTPUT		//PE4	USR_1
#define GPIO11_DIR		INPUT		//PE5	USR_2_HR
#define GPIO12_DIR		INPUT		//PE7	LIN_ENABLE_HR
#define GPIO13_DIR		OUTPUT		//PA3	RTM_12V_ENABLE
#define GPIO14_DIR		OUTPUT		//PA4	RTM_3V3_ENABLE
#define GPIO15_DIR		OUTPUT		//PA5	RTM_I2C_ENABLE

/** PAYLOD CONFIGURATION */
//Define power on sequence (Mandatory)
#define POWER_ON_SEQ										\
	SET_SIGNAL(LOCAL_DCDC_ENABLE)							\
	SET_SIGNAL(LOCAL_REG_ENABLE)							\
	PLL_config_LHC_40();									\
	set_address_ipmi();					
//	PLL_config_125();										\
														  
	//WAIT_FOR_HIGH(LOCAL_FPGA2_INIT_DONE, 500, PON_ERROR)

//Define power OFF sequence (Mandatory)	
#define POWER_OFF_SEQ										\
	CLEAR_SIGNAL(LOCAL_DCDC_ENABLE)							\
	CLEAR_SIGNAL(LOCAL_REG_ENABLE)							\
	DELAY(500)
	
//Define reboot sequence (Optional)
#define REBOOT_SEQ											\
	CLEAR_SIGNAL(LOCAL_DCDC_ENABLE)							\
	CLEAR_SIGNAL(LOCAL_REG_ENABLE)							\
	DELAY(500)												\
	SET_SIGNAL(LOCAL_DCDC_ENABLE)							\
	SET_SIGNAL(LOCAL_REG_ENABLE)							\
	PLL_config_125();										\									
	PLL_config_LHC_40();																			
	//WAIT_FOR_HIGH(LOCAL_FPGA2_INIT_DONE, 200, PON_CONTINUE)
	
//Define warm reset sequence (Optional)
#define WARM_RESET_SEQ										\
	CLEAR_SIGNAL(LOCAL_DCDC_ENABLE)							\
	CLEAR_SIGNAL(LOCAL_REG_ENABLE)							\
	DELAY(500)												\
	SET_SIGNAL(LOCAL_DCDC_ENABLE)							\
	SET_SIGNAL(LOCAL_REG_ENABLE)							\
	PLL_config_125();										\
	PLL_config_LHC_40();								
	//WAIT_FOR_HIGH(LOCAL_FPGA2_INIT_DONE, 200, PON_CONTINUE)

//Define cold reset sequence (Optional)	
#define COLD_RESET_SEQ										\
	CLEAR_SIGNAL(LOCAL_DCDC_ENABLE)							\
	CLEAR_SIGNAL(LOCAL_REG_ENABLE)							\
	DELAY(500)												\
	SET_SIGNAL(LOCAL_DCDC_ENABLE)							\
	SET_SIGNAL(LOCAL_REG_ENABLE)							\
	PLL_config_125();										\
	PLL_config_LHC_40();
	//WAIT_FOR_HIGH(LOCAL_FPGA2_INIT_DONE, 200, PON_CONTINUE)


/** LED CONFIGURATION */
#define AMC_USER_LED_CNT	0
#define AMC_USER_LED_LIST									\
	{														\
		{	/* LED ID 3*/									\
			io_type: MMC_PORT,								\
			port:MMCPORT_E,									\
			pin:PE6,										\
			color: RED,										\
			init: INACTIVE,									\
			active: LOW,									\
			inactive: HIGH									\
		},													\
		{	/* LED ID 4*/									\
			io_type: MMC_PORT,								\
			port:MMCPORT_E,									\
			pin:PE4,										\
			color: RED,										\
			init: INACTIVE,									\
			active: LOW,									\
			inactive: HIGH									\
		},													\
		{	/* LED ID 5*/									\
			io_type: MMC_PORT,								\
			port:MMCPORT_E,									\
			pin:PE5,										\
			color: RED,										\
			init: INACTIVE,									\
			active: LOW,									\
			inactive: HIGH									\
		},													\
		{	/* LED ID 6*/									\
			io_type: MMC_PORT,								\
			port:MMCPORT_F,									\
			pin:PF1,										\
			color: RED,										\
			init: INACTIVE,									\
			active: LOW,									\
			inactive: HIGH									\
		},													\
		{	/* LED ID 7*/									\
			io_type: MMC_PORT,								\
			port:MMCPORT_F,									\
			pin:PF2,										\
			color: RED,										\
			init: INACTIVE,									\
			active: LOW,									\
			inactive: HIGH									\
		},													\
		{	/* LED ID 8*/									\
			io_type: MMC_PORT,								\
			port:MMCPORT_F,									\
			pin:PF3,										\
			color: RED,										\
			init: INACTIVE,									\
			active: LOW,									\
			inactive: HIGH									\
		},													\
		{	/* LED ID 9*/									\
			io_type: MMC_PORT,								\
			port:MMCPORT_E,									\
			pin:PE7,										\
			color: RED,										\
			init: INACTIVE,									\
			active: LOW,									\
			inactive: HIGH									\
		}													\
	}

#endif /* CONFIG_H_ */