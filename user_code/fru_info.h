#ifndef FRU_INFO_H
#define FRU_INFO_H

#include "../fru.h"
#include "../languages.h"

#define USE_FRU_INFO_ENABLE

/*********************************************
 * Common defines
 *********************************************/
#define LANG_CODE		ENGLISH
#define FRU_FILE_ID		"CoreFRU"	//Allows knowing the source of the FRU present in the memory

#define BOARD_INFO_AREA_ENABLE
#define PRODUCT_INFO_AREA_ENABLE
#define MULTIRECORD_AREA_ENABLE

/*********************************************
 * Board information area
 *********************************************/
#define BOARD_MANUFACTURER		"CERN"
#define	BOARD_NAME				"TwinMux"
#define BOARD_SN				"000000001"
#define BOARD_PN				"TM7"
 
/*********************************************
 * Product information area
 *********************************************/
#define PRODUCT_MANUFACTURER	"CERN"
#define PRODUCT_NAME			"TwinMux"
#define PRODUCT_PN				"00001"
#define PRODUCT_VERSION			"v2"
#define PRODUCT_SN				"00001"
#define PRODUCT_ASSET_TAG		"No tag"
 
/*********************************************
 * AMC: Point to point connectivity record
 *********************************************/
#define LIGHT_AMC_POINT_TO_POINT_RECORD_CNT		1
#define LIGHT_AMC_POINT_TO_POINT_RECORD_LIST															\
	LIGHT_AMC_MODULE_POINT_TO_POINT_RECORD(0, PORT(4), PCIE, NO_EXTENSION, EXACT_MATCHES)
	
/*********************************************
 * AMC: Point to point clock record
 *********************************************/
#define AMC_POINT_TO_POINT_CLOCK_CNT			1
#define AMC_POINT_TO_POINT_CLOCK_LIST																	\
	DIRECT_CLOCK_CONNECTION(0, 2, 0, 1, 0, 0, 0, 40000000, 38000000, 42000000)

/**********************************************
 * PICMG: Module current record
 **********************************************/
#define MODULE_CURRENT_RECORD		 current_in_ma(8000)	//	current_in_ma(max_current_in_mA)
	
#endif