//*****************************************************************************
// Copyright (C) 2007 DESY(Deutsches-Elektronen Synchrotron) 
//
// File Name	: led.c
// 
// Title		: LED control file
// Revision		: 1.1
// Notes		:	
// Target MCU	: Atmel AVR series
//
// Author       : Vahan Petrosyan (vahan_petrosyan@desy.de)
// Modified by  : Frederic Bompard CPPM ( Centre Physique des Particules de Marseille )
// Modified by  : Markus Joos (markus.joos@cern.ch)
// Modified by  : Julian Mendez (julian.mendez@cern.ch)
//
// Description : Control LEDs on front panel
//					
//
// This code is distributed under the GNU Public License
//		which can be found at http://www.gnu.org/licenses/gpl.txt
//*****************************************************************************

// Header file
#include <avr/io.h>
#include "avrlibdefs.h"
#include "avrlibtypes.h"
#include "rtm_mng.h"
#include "led.h"
#include "user_code_select.h"
#include "user_code/config.h"
#include "timer.h"
#include "i2csw.h"

#ifdef LED_DEFINITION_ENABLE
	#undef NUM_OF_AMC_LED
	#define NUM_OF_AMC_LED	3+AMC_USER_LED_CNT
#endif

//Globals
leds amc_led[NUM_OF_AMC_LED];

#ifdef USE_RTM
	leds rtm_led[NUM_OF_RTM_LED];
#endif

//*************/
void leds_init()    //Called from mmc_main.c
//*************/
{
	#ifdef LED_DEFINITION_ENABLE
		u08 i;
		LED_t led[AMC_USER_LED_CNT] = AMC_USER_LED_LIST;
	#endif
	
    //AMC LEDs
    amc_led[BLUE_LED].local_cntr_fnc  = LED_ON;
    amc_led[BLUE_LED].fnc_off         = LED_ON;
    amc_led[BLUE_LED].on_duration     = 0;
    amc_led[BLUE_LED].color           = BLUE;
    amc_led[BLUE_LED].control_state   = LOCAL_CONTROL_STATE;
    local_led_control(BLUE_LED, LED_ON);

    amc_led[RED_LED].local_cntr_fnc   = LED_OFF;
    amc_led[RED_LED].fnc_off          = LED_OFF;
    amc_led[RED_LED].on_duration      = 0;
    amc_led[RED_LED].color            = RED;
    amc_led[RED_LED].control_state    = LOCAL_CONTROL_STATE;
    local_led_control(RED_LED, LED_OFF);

    amc_led[GREEN_LED].local_cntr_fnc = LED_OFF;
    amc_led[GREEN_LED].fnc_off        = LED_OFF;
    amc_led[GREEN_LED].on_duration    = 0;
    amc_led[GREEN_LED].color          = GREEN;
    amc_led[GREEN_LED].control_state  = LOCAL_CONTROL_STATE;
    local_led_control(GREEN_LED, LED_OFF);
	
    //RTM LEDs
#ifdef USE_RTM
    rtm_led[BLUE_LED].local_cntr_fnc  = LED_ON;
    rtm_led[BLUE_LED].fnc_off         = LED_ON;
    rtm_led[BLUE_LED].on_duration     = 0;
    rtm_led[BLUE_LED].color           = BLUE;
    rtm_led[BLUE_LED].control_state   = LOCAL_CONTROL_STATE;

    rtm_led[RED_LED].local_cntr_fnc   = LED_OFF;
    rtm_led[RED_LED].fnc_off          = LED_OFF;
    rtm_led[RED_LED].on_duration      = 0;
    rtm_led[RED_LED].color            = RED;
    rtm_led[RED_LED].control_state    = LOCAL_CONTROL_STATE;

    rtm_led[GREEN_LED].local_cntr_fnc = LED_OFF;
    rtm_led[GREEN_LED].fnc_off        = LED_OFF;
    rtm_led[GREEN_LED].on_duration    = 0;
    rtm_led[GREEN_LED].color          = GREEN;
    rtm_led[GREEN_LED].control_state  = LOCAL_CONTROL_STATE;
#endif

	#ifdef LED_DEFINITION_ENABLE
		for(i=0; i < AMC_USER_LED_CNT; i++){
			amc_led[3+i].local_cntr_fnc  = led[i].init;
			amc_led[3+i].fnc_off         = led[i].init;
			amc_led[3+i].on_duration     = 0;
			amc_led[3+i].color           = led[i].color;
			amc_led[3+i].control_state   = LOCAL_CONTROL_STATE;
			local_led_control(3+i, led[i].init);
		}
	#else
		if (USER_CODE == 2)		leds_init_user();
	#endif
	
	ledTimerAttach(ledTimerCallback);
}


//*********************/
u08 state_led(u08 led_n)   //Called from mmc_main.c
//*********************/
{
	#ifdef LED_DEFINITION_ENABLE
		u08 data;
		LED_t led[AMC_USER_LED_CNT] = AMC_USER_LED_LIST;
	#endif
	
    //if (amc_led[led_n].control_state == OVERRIDE_STATE)
    //    return (0); //MJ: 0 = LED_OFF. Is this what we want in mmc_main.c? What is OVERRIDE_STATE used for?	This function should be used for blinking function (also in overide state)

    switch (led_n)
    {
		case BLUE_LED:
			if (inb(LOCAL_BLUE_LED_PPIN) & BV(LOCAL_BLUE_LED_PIN))		return LED_OFF;
			else														return LED_ON;
			break;
		case RED_LED:
			if (inb(LOCAL_RED_LED_PPIN) & BV(LOCAL_RED_LED_PIN))		return LED_OFF;
			else														return LED_ON;
			break;
		case GREEN_LED:
			if (inb(LOCAL_GREEN_LED_PPIN) & BV(LOCAL_GREEN_LED_PIN))	return LED_OFF;
			else														return LED_ON;
			break;
		default:
			#ifdef LED_DEFINITION_ENABLE
				switch(led[led_n-3].io_type){
					case MMC_PORT:
						if(get_mmcport_led_state(led_n-3) == led[led_n-3].active)	return LED_ON;
						else														return LED_OFF;
						break;
					
					case IO_EXTENDER_PCF8574AT:
						data = get_PCF8574AT_port(led[led_n-3].addr);
						data &= (0x00 | (0x01 << led[led_n-3].pin));
						
						if((data?HIGH:LOW) == led[led_n-3].active)					return LED_ON;
						else														return LED_OFF;
						break;
				}
			#else
				if (USER_CODE == 2)		return(state_led_user(led_n));
			#endif
			
			break;
    }
    return (0xff); //MJ: mmc_main.c does not process this error. How could it react?
}


//*********************************************/
void local_led_control(u08 led_n, u08 led_state)   //Called from led.c only - WARNING (JM) : local_led_control does not support custom blink but only LED_LONG_BLINK and LED_SHORT_BLINK
//*********************************************/
{
	#ifdef LED_DEFINITION_ENABLE
		u08 data;
		LED_t led[AMC_USER_LED_CNT] = AMC_USER_LED_LIST;
	#endif
	
    if (amc_led[led_n].control_state == OVERRIDE_STATE || amc_led[led_n].control_state == LAMP_TEST_STATE)
        return;

	if (led_state == LED_LONG_BLINK){
		amc_led[led_n].local_on_duration = LED_LONG_BLINK;
		amc_led[led_n].local_cntr_fnc = LED_LONG_BLINK;
	}else if (led_state == LED_SHORT_BLINK){
		amc_led[led_n].local_on_duration = LED_SHORT_BLINK;
		amc_led[led_n].local_cntr_fnc = LED_SHORT_BLINK;
	}
	
    switch (led_n)
    {
	    case BLUE_LED:
			if (led_state == LED_ON)						  cbi(LOCAL_BLUE_LED_PPORT, LOCAL_BLUE_LED_PIN);	    
			else if(led_state == LED_OFF)                     sbi(LOCAL_BLUE_LED_PPORT, LOCAL_BLUE_LED_PIN);	  
			  
			break;
	    
	    case RED_LED:	   
			if (led_state == LED_ON)						  cbi(LOCAL_RED_LED_PPORT, LOCAL_RED_LED_PIN);
	    	else if(led_state == LED_OFF)                     sbi(LOCAL_RED_LED_PPORT, LOCAL_RED_LED_PIN);	    
		   
			break;
	    
	    case GREEN_LED:
			if (led_state == LED_ON)						  cbi(LOCAL_GREEN_LED_PPORT, LOCAL_GREEN_LED_PIN);	    
			else if(led_state == LED_OFF)                     sbi(LOCAL_GREEN_LED_PPORT, LOCAL_GREEN_LED_PIN);
	    
			break;
	    
	    default:
			#ifdef LED_DEFINITION_ENABLE
				switch(led[led_n-3].io_type){
					case MMC_PORT:
						if(led_state == LED_ON)			active_mmcport_led(led_n-3);
						else if(led_state == LED_OFF)	desactive_mmcport_led(led_n-3);
						break;
						
					case IO_EXTENDER_PCF8574AT:		
						data = get_PCF8574AT_port(led[led_n-3].addr);
										
						if(led_state == LED_ON){
							if(led[led_n-3].active)			data |= (0x01 < (led[led_n-3].pin));
							else							data &= ~(0x01 < (led[led_n-3].pin));
						}
						else if(led_state == LED_OFF){
							if(led[led_n-3].inactive)		data |= (0x01 < (led[led_n-3].pin));
							else							data &= ~(0x01 < (led[led_n-3].pin));
						}
						
						set_PCF8574AT_port(led[led_n-3].addr, data);
						
						break;
				}
			
			#else
				if (USER_CODE == 2)				local_led_control_user(led_n, led_state);
			#endif
			
			break;
    }

    amc_led[led_n].local_cntr_fnc = led_state;
    amc_led[led_n].state = led_state;
}


//***************************************/
void led_control(u08 led_n, u08 led_state)  //Called from led.c only
//***************************************/
{
	#ifdef LED_DEFINITION_ENABLE
		u08 data;
		LED_t led[AMC_USER_LED_CNT] = AMC_USER_LED_LIST;
	#endif
	
    if (amc_led[led_n].control_state == LOCAL_CONTROL_STATE)
        return;
	
	if (led_state == LED_LONG_BLINK){
		amc_led[led_n].on_duration = LED_LONG_BLINK;
		amc_led[led_n].fnc_off = LED_LONG_BLINK;
	}else if (led_state == LED_SHORT_BLINK){
		amc_led[led_n].on_duration = LED_SHORT_BLINK;
		amc_led[led_n].fnc_off = LED_SHORT_BLINK;
	}
	
    switch (led_n)
    {
		case BLUE_LED:			
			if (led_state == LED_ON)						  cbi(LOCAL_BLUE_LED_PPORT, LOCAL_BLUE_LED_PIN);			
			else if(led_state == LED_OFF)                     sbi(LOCAL_BLUE_LED_PPORT, LOCAL_BLUE_LED_PIN);
			
			break;
			
		case RED_LED:
			if (led_state == LED_ON)						  cbi(LOCAL_RED_LED_PPORT, LOCAL_RED_LED_PIN);		
			else if(led_state == LED_OFF)                     sbi(LOCAL_RED_LED_PPORT, LOCAL_RED_LED_PIN);
		
			break;
			
		case GREEN_LED:
			if (led_state == LED_ON)						  cbi(LOCAL_GREEN_LED_PPORT, LOCAL_GREEN_LED_PIN);		
			else if(led_state == LED_OFF)                     sbi(LOCAL_GREEN_LED_PPORT, LOCAL_GREEN_LED_PIN);
			
			break;	
		
		default:
			#ifdef LED_DEFINITION_ENABLE
				switch(led[led_n-3].io_type){
					case MMC_PORT:
						if(led_state == LED_ON)			active_mmcport_led(led_n-3);
						else if(led_state == LED_OFF)	desactive_mmcport_led(led_n-3);
						break;
			
					case IO_EXTENDER_PCF8574AT:
						data = get_PCF8574AT_port(led[led_n-3].addr);
						
						if(led_state == LED_ON){
							if(led[led_n-3].active)			data |= (0x01 < (led[led_n-3].pin));
							else							data &= ~(0x01 < (led[led_n-3].pin));
						}
						else if(led_state == LED_OFF){
							if(led[led_n-3].inactive)		data |= (0x01 < (led[led_n-3].pin));
							else							data &= ~(0x01 < (led[led_n-3].pin));
						}
						set_PCF8574AT_port(led[led_n-3].addr, data);
						
						break;
				}		
			#else
				if (USER_CODE == 2)				led_control_user(led_n, led_state);
			#endif
			
			break;
    }

    amc_led[led_n].state = led_state;
}


//*****************************************************************/
u08 ipmi_set_fru_led_state(u08 fru, u08 LedId, u08 LedFn, u08 LedOn)   //Called from ipmi_if.c
//*****************************************************************/
{
    if (fru == 0)
    {		
        if (LedId >= NUM_OF_AMC_LED)                       // value out of range
            return (0xff);

		if (amc_led[LedId].control_state == LAMP_TEST_STATE)	//JM : Set FRU Led State should return completion code 0xCC if Lamp Test is in progress
		{
				return 0xff;
		}
        else if (LedFn >= 1 && LedFn <= 0xfa)	//JM : The Led blink function is not implemented => This command can not be executed
        {
            amc_led[LedId].fnc_off       = LedFn;          // ON/OFF/Off time
            amc_led[LedId].on_duration   = LedOn;          // ON time
            amc_led[LedId].control_state = OVERRIDE_STATE; // initial state
			
			led_control(LedId, LED_BLINK);			
        }
        else if (LedFn == LED_OFF || LedFn == LED_ON)
        {
            amc_led[LedId].fnc_off       = LedFn;            // ON/OFF/Off time
            amc_led[LedId].on_duration   = 0x00;             // ON time
            amc_led[LedId].control_state = OVERRIDE_STATE;   // initial state
            led_control(LedId, LedFn);
        }
        else if (LedFn == RESTORE_LOCAL_CONTROL)
        {
            amc_led[LedId].control_state = LOCAL_CONTROL_STATE; // initial state			
            local_led_control(LedId, amc_led[LedId].local_cntr_fnc);
        }
		else if (LedFn == LAMP_TEST_CMD){		//JM : Lamp Test case		
					
			if (LedOn >= 128){	//Test time value should be less than 128
				return (0xff);
			}
			
			//	Save the old state
			if (amc_led[LedId].control_state == LOCAL_CONTROL_STATE)
				amc_led[LedId].lamp_test_return_control_state = RESTORE_LOCAL_CONTROL;
			else
				amc_led[LedId].lamp_test_return_control_state = amc_led[LedId].fnc_off;
			
			//amc_led[LedId].fnc_off = LAMP_TEST_STATE;
			
			//	Set the control state
			amc_led[LedId].control_state = LAMP_TEST_STATE;
			
			//	Set cnt and duration : it is used to get the test lamp remaining time
			
			amc_led[LedId].lamp_test_duration = LedOn;
			amc_led[LedId].lamp_test_cnt = 0;
			led_control(LedId, LED_ON);
		}
        else
            return (0xff);
    }
	#ifdef USE_RTM
		else if (fru == 1)
		{
			rtm_led[LedId].fnc_off       = LedFn;          // ON/OFF/Off time
			rtm_led[LedId].on_duration   = LedOn;          // ON time
			rtm_led[LedId].control_state = OVERRIDE_STATE; // initial state

			//set_led_pattern(0, 0x83, 1);
			if (LedFn == LED_OFF)
				rtm_led_onoff(LedId, LED_OFF);     //Step MTCA.4, 3.5.1-20 OK if rqs.data[2] refers to the BLUE LED
			else if (LedFn == LED_ON)
				rtm_led_onoff(LedId, LED_ON);      //Step MTCA.4, 3.5.2-11b OK if rqs.data[2] refers to the BLUE LED
			else //Blink the LED //MJ: This is a simplified implementation. The PICMG3.0 standard requires more sophistication...
				rtm_led_onoff(LedId, LED_BLINK);  //Step MTCA.4, 3.5.1-11 or 3.5.2-3 OK (sort of...)
				//According to MTCA.4, 3.5.1-10 the blue LED of the RTM has to do a long blink
				//According to MTCA.4, 3.5.2-3 the blue LED of the RTM has to do a short blink
		}
	#endif
    return (0);
}


//*****************************************************/
u08 ipmi_get_fru_led_state(u08 fru, u08 LedId, u08 *buf)   //Called from ipmi_if.c
//*****************************************************/
{
    u08 len = 0;

    if (fru == 0)
    {
        if (LedId >= NUM_OF_AMC_LED)                 // value out of range
            return (0xff);

        buf[len++] = amc_led[LedId].control_state;   // LED States - override state has been enabled
        buf[len++] = amc_led[LedId].local_cntr_fnc;  // Local Control Function - not supported
        buf[len++] = amc_led[LedId].on_duration;     // Local control ON duration
        buf[len++] = amc_led[LedId].color;           // Local colour: Blue or Red  //MJ: Where is green?
        if (amc_led[LedId].control_state == OVERRIDE_STATE || amc_led[LedId].control_state == LAMP_TEST_STATE)	//Send information in case of lamp test
        {
            buf[len++] = amc_led[LedId].fnc_off;     // Override state function - ON/OFF/Off time
            buf[len++] = amc_led[LedId].on_duration; // Override control ON duration
            buf[len++] = amc_led[LedId].color;       // Override colour: Blue or Red
			
			if(amc_led[LedId].control_state == LAMP_TEST_STATE)	//Lamp test case : lamp test duration should be sent
				buf[len++] = amc_led[LedId].lamp_test_duration;
        }
    }
#ifdef USE_RTM
    else if (fru == 1)
    {
        if (LedId >= NUM_OF_RTM_LED)                 // value out of range
            return (0xff);

        buf[len++] = rtm_led[LedId].control_state;   // LED States - override state has been enabled
        buf[len++] = rtm_led[LedId].local_cntr_fnc;  // Local Control Function - not supported
        buf[len++] = 0x00;                           // Local control ON duration
        buf[len++] = rtm_led[LedId].color;           // Local colour: Blue or Red  //MJ: Where is green?
        if (rtm_led[LedId].control_state == OVERRIDE_STATE)
        {
            buf[len++] = rtm_led[LedId].fnc_off;     // Override state function - ON/OFF/Off time
            buf[len++] = rtm_led[LedId].on_duration; // Override control ON duration
            buf[len++] = rtm_led[LedId].color;       // Override colour: Blue or Red
        }
    }
#endif

    return (len);
}


//***********************************************/
u08 ipmi_get_fru_led_properties(u08 fru, u08 *buf)  //Called from ipmi_if.c
//***********************************************/
{
    u08 len = 0;

    buf[len++] = 0x07;           // support the control of 3 standard LEDs (BLUE LED, LED1, LED2). LED3 is not supported

    if(fru == 0)
        buf[len++] = NUM_OF_AMC_LED; // number of AMC LEDs under control
	#ifdef USE_RTM
		else if(fru == 1)
			buf[len++] = NUM_OF_RTM_LED; // number of RTM LEDs under control
	#endif
    else
        buf[len++] = 0; // Just in case....


    return (len);
}


//**************************************************************/
u08 ipmi_get_led_color_capabilities(u08 fru, u08 LedId, u08 *buf)   //Called from ipmmi_if.c
//**************************************************************/
{
    u08 len = 0;

	#ifdef LED_DEFINITION_ENABLE
		LED_t led[AMC_USER_LED_CNT] = AMC_USER_LED_LIST;
	#endif
	
    if (fru == 0 && LedId >= NUM_OF_AMC_LED)     // value out of range
        return(0xff);
	#ifdef USE_RTM
		if (fru == 1 && LedId >= NUM_OF_RTM_LED)     // value out of range
			return(0xff);
	#endif
	
    switch (LedId)               //For the 3 standard LEDs we do not have to distinguish between fru=0 and fru=1
    {                            //The data returned by this function is defined in table 3-30 of the ATCA spec.
    case BLUE_LED:
        buf[len++] = 1 << BLUE;  // capability is BLUE only
        buf[len++] = BLUE;       // default colour is BLUE
        buf[len++] = BLUE;       // default colour in override state is BLUE
        break;
    case RED_LED:
        buf[len++] = 1 << RED;   // capability is RED only
        buf[len++] = RED;        // default colour is RED
        buf[len++] = RED;        // default colour in override state is RED
        break;
    case GREEN_LED:
        buf[len++] = 1 << GREEN; // capability is RED only
        buf[len++] = GREEN;      // default colour is RED
        buf[len++] = GREEN;      // default colour in override state is RED
        break;
    default:
		#ifdef LED_DEFINITION_ENABLE
			buf[len++] = 1 << led[LedId-3].color;  // capability
			buf[len++] = led[LedId-3].color;       // default colour
			buf[len++] = led[LedId-3].color;       // default colour in override state
		#else
			if (USER_CODE == 2)            return(ipmi_get_led_color_capabilities_user(fru, LedId, buf));
		#endif		
		break;
    }

    return (len);
}

void ledTimerCallback(){	//Called every 10 ms
	u16 i;
	u08 ledState, state;
	
	for (i=0; i<NUM_OF_AMC_LED; i++){
		if(amc_led[i].control_state == LAMP_TEST_STATE){
			amc_led[i].lamp_test_cnt++;
			
			if(amc_led[i].lamp_test_cnt >= (amc_led[i].lamp_test_duration*10)){	//Lamp test finished : lamp_test_duration is set in hundreds of milliseconds					
				amc_led[i].control_state = EMPTY;
				ipmi_set_fru_led_state(0, i, amc_led[i].lamp_test_return_control_state, amc_led[i].on_duration);	
			}
		}
		else if(amc_led[i].state == LED_BLINK || amc_led[i].state == LED_LONG_BLINK || amc_led[i].state == LED_SHORT_BLINK){
			amc_led[i].cnt--;			
			
			if (amc_led[i].cnt <= 0){				
				ledState = state_led(i);
				state = amc_led[i].state;
				
				if(amc_led[i].control_state == OVERRIDE_STATE && ledState)			led_control(i, LED_OFF);
				else if(amc_led[i].control_state == OVERRIDE_STATE && !ledState)	led_control(i, LED_ON);
				else if(ledState)													local_led_control(i, LED_OFF);
				else if(!ledState)													local_led_control(i, LED_ON);
				
				amc_led[i].state = state;
				
				if (state == LED_LONG_BLINK)		amc_led[i].cnt = LED_LONG_BLINK;
				else if(state == LED_SHORT_BLINK)	amc_led[i].cnt = LED_SHORT_BLINK;
				else if(ledState)					amc_led[i].cnt = amc_led[i].fnc_off;
				else 								amc_led[i].cnt = amc_led[i].on_duration;
			}
		}
	}
}

/* Pin control functions */
u08 get_mmcport_led_state(u08 led_n){
	#ifdef LED_DEFINITION_ENABLE	
		LED_t led[AMC_USER_LED_CNT] = AMC_USER_LED_LIST;
		
		switch(led[led_n].port){
			case MMCPORT_A:	return (inb(PINA) & BV(led[led_n].pin))?HIGH:LOW;
			case MMCPORT_B:	return (inb(PINB) & BV(led[led_n].pin))?HIGH:LOW;
			case MMCPORT_C:	return (inb(PINC) & BV(led[led_n].pin))?HIGH:LOW;
			case MMCPORT_D:	return (inb(PIND) & BV(led[led_n].pin))?HIGH:LOW;
			case MMCPORT_E:	return (inb(PINE) & BV(led[led_n].pin))?HIGH:LOW;
			case MMCPORT_F:	return (inb(PINF) & BV(led[led_n].pin))?HIGH:LOW;
			case MMCPORT_G: return (inb(PING) & BV(led[led_n].pin))?HIGH:LOW;
		}
	#endif
	
	return 0x00;
}

void active_mmcport_led(u08 led_n){
	#ifdef LED_DEFINITION_ENABLE
		LED_t led[AMC_USER_LED_CNT] = AMC_USER_LED_LIST;
		
		if(led[led_n].active){
			switch(led[led_n].port){
				case MMCPORT_A:	sbi(PORTA, led[led_n].pin);		break;
				case MMCPORT_B:	sbi(PORTB, led[led_n].pin);		break;
				case MMCPORT_C:	sbi(PORTC, led[led_n].pin);		break;
				case MMCPORT_D:	sbi(PORTD, led[led_n].pin);		break;
				case MMCPORT_E:	sbi(PORTE, led[led_n].pin);		break;
				case MMCPORT_F:	sbi(PORTF, led[led_n].pin);		break;
				case MMCPORT_G: sbi(PORTG, led[led_n].pin);		break;
			}
		}
		else{
			switch(led[led_n].port){
				case MMCPORT_A:	cbi(PORTA, led[led_n].pin);		break;
				case MMCPORT_B:	cbi(PORTB, led[led_n].pin);		break;
				case MMCPORT_C:	cbi(PORTC, led[led_n].pin);		break;
				case MMCPORT_D:	cbi(PORTD, led[led_n].pin);		break;
				case MMCPORT_E:	cbi(PORTE, led[led_n].pin);		break;
				case MMCPORT_F:	cbi(PORTF, led[led_n].pin);		break;
				case MMCPORT_G: cbi(PORTG, led[led_n].pin);		break;
			}
		}
	#endif
}

void desactive_mmcport_led(u08 led_n){
	#ifdef LED_DEFINITION_ENABLE
		LED_t led[AMC_USER_LED_CNT] = AMC_USER_LED_LIST;
		
		if(led[led_n].inactive){
			switch(led[led_n].port){
				case MMCPORT_A:	sbi(PORTA, led[led_n].pin);		break;
				case MMCPORT_B:	sbi(PORTB, led[led_n].pin);		break;
				case MMCPORT_C:	sbi(PORTC, led[led_n].pin);		break;
				case MMCPORT_D:	sbi(PORTD, led[led_n].pin);		break;
				case MMCPORT_E:	sbi(PORTE, led[led_n].pin);		break;
				case MMCPORT_F:	sbi(PORTF, led[led_n].pin);		break;
				case MMCPORT_G: sbi(PORTG, led[led_n].pin);		break;
			}
		}
		else{
			switch(led[led_n].port){
				case MMCPORT_A:	cbi(PORTA, led[led_n].pin);		break;
				case MMCPORT_B:	cbi(PORTB, led[led_n].pin);		break;
				case MMCPORT_C:	cbi(PORTC, led[led_n].pin);		break;
				case MMCPORT_D:	cbi(PORTD, led[led_n].pin);		break;
				case MMCPORT_E:	cbi(PORTE, led[led_n].pin);		break;
				case MMCPORT_F:	cbi(PORTF, led[led_n].pin);		break;
				case MMCPORT_G: cbi(PORTG, led[led_n].pin);		break;
			}
		}
	#endif
}

u08 get_PCF8574AT_port(u08 addr){
	
	u08 data;
		
	CRITICAL_SECTION_START;
	i2cswReceive(addr, 0, 0, 1, &data);
	CRITICAL_SECTION_END;
	
	return data;
}

void set_PCF8574AT_port(u08 addr, u08 data){
	CRITICAL_SECTION_START;
	i2cswSend(addr, 0, 0, 1, &data);
	CRITICAL_SECTION_END;
}

