/*
 * sensors.c
 *
 * Created: 27/03/2015 12:37:17
 *  Author: jumendez
 */ 

#include <avr/io.h>
#include "../avrlibtypes.h"
#include "../avrlibdefs.h"
#include "../a2d.h"
#include "../led.h"
#include "../sdr.h"
#include "../i2csw.h"
#include "../ipmi_if.h"
#include "../eeprom.h"
#include "../fru.h"
#include "../timer.h"
#include <util/delay.h>
#include "PLL_config.h"

#include "sensors.h"

#include "../hotswap.h"

int tt=0;
extern u08 state;
extern u08 handle_switch_value;
int on_delay=0;

u08 temp1, temp2, temp3, v12;

//Sensor initialization
void sensor_init_user(){
	
}
 
//Sensor are polled (Every 100 ms)
void sensor_monitoring_user(){ 
	
 // DE: Initial state of the ADC multiplexer is set by the function sensor_init() to the V12 channel (sdr.c)
 // the sensor_init() also starts first adc conversion, so that ADCH register has the V12 value when 
 // entering this function for the first time
 
/*	temp3=tempSensorRead(TEMPERATURE_SENSOR3);
	set_sensor_value(TEMPERATURE_SENSOR3, temp3);
*/

	switch (tt){
		case 0:
		v12=a2dConvert8bit(0); // DE: it is not clear why the argument is needed for a2dConvert8bit(), because this function just returns current state of the ADCH register (i.e. from recent ADC conversion)
		set_sensor_value(VOLT_12, v12);
		a2dSetChannel(1);
		a2dStartConvert();
		tt=1;
		break;
		case 1:
		temp1=a2dConvert8bit(1);
		set_sensor_value(TEMPERATURE_SENSOR1, temp1);//((temp1*330)>>8)-50);
		a2dSetChannel(2);
		a2dStartConvert();
		tt=2;
		break;
		case 2:
		temp2=a2dConvert8bit(1);
		set_sensor_value(TEMPERATURE_SENSOR2, temp2);//((temp2*330)>>8)-50);
		a2dSetChannel(0);
		a2dStartConvert();
		tt=0;
		break;
		default: //DE: added default state in order to avoid hanging in an unknown state if tt is changed for any reason
			v12=a2dConvert8bit(0);
			set_sensor_value(VOLT_12, v12);
			a2dSetChannel(0);
			a2dStartConvert();
			tt=0;
			
	}
	if(on_delay<ON_DELAY_T){
		on_delay += 1;
	}
	else{
		if(on_delay==ON_DELAY_T){
			if(handle_switch_value == HANDLE_SWITCH_CLOSED){
				sendHotswapEvent(HANDLE_SWITCH_CLOSED);
			}
			on_delay=ON_DELAY_T + 1;
		}
	}

	
}


//***************************/
u08 tempSensorRead(u08 sensor)    //Called from sensor_monitoring_user in user_code.c
//***************************/
{
	u08 stemp;     //sensor temperature
	u08 saddr;     //temperature sensor address

	if (sensor == TEMPERATURE_SENSOR1)
	saddr = 0x2A; // this sensor is supplied by the MP.
	if (sensor == TEMPERATURE_SENSOR2)
	saddr = 0x1A; // this sensor is supplied via the PP (only accessible when in M4).
	if (sensor ==  TEMPERATURE_SENSOR3)
	{saddr = 0x18;
		I2C_write_One_byte(0x74,0x10);
		_delay_ms(10);
	}

	CRITICAL_SECTION_START;
	i2cswReceive(saddr, 0, 1, 1, &stemp);
	CRITICAL_SECTION_END;

	return stemp;
}
