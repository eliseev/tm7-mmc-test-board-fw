/*
 * sensors.h
 *
 * Created: 26/03/2015 22:22:20
 *  Author: jumendez
 */ 
#include "../avrlibtypes.h"
#include "../avrlibdefs.h"

#ifndef SENSORS_H_
#define SENSORS_H_

#define SDR_MAX_ID		5

#define ON_DELAY_T		50	//delay (in 100 ms unit) for switching on the payload power if the handle is pushed after a crate powercycle
/*Define sensor number*/
#define HOT_SWAP_SENSOR     0
#define VOLT_12				1
#define TEMPERATURE_SENSOR1 2
#define TEMPERATURE_SENSOR2 3
#define TEMPERATURE_SENSOR3 4

#define SDR0_RECORD		/* Mandatory : Device locator (Should be SDR0_RECORD) */		\
	{																					\
		0x01,           /* record number, LSB -> SDR_Init */							\
		0x00,           /* record number,MSB -> SDR_Init */								\
		0x51,           /* IPMI protocol version */										\
		0x12,           /* record type: device locator */								\
		0x16,           /* record length: remaining bytes -> SDR_Init */				\
		0x00,           /* device slave address */										\
		0x00,           /* channel number */											\
		0x00,           /* power state notification, global initialization */			\
		0x29,           /* device capabilities */										\
		0x00,           /* reserved */													\
		0x00,           /* reserved */													\
		0x00,           /* reserved */													\
		0xc1,           /* entityID */													\
		0x00,           /* entity instance */											\
		0x00,           /* OEM */														\
		0xcb,           /* device ID string type/length */								\
		'A','M','C',' ','T','w','i','n','M','u','x'										\
	}

#define SDR1_RECORD		/* Manadatory : Hotswap */										\
	{																					\
		0x03,            /* record number, LSB  - filled by SDR_Init() */				\
		0x00,            /* record number, MSB  - filled by SDR_Init() */				\
		0x51,            /* IPMI protocol version */									\
		0x01,            /* record type: full sensor */									\
		0x37,            /* record length: remaining bytes -> SDR_Init */				\
		0x00,            /* i2c address, -> SDR_Init */									\
		0x00,            /* sensor owner LUN */											\
		HOT_SWAP_SENSOR, /* sensor number */											\
		0xc1,            /* entity id: AMC Module */									\
		0x00,            /* entity instance -> SDR_Init */								\
		0x03,            /* init: event generation + scanning enabled */				\
		0xc1,            /* capabilities: auto re-arm, */								\
		HOT_SWAP,        /* sensor type: HOT SWAP */									\
		0x6f,            /* sensor reading */											\
		0x07,            /* LSB assert event mask: 3 bit value */						\
		0x00,            /* MSB assert event mask */									\
		0x00,            /* LSB deassert event mask: 3 bit value */						\
		0x00,            /* MSB deassert event mask */									\
		0x07,            /* LSB: discrete reading mask */								\
		0x00,            /* MSB: discrete reading mask */								\
		0xc0,            /* sensor units 1 */											\
		0x00,            /* sensor units 2 */											\
		0x00,            /* sensor units 3 */											\
		0x00,            /* Linearization */											\
		0x00,            /* M */														\
		0x00,            /* M - Tolerance */											\
		0x00,            /* B */														\
		0x00,            /* B - Accuracy */												\
		0x00,            /* Sensor direction */											\
		0x00,            /* R-Exp , B-Exp */											\
		0x00,            /* Analogue characteristics flags */							\
		0x00,            /* Nominal reading */											\
		0x00,            /* Normal maximum */											\
		0x00,            /* Normal minimum */											\
		0x00,            /* Sensor Maximum reading */									\
		0x00,            /* Sensor Minimum reading */									\
		0x00,            /* Upper non-recoverable Threshold */							\
		0x00,            /* Upper critical Threshold */									\
		0x00,            /* Upper non critical Threshold */								\
		0x00,            /* Lower non-recoverable Threshold */							\
		0x00,            /* Lower critical Threshold */									\
		0x00,            /* Lower non-critical Threshold */								\
		0x00,            /* positive going Threshold hysteresis value */				\
		0x00,            /* negative going Threshold hysteresis value */				\
		0x00,            /* reserved */													\
		0x00,            /* reserved */													\
		0x00,            /* OEM reserved */												\
		0xcc,            /* 8 bit ASCII, number of bytes */								\
		'F', 'R', 'U', ' ', 'H', 'O', 'T', '_', 'S', 'W', 'A', 'P' /* sensor string */	\
	}
	
#define SDR2_RECORD																				\
	{																							\
		0x06,				/* record number, LSB  - filled by SDR_Init() */					\
		0x00,				/* record number, MSB  - filled by SDR_Init() */					\
		0x51,				/* IPMI protocol version */											\
		0x01,				/* record type: full sensor */										\
		0x2e,				/* record length: remaining bytes -> SDR_Init */					\
		0x00,				/* i2c address, -> SDR_Init */										\
		0x00,				/* sensor owner LUN */												\
		VOLT_12,			/* sensor number */													\
		0xc1,				/* entity id: AMC Module */											\
		0x00,				/* entity instance -> SDR_Init */									\
		0x03,				/* init: event generation + scanning enabled */						\
		0x48,				/* capabilities: auto re-arm, Threshold is readable and setable */	\
		VOLTAGE,			/* sensor type: Voltage */											\
		0x01,				/* sensor reading: Threshold */										\
		0x80,				/* LSB assert event mask: 3 bit value */							\
		0x0a,				/* MSB assert event mask */											\
		0xa8,				/* LSB deassert event mask: 3 bit value */							\
		0x7a,				/* MSB deassert event mask */										\
		0x38,				/* LSB: readable Threshold mask: all thresholds are readable */		\
		0x38,				/* MSB: setable Threshold mask: all thresholds are setable */		\
		0x00,				/* sensor units 1 : Unsigned */										\
		0x04,				/* sensor units 2 : Temperature */									\
		0x00,				/* sensor units 3 : Modifier */										\
		0x00,				/* Linearization */													\
		0x4D,				/* M //52 */														\
		0x02,				/* M - Tolerance */													\
		0x00,				/* B */																\
		0x04,				/* B - Accuracy */													\
		0x0c,				/* Sensor direction */												\
		0xD0,				/* R-Exp , B-Exp   //0xD0 */										\
		0x07,				/* Analogue characteristics flags */								\
		0x7f,				/* Nominal reading */												\
		0xef,				/* Normal maximum */												\
		0xab,				/* Normal minimum */												\
		0xff,				/* Sensor Maximum reading */										\
		0x00,				/* Sensor Minimum reading */										\
		0xBd,				/* Upper non-recoverable Threshold */								\
		0xb0,				/* Upper critical Threshold */										\
		0xA9,				/* Upper non critical Threshold */									\
		0x8a,				/* Lower non-recoverable Threshold */								\
		0x90,				/* Lower critical Threshold */										\
		0x96,				/* Lower non-critical Threshold */									\
		0x02,				/* positive going Threshold hysteresis value */						\
		0x02,				/* negative going Threshold hysteresis value */						\
		0x00,				/* reserved */														\
		0x00,				/* reserved */														\
		0x00,				/* OEM reserved */													\
		0xc3,				/* 8 bit ASCII, number of bytes */									\
		'1', '2', 'V'		/* sensor string */													\
	}
	
	//Temperature 1 - Full Sensor
#define SDR3_RECORD																				\
{																								\
    /*sensor record header*/																	\
    /*Byte 1*/																					\
    0x02,                /*record number, LSB*/													\
    0x00,                /*record number,MSB*/													\
    0x51,                /*IPMI protocol version*/												\
    0x01,                /*record type: full sensor*/											\
    0x33,                /*record length: remaining bytes -> SDR_Init*/							\
    /*record key bytes*/																		\
    0x00,                /*i2c address, -> SDR_Init*/											\
    0x00,                /*sensor owner LUN*/													\
    /*Byte 8*/																					\
    TEMPERATURE_SENSOR1, /*sensor number*/														\
    /*record body bytes*/																		\
    0xc1,                /*entity id: AMC Module*/												\
    0x00,                /*entity instance -> SDR_Init*/										\
    0x03,                /*init: event generation + scanning enabled*/							\
    0x48,                /*capabilities: auto re-arm, Threshold is readable and setable*/		\
    0x01,                /*sensor type: Temperature*/											\
    0x01,                /*sensor reading: Threshold*/											\
    0x80,                /*LSB assert event mask: 3 bit value*/									\
    /*Byte 16*/																					\
    0x0a,                /*MSB assert event mask*/												\
    0xa8,                /*LSB deassert event mask: 3 bit value*/								\
    0x7a,                /*MSB deassert event mask*/											\
    0x38,                /*LSB: readable Threshold mask: all thresholds are readable*/			\
    0x38,                /*MSB: setable Threshold mask: all thresholds are setable*/			\
    0x80,                /*sensor units 1 : 2's complement*/									\
    0x01,                /*sensor units 2 : Temperature*/										\
    0x00,                /*sensor units 3 : Modifier*/											\
    /* Byte 24*/																				\
    0x00,                /*Linearisation*/														\
    0x81,                /*M*/																	\
    0x01,                /*M - Tolerance*/														\
    0x0C,                /*B*/																	\
    0xA5,                /*B - Accuracy*/														\
    0x88,                /*Sensor direction*/													\
    0xE1,                /*R-Exp , B-Exp*/														\
    0x07,                /*Analogue characteristics flags*/										\
    /*Byte 32*/																					\
    58,                  /*Nominal reading*/													\
    78,                  /*Normal maximum*/														\
    54,                  /*Normal minimum*/														\
    0xFF,                /*Sensor Maximum reading*/												\
    0x00,                /*Sensor Minimum reading*/												\
    97,                  /*Upper non-recoverable Threshold*/									\
    89,                  /*Upper critical Threshold*/											\
    81,                  /*Upper non critical Threshold*/										\
    /*Byte 40*/																					\
    31,                 /*Lower non-recoverable Threshold*/									\
    38,                   /*Lower critical Threshold*/											\
    47,                  /*Lower non-critical Threshold*/										\
    0x02,                /*positive going Threshold hysteresis value*/							\
    0x02,                /*negative going Threshold hysteresis value*/							\
    0x00,                /*reserved*/															\
    0x00,                /*reserved*/															\
    0x00,                /*OEM reserved*/														\
    /*Byte 48*/																					\
    0xC9,                /*8 bit ASCII, number of bytes*/										\
    'T','M','P','3','6','_','I','C','1'   /*sensor string*/										\
}					

/*Temperature 2 - Full Sensor*/
#define SDR4_RECORD																				\
{																								\
    /*sensor record header*/																	\
    /*Byte 1*/																					\
    0x03,                /*record number, LSB  - filled by SDR_Init()*/							\
    0x00,                /*record number, MSB  - filled by SDR_Init()*/							\
    0x51,                /*IPMI protocol version*/												\
    0x01,                /*record type: full sensor*/											\
    0x33,                /*record length: remaining bytes -> SDR_Init*/							\
    /*record key bytes*/																		\
    0x00,                /*i2c address, -> SDR_Init*/											\
    0x00,                /*sensor owner LUN*/													\
    /*Byte 8*/																					\
    TEMPERATURE_SENSOR2, /*sensor number*/														\
    /*record body bytes*/																		\
    0xc1,                /*entity id: AMC Module*/												\
    0x00,                /*entity instance -> SDR_Init*/										\
    0x03,                /*init: event generation + scanning enabled*/							\
    0x48,                /*capabilities: auto re-arm, Threshold is readable and setable*/		\
    0x01,                /*sensor type: Temperature*/											\
    0x01,                /*sensor reading: Threshold*/											\
    0x80,                /*LSB assert event mask: 3 bit value*/									\
    /*Byte 16*/																					\
    0x0a,                /*MSB assert event mask*/												\
    0xa8,                /*LSB deassert event mask: 3 bit value*/								\
    0x7a,                /*MSB deassert event mask*/											\
    0x38,                /*LSB: readable Threshold mask: all thresholds are readable*/			\
    0x38,                /*MSB: setable Threshold mask: all thresholds are setable*/			\
    0x80,                /* Modified by P.Vichoudis - it was: 0x00, sensor units 1 : 2's complement*/	\
    0x01,                /*sensor units 2 : Temperature*/										\
    0x00,                /*sensor units 3 : Modifier*/											\
    /*Byte 24*/																					\
    0x00,                /*Linearisation*/														\
    0x81,                /*M*/																	\
    0x01,                /*M - Tolerance*/														\
    0x0C,                /*B*/																	\
    0xA5,                /*B - Accuracy*/														\
    0x88,                /*Sensor direction*/													\
    0xE1,                /*R-Exp , B-Exp*/														\
    0x07,                /*Analogue characteristics flags*/										\
    /*Byte 32*/																					\
    58,                  /*Nominal reading*/													\
    78,                  /*Normal maximum*/														\
    54,                  /*Normal minimum*/														\
    0xff,                /*Sensor Maximum reading*/												\
    0x00,                /*Sensor Minimum reading*/												\
    97,                 /*Upper non-recoverable Threshold*/										\
    89,                  /*Upper critical Threshold*/											\
    81,                  /*Upper non critical Threshold*/										\
    /*Byte 40*/																					\
    31,                 /*Lower non-recoverable Threshold*/									\
    38,                 /*Lower critical Threshold*/												\
    47,                   /*Lower non-critical Threshold*/										\
    0x02,                /*positive going Threshold hysteresis value*/							\
    0x02,                /*negative going Threshold hysteresis value*/							\
    0x00,                /*reserved*/															\
    0x00,                /*reserved*/															\
    0x00,                /*OEM reserved*/														\
    /*Byte 48*/																					\
    0xca,                /*8 bit ASCII, number of bytes*/										\
    'T','M','P','3','6','_','I','C','4','2'  /*sensor string*/									\
}


/* Temperature 3 - Full Sensor*/
#define SDR5_RECORD																				\
{																								\
    /* sensor record header*/																	\
    0x03,           /* record number, LSB  - filled by SDR_Init()*/								\
    0x00,           /* record number, MSB  - filled by SDR_Init()*/								\
    0x51,           /* IPMI protocol version*/													\
    0x01,           /* record type: full sensor*/												\
    0x33,           /* record length: remaining bytes -> SDR_Init*/								\
    /* record key bytes*/																		\
    0x00,           /* i2c address, -> SDR_Init*/												\
    0x00,           /* sensor owner LUN*/														\
    TEMPERATURE_SENSOR3,    /* sensor number*/													\
    /* record body bytes*/																		\
    0xc1,           /* entity id: AMC Module*/													\
    0x00,           /* entity instance -> SDR_Init*/											\
    0x03,           /* init: event generation + scanning enabled*/								\
    0x48,           /* capabilities: auto re-arm, Threshold is readable and setable,*/			\
    0x01,           /* sensor type: Temperature*/												\
    0x01,           /* sensor reading: Threshold*/												\
    0x80,           /* LSB assert event mask: 3 bit value*/										\
    0x0a,           /* MSB assert event mask*/													\
    0xa8,           /* LSB deassert event mask: 3 bit value*/									\
    0x7a,           /* MSB deassert event mask*/												\
    0x38,           /* LSB: readabled Threshold mask: all thresholds are readabled:*/			\
    0x38,           /* MSB: setabled Threshold mask: all thresholds are setabled:*/				\
    0x80,           /* sensor units 1 : 2's complement*/										\
    0x01,           /* sensor units 2 : Temperature*/											\
    0x00,           /* sensor units 3 : Modifier*/												\
    0x00,           /* Linearization*/															\
    0x01,           /* M*/																		\
    0x01,           /* M - Tolerance*/															\
    0x00,           /* B*/																		\
    0x25,           /* B - Accuracy*/															\
    0x88,           /* Sensor direction*/														\
    0x00,           /* R-Exp , B-Exp*/															\
    0x07,           /* Analogue characteristics flags*/											\
    25,             /* Nominal reading*/														\
    50,             /* Normal maximum*/															\
    20,             /* Normal minimum*/															\
    127,            /* Sensor Maximum reading*/													\
    -128,           /* Sensor Minimum reading*/													\
    75,             /* Upper non-recoverable Threshold*/										\
    65,             /* Upper critical Threshold*/												\
    55,             /* Upper non critical Threshold*/											\
    -10,            /* Lower non-recoverable Threshold*/										\
    0,            /* Lower critical Threshold*/													\
    10,              /* Lower non-critical Threshold*/											\
    0x02,           /* positive going Threshold hysteresis value*/								\
    0x02,           /* negative going Threshold hysteresis value*/								\
    0x00,           /* reserved*/																\
    0x00,           /* reserved*/																\
    0x00,           /* OEM reserved*/															\
    0xc8,           /* 8 bit ASCII, number of bytes*/											\
    'L','M','8','2',' ','I','C','3' /* sensor string*/											\
}	

	
#endif /* SENSORS_H_ */

u08 tempSensorRead(u08 sensor);