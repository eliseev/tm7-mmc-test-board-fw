//*****************************************************************************
// File Name    : user_code.h
//
// User code for the ALB & RTM
//
// Author		: Markus Joos (markus.joos@cern.ch)
// Modified by	: Julian Mendez <julian.mendez@cern.ch>
//*****************************************************************************

//User code for the ALB-RTM for AMC V3

// This file is the header file for user code.
// Users of the MMC S/W should only have to modify this file as well as user_code.c

#include <avr/io.h>

#ifndef USER_CODE_H
#define USER_CODE_H

#include "../avrlibtypes.h"
#include "../avrlibdefs.h"

#define ADD_0		4
#define ADD_1		5
#define ADD_2		4
#define ADD_3		3

//********************************
//Constants for enable features
//********************************
//#define USE_RTM
#define LED_DEFINITION_ENABLE
#define USERGPIO_DEF_ENABLE
#define PROM_VERSION_CHANGE_ENABLE
#define ENABLE_CONTROLLER_SPECIFIC		/*	Controller specific : Vendor specific (16 Network Functions [8 pairs]). 
											The Manufacturer ID	associated with the controller implementing the 
											command identifies the vendor or group that specifies the command 
											functionality. While the vendor defines the functional semantics for the 
											cmd and data fields, the cmd field is required to hold the same value in 
											requests and responses for a given operation in order for the messages to 
											be supported under the IPMI message handling and transport mechanisms. */
	

//********************************
//Constants for the IPMI DEVICE ID
//********************************
// firmware release
#define MMC_FW_REL_MAJ       1                  // major release, 7 bit
#define MMC_FW_REL_MIN       0                  // minor release, 8 bit

// manufacturer specific identifier codes
// NOTE: Manufacturer identification is handled by http://www.iana.org/assignments/enterprise-numbers
#define IPMI_MSG_MANU_ID_LSB 0x60               //CERN IANA ID = 0x000060
#define IPMI_MSG_MANU_ID_B2  0x00
#define IPMI_MSG_MANU_ID_MSB 0x00
#define IPMI_MSG_PROD_ID_LSB 0x35
#define IPMI_MSG_PROD_ID_MSB 0x12

//*****************************
// Controller specific commands
//*****************************
#define FRU_PROM_REVISION_CMD 1
#define JTAG_CTRL_SET_CMD     2
#define FPGA_JTAG_PLR_CMD     3

//*****************************
//Mandatory function prototypes
//*****************************
//NOTE: Users must not change the API of the functions listed below
u08 ipmi_oem_user(u08 command, u08 *iana, u08 *user_data, u08 data_size, u08 *buf, u08 *user_error_code);
u08 ipmi_controller_specific(u08 command, u08 *user_data, u08 data_size, u08 *buf, u08 *user_error_code);

void set_address_ipmi();

#endif
